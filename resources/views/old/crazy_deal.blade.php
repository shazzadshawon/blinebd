<!DOCTYPE HTML>
@include('pages.head')
<body>
    <div class="wrap">
        <div class="header">
            @include('pages.menu')   
           
        </div>
        <div class="main">
            <div class="content">
                <div class="content_top">
                    <div class="heading">
                        <h3>
                            @if($id==1)
                            <h1>Crazy Deal</h1>
                            @elseif($id==2)
                            <h1>NEW ARRIVALS</h1>
                            @elseif($id==4)
                            <h1>SPECIAL OFFERS</h1>
                            @elseif($id==3)
                            <h1>BEST SELLERS</h1>
                            @endif
                        </h3>
                    </div>
                    <div class="see">
                        {{-- <p><a href="#">See all Products</a></p> --}}
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="section group" style="margin-top: 10px;">
                    
                    
                    @foreach($products as $product_info)

                    @php
                    	$product_image = DB::table('product_images')
                    	->where('product_id',$product_info->id)
                    	->first();
                    @endphp
                    <div class="col-md-3 text-center">
                        <a href="{{URL::to('/product-details/'.$product_info->id)}}"><img src="../{{$product_image->product_image}}" class="img-thumbnail" alt=""  style="height: 250px;" /></a>
                        <h2>{{$product_info->product_name}}</h2>
                        <div class="price-details">
                            <div class="price-number">
                            @if($product_info->discount>0)
                             
                            <p><span class="rupees">{{$price = $product_info->product_price - ($product_info->product_price*$product_info->discount)/100}} TK</span> <strike>{{$product_info->product_price}}</strike></p>
                            @else
 <p><span class="rupees">{{$price =$product_info->product_price}} TK</span></p>
                            @endif
                               
                            </div>
                            <div class="add-cart">								
                                {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true]) !!}
                     <input type="hidden" name="product_id" value="{{$product_info->id}}">
                                <input type="hidden" name="product_name" value="{{$product_info->product_name}}">
                                <input type="hidden" name="product_code" value="{{$product_info->product_code}}">
                                <input type="hidden" name="product_price" value="
                                       @if($product_info->discount > 0)
                                       {{$dis = $product_info->product_price-($product_info->product_price*$product_info->discount)/100}} 
                                        @else
                                        {{$product_info->product_price}}
                                         @endif
                                       ">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$product_info->publication_status}}">
                     <button class="button" type="submit"> <span><a>Add to Cart</a></span> </button>
                    <div class="clear"></div>
                     {!! Form::close() !!}
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>             
            </div>
        </div>
    </div>
    @include('pages.footer')

</body>
</html>

