@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->



                        
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
            
                    <div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
  
        <!-- ./breadcrumb -->
                  
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
               <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">
                        
                               Wish List
                               
                                </span>
                 
                </h2>
                <!-- ../page heading-->
          
                <ul class="row list-wishlist">
                 <?php
                            $wishlist = DB::table('wishlists')
                                                ->orderBy('id', 'desc')
                                                ->where('customer_id', Session::get('customer_id'))->get();
                            
                                foreach ($wishlist as $wishlist_info){
                                $product_id=$wishlist_info->product_id;
                                $product= DB::table('products')                                                
                                                ->where('id', $product_id)->first();
                                $product_image= DB::table('product_images')                                                
                                                ->where('product_id', $product_id)->first();
                        ?>
                    <li class="col-sm-3">
                        <div class="button-action pull-right">
                          
                            <a href="{{URL::to('/remove-wishlist/'.$wishlist_info->id)}}"><i class="fa fa-close"></i></a>
                        </div>
                        <div class="product-img">
                            <a href="{{URL::to('product-details/'.$wishlist_info->product_id)}}"><img src="{{ asset('product_image/'.$product_image->product_image) }}" alt="Product" style="height: 250px;"></a>
                        </div>
                        <br>
                        <h4 align="center" class="product-name">
                            <a href="{{URL::to('product-details/'.$wishlist_info->product_id)}}">{{$product->product_name}}</a>
                        </h4>
                     
                     
                       
                    </li>
                 <?php }?>  
        
                </ul>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection