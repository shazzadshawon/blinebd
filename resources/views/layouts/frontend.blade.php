<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

    <head>
        <title>Bline-BD</title>
        
        @include('frontend/include/head')
    </head>
      {{-- <body id="cms" class="cms cms-3 cms-terms-and-conditions-of-use hide-right-column lang_en"> --}}
    <body id="index" class="index hide-left-column hide-right-column lang_en">

    <div id="page">
        <div class="header-container">
            
            @include('frontend/include/header')
        </div>
       @yield('content')

        <!-- Footer -->
      
        @include('frontend/include/footer')
        <!-- #footer -->
    </div><!-- #page -->
    <a class="top_button" href="#" style="display:none;">&nbsp;</a>

   
    @include('frontend/include/foot')

    </body>
</html>