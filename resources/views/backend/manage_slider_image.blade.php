@extends('layouts.backend')
@section('content')
<div class="col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Slider Images</h2>
             <div class="pull-right">
                <a class="btn btn-primary" href="{{ url('add-slider-image') }}">Add Slider Image</a>
            </div>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                   

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th> #</th>
                        <th>Image</th>                     
                        <th style="width: 50%">Caption</th>                     
                        {{-- <th>Subtitle</th>  --}}                    
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($slider_image as $slider_image_info){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td class="center"><img src="<?php echo 'slider_image/'.$slider_image_info->slider_image;?>" style="height: 50px; width: 100px;"></td> 
                        <td>
                            @php
                                print_r($slider_image_info->title);
                            @endphp
                        </td>                    
                       {{--   <td>
                            {{ $slider_image_info->subtitle }}
                        </td>      --}}                  
                        <td class="center">
                            <?php 
                            if($slider_image_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-important">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($slider_image_info->publication_status==1){
                            ?>
                            <a class="btn btn-primary" href="{{URL::to('/unpublished-slider-image/'.$slider_image_info->slider_image_id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  unpublish
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-primary" href="{{URL::to('/published-slider-image/'.$slider_image_info->slider_image_id)}}">
                                <i class="halflings-icon thumbs_up"></i>  published
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/slider-image/'.$slider_image_info->slider_image_id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  Edit
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-slider-image/'.$slider_image_info->slider_image_id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++; }?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection