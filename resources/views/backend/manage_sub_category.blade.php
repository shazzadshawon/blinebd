@extends('layouts.backend')
@section('content')
<div class="col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Sub Category</h2>
             <div class="pull-right">
                                        <a class="btn btn-primary" href="{{ url('add-sub-category') }}">Add Sub Category</a>
                                    </div>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>Sub Category Name</th> 
                        <th> Category</th>        
                         
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($SubCategories as $SubCategory_info){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td class="center"><?php echo $SubCategory_info->sub_category_name;?></td>
                        <td class="center"><?php echo $SubCategory_info->category_name;?></td> 
                        <td class="center">
                            <?php 
                            if($SubCategory_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-default">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($SubCategory_info->publication_status==1){
                            ?>
                            <a class="btn btn-primary" href="{{URL::to('/unpublished-sub-category/'.$SubCategory_info->sub_category_id)}}">
                                <i class="halflings-icon white thumbs_down"></i> unpublished 
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-primary" href="{{URL::to('/published-sub-category/'.$SubCategory_info->sub_category_id)}}">
                                <i class="halflings-icon thumbs_up"></i>  published
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/sub-category/'.$SubCategory_info->sub_category_id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  Edit
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-sub-category/'.$SubCategory_info->sub_category_id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++; }?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection