@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">


    <div class="panel panel-success">
         <div class="panel-heading" data-original-title>
            <h4><i class="fa fa-edit"></i> Edit Slider Image</h4>
            <div class="box-icon">
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="panel-body">
                         
        <div class="row">
            <div class="col-md-8">
                {!! Form::model($slider_image, ['route' => ['slider-image.update',$slider_image->slider_image_id], 'method' => 'PUT', 'files'=>true,'name'=>'edit_slider_image','class'=>'form-horizontal']) !!}
                <fieldset>


                     {{-- <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Title</label>
                        <div class="col-md-10">
                            <input type="text"  name="title" class="form-control" id=""  value="{{}}" data-items="" >
                           
                          
                        </div>
                    </div> --}}

                       <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Image</label>
                        <div class="col-md-10">
<!--                            {!! Form::file('slider_image') !!}-->
                            <input type="file"  name="slider_image" class="" id="typeahead"  data-provide="typeahead" data-items="4" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}" >
                           {{--  <img src="{{ asset('slider_image/'.$slider_image->slider_image) }}" style="height: 100px; width: 150px;" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}"> --}}
                          
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Caption</label>
                        <div class="col-md-10">
                            {{-- <input type="text"  name="title" class="form-control" id=""  data-provide="" data-items="" > --}}
                            <textarea  name="editor1" class="form-control" id="" >
                                @php
                                    print_r( $slider_image->title );
                                @endphp
                            </textarea>
                           
                          
                        </div>
                    </div>



                    {{--   <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Subtitle</label>
                        <div class="col-md-10">
                            <input type="text"  name="subtitle" class="form-control" value="{{ $slider_image->subtitle }}" >
                           
                          
                        </div>
                    </div> --}}

                 
                   
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                            @if($slider_image->publication_status == 1)
                                <option value="1" selected>Published</option>
                                <option value="0">Unpublished</option>
                            @else
                                <option value="0" selected>Unpublished</option>
                                <option value="1" >Published</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                     <label class="control-label col-md-2" for="typeahead"></label>
                     <div class="col-md-10">
                         
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                     </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
            <div class="col-md-4">
                 <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('slider_image/'.$slider_image->slider_image)}}" />
            </div>
        </div>
        </div>
    </div>


       

    </div>
</div>
<script>
    document.forms['edit_slider_image'].elements['publication_status'].value = {{$slider_image->publication_status}};    
</script>
@endsection

