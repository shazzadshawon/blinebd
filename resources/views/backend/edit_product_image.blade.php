@extends('layouts.backend')
@section('content')
<div class="row sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
              @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif
        
        <div class="box-content">
            <div class="box-content">
<!--             	{!! Form::model($product_image, ['route' => ['/update-product/',$product_image->product_id,'/image/',$product_image->slider_image_id], 'method' => 'PUT', 'files'=>true,'name'=>'edit_slider_image']) !!}-->
                {!! Form::open(array('url'=>'add-product/'.$product_image->product_id,'method'=>'PUT', 'files'=>true)) !!}
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Select Images</label>
                        <div class="controls">
<!--                            {!! Form::file('slider_image', array('multiple'=>true)) !!}-->
                            <input type="file"  name="slider_image" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                            <img src="/{{$product_image->product_image}}" style="height: 100px; width: 150px;">
                          
                        </div>
                    </div>

                    
                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status</label>
                        <div class="controls">
                            <select name="publication_status">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_slider_image'].elements['publication_status'].value = '{{$slider_image->publication_status}}';    
</script>
@endsection

