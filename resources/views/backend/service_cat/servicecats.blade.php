@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Service Categories</h3>
                                  <div class="pull-right">
                                      <a class="btn btn-success" href="{{url('addservicecategory')}}">Add Service Category</a>
                                  </div>                   
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                               <!--  <th>Image</th> -->
                                               {{--  <th style="width: 60%">Sub category</th>
                                                 --}}
                                               <!--  <th style="width: 50%">Description</th> -->
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($service_cats as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->cat_name }}</td>
                                              
                                                <td>
                                                    <a href="{{ url('editservicecategory/'.$cat->id) }}" class="btn btn-primary">Edit</a>
                                                  <!--   <a href="{{ url('singleservicecategory/'.$cat->id) }}" class="btn btn-warning">View</a>  -->
                                                     
<!-- 
                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete</button>
                                                    -->

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>All services from this Category will be also deleted.</p>                    
                        <p>Are you sure you want to delete this item ?</p>                    
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deleteservice/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection