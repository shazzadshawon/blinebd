@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-primary">
        <div class="panel-heading">
             <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Slider Image</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        </div>
        <div class="panel-body">
                    <div class="col-md-8">
            <div class="">
                {!! Form::open(['route' => 'slider-image.store','files'=>true, 'class'=>'form-horizontal']) !!}

                <fieldset>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Select Images</label>
                        <div class="col-md-10">
                            {{-- {!! Form::file('slider_image', array('multiple'=>false, 'class' => 'form-control')) !!} --}}
                            <input type="file" name="slider_image" class="" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}">
                           
                        </div>
                    </div>

                      <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Caption</label>
                        <div class="col-md-10">
                            {{-- <input type="text"  name="title" class="form-control" id=""  data-provide="" data-items="" > --}}
                            <textarea  name="editor1" class="form-control" id="" ></textarea>
                           
                          
                        </div>
                    </div>
                    {{--   <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Subtitle</label>
                        <div class="col-md-10">
                            <input type="text"  name="subtitle" class="form-control" id=""  data-provide="" data-items="" >
                           
                          
                        </div>
                    </div>
 --}}
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">

                        <label class="control-label col-md-2" for="date01"></label>
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
        <div class="col-md-4">
              <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/demo.png')}}" />
        </div>
        </div>
    </div>
       
             
        

    </div>
</div>
@endsection