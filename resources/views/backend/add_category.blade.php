@extends('layouts.backend')
@section('content')

<div class="row">
    <div class="box col-md-12">
       
        <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Main Category</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
{!! Form::open(['route' => 'category.store', 'class' => 'form-horizontal']) !!}
                     

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="typeahead">Category Name</label>
                        <div class="col-md-10">
                            <input type="text"  name="category_name" class="form-control" id=""  data-provide="" >
                           
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2" for="">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>                    
                  {{--   <div class="form-group">
                        <label class="control-label col-md-2" for="" class="control"></label>
                        <input type="checkbox" name="mega" value="1" class=""> Add to Mega menu<br>
                    </div> --}}

                    <div class="form-group">
                        <label class="control-label col-md-2" for="" class="control"></label>
                         <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>

                {!! Form::close() !!}

             </div>
                 </div>
             </div>
             
      
    </div>
</div>
@endsection