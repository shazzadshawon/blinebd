@extends('layouts.backend')
@section('content')
<div class="col-md-12">


    <table class="table table-striped">
        <tr>
            <td colspan="2" style="text-align: center; color: #EB3C00;"><h2>Product Details</h2>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ url(htmlspecialchars($login_link)) }}">Post To Facebook</a>
            </div>
            </td>
        </tr>
        <tr>   
            <td>
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <tr>
                        <td>Product Name</td>
                        <td>{{ $product_info->product_name}}</td>
                    </tr>
                    <tr>
                        <td>Category Name</td>
                        <td>{{ $product_info->category_name}}</td>
                    </tr>
                    <tr>
                        <td>Sub Category Name</td>
                        <td>{{ $product_info->sub_category_name}}</td>
                    </tr>
                    <tr>
                        <td>Product Price</td>
                        <td>{{ $product_info->product_price}} Tk</td>
                    </tr>
                    <tr>
                        <td>Product Code</td>
                        <td>{{ $product_info->product_code}}</td>
                    </tr>
                    <tr>
                        <td>Product Quantity</td>
                        <td>{{ $product_info->product_quantity}}</td>
                    </tr>

                </table>
            </td>
            <td>
                <table class="table table-striped table-bordered bootstrap-datatable datatable">  
                    <tr>
                        <td>Product Discount</td>
                        <td>{{ $product_info->discount}}%</td>
                    </tr>                

                    <tr>
                        <td>Product Offer Status</td>
                        <td>
                            @if($product_info->offer_status==2)
                            <h3>New Arrivals</h3>
                            @elseif($product_info->offer_status==3)
                            <h3>Best seller</h3>
                          
                            @elseif($product_info->offer_status==1)
                            <h3>Featured</h3>
                            
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Publication Status</td>
                        <td>
                            @if($product_info->publication_status==1)
                            <h3>Published</h3>
                            @elseif($product_info->publication_status==2)
                            <h3>Unpublished</h3>
                            @endelseif
                            @elseif($product_info->publication_status==3)
                            <h3>Stock Out</h3>
                            @endelseif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Date Created</td>
                        <td>{{date('M j, Y h:ia', strtotime($product_info->created_at))}}</td>
                    </tr>
                    <tr>
                        <td>Date Updated</td>
                        <td>{{date('M j, Y h:ia', strtotime($product_info->updated_at))}}</td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr>
            <td>Short Description</td>
            <td><?php echo  $product_info->description_bn;?></td>
        </tr>
        <tr>
            <td>Full Description</td>
            <td><?php echo  $product_info->description;?></td>
        </tr>
        <tr>
            <td>         

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Product Image</th>
                            <th>Action</th>                                          
                        </tr>
                    </thead>   
                    <tbody>
                     @php $i=1 @endphp
                    @foreach($image as $product_image)
                        <tr>
                            <td>{{$i}}</td>                        
                            <td class="center"><img src="{{ asset('product_image/'.$product_image->product_image) }}" style="height: 70px; width:150px;"></td>
                            <td class="center">
                              <a class="btn btn-danger" href="{{URL::to('/delete-product/'.$product_image->product_id.'/image/'.$product_image->product_image_id)}}" onclick="return checkDelete();">
                                <i class="fa fa-remove"></i> Remove
                            </a>
<!--                             <a class="btn btn-info" href="{{URL::to('/edit-product-image/'.$product_image->product_image_id)}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>-->
                            </td>                                        
                        </tr>       
                        @php $i++ @endphp
                   @endforeach
                    </tbody>
                </table>                    

            </td>
            <td>
              <div class="box-content">
            <div class="box-content">
<!--             	{!! Form::open(['route' => 'slider-image.store','files'=>true]) !!}
                    {!!   Form::open(['url' => 'foo/bar']) !!}-->
{!! Form::open(array('url'=>'/add-product/'.$product_info->id.'/image','method'=>'POST', 'files'=>true)) !!}
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Add Product  Images</label>
                        <div class="controls">
                            {!! Form::file('product_image[]', array('multiple'=>true)) !!}
<!--                            <input type="file"  name="slider_image[]" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>-->
                           
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
            </td>
        </tr>
                {{--  ------------------Size------------------ --}}
        <tr>
            <td>         

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Product Size</th>
                            <th>Action</th>                                          
                        </tr>
                    </thead>   
                    <tbody>
                     @php
                      $i=1;
                      $size = DB::table('product_sizes')
                                       ->where('product_id',$product_info->id)                                  
                                       ->get();
                     @endphp
                    @foreach($size as $sizeInfo)
                        <tr>
                            <td>{{$i}}</td>                        
                            <td class="center">{{ $sizeInfo->size }}</td>
                            <td class="center">
                              <a class="btn btn-danger" href="{{URL::to('/delete-product/'.$sizeInfo->product_id.'/size/'.$sizeInfo->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> Remove
                            </a>
<!--                             <a class="btn btn-info" href="{{URL::to('/edit-product-image/'.$product_image->product_image_id)}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>-->
                            </td>                                        
                        </tr>       
                        @php $i++ @endphp
                   @endforeach
                    </tbody>
                </table>                    

            </td>
            <td>
              <div class="box-content">
            <div class="box-content">

{!! Form::open(array('url'=>'/add-product/'.$product_info->id.'/size','method'=>'POST')) !!}
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Size Name</label>
                        <div class="controls">
                            
                         <input type="text"  name="size" class="span6 typeahead" placeholder="XL,L,M,S eg">
                           
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
            </td>
        </tr>
    </table>

</div>
@endsection