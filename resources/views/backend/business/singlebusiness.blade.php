@extends('layouts.backend')

@section('content') 
    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            
                            <div class="panel panel-primary">
                            <div class="panel-heading">
                             <div class="post-">
                                           <h4> {{ $blog->blog_title }}</h4>
                                           
                                        </div></div>
                                <div class="panel-body posts">
                                            
                                    <div class="post-item">
                                       
                                         <div align="right" >
                                                 <?php
                                                        // echo '<a class="btn btn-info" href="' . htmlspecialchars($login_link) . '">Post to Facebook</a>';
                                                    ?>
                                            </div>
                                        <!--<div class="post-date"><span class="fa fa-calendar"></span>{{ $blog->created_at }}</div>-->
                                        <div class="post-text">                                            
                                        <img style="width: 100%; height: 300px" class="img img-responsive" src="{{asset('public/uploads/blog/'.$blog->blog_image)}}">
                                            <p>
                                                <?php print_r($blog->blog_description); ?>
                                            </p>
                                        </div>
                                       
                                    </div>                                            
                                            
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
@endsection