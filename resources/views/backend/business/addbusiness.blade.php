@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Business</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                        <div class="row">
                            <div class="col-md-8">
                                
                                 <form class="form-horizontal" method="POST" action="{{ url('storebusiness') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="title"/>
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Logo</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="name" value="{{ old('name') }}" required autofocus onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}">
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Link </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea class="form-control" name="link" rows="2"></textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>


                            </div>
                            <div class="col-md-4">
                                <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/demo.png')}}" />
                            </div>
                        </div>
                                  
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection