@extends('layouts.backend')
@section('content')
<div class="row ">
    <div class="col-md-8">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Sub Category</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
             
        <div class="">
            <div class="">
             	{!! Form::open(['route' => 'sub-category.store',  'class'=>'form-horizontal']) !!}
                <fieldset>
                      <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Name</label>
                        <div class="col-md-10">
                            <input type="text"  name="sub_category_name" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Main Category</label>
                        <div class="col-md-10">                            
                            <select name="category_id" class="form-control">
                                <option >====Select Main Category===</option>
                                @foreach ($categories as $category_info)
                                <option value="{{$category_info->category_id}}">{{$category_info->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                  


                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-2" for="date01"></label>
                       <div class="col-md-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                       </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection