@extends('layouts.backend')
@section('content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="text-align: center; color: green;"> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Sub Category id</th>
                        <th>Category Name</th>        
                         <th>Sub Category Name</th>
                         <th>সাব ক্যাটাগরি নাম </th>        
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    foreach ($SubSubCategories as $SubCategory_info){
                    ?>
                    <tr>
                        <td><?php echo $SubCategory_info->id;?></td>
                        <td class="center"><?php echo $SubCategory_info->sub_category_name;?></td> 
                        <td class="center"><?php echo $SubCategory_info->sub_sub_category_name;?></td> 
                        <td class="center"><?php echo $SubCategory_info->sub_sub_category_name_bn;?></td> 
                        <td class="center">
                            <?php 
                            if($SubCategory_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-important">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($SubCategory_info->publication_status==1){
                            ?>
                            <a class="btn btn-success" href="{{URL::to('/unpublished-sub-sub-category/'.$SubCategory_info->id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-danger" href="{{URL::to('/published-sub-sub-category/'.$SubCategory_info->id)}}">
                                <i class="halflings-icon thumbs_up"></i>  
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/sub-sub-category/'.$SubCategory_info->id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-sub-sub-category/'.$SubCategory_info->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    
                    <?php }?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection