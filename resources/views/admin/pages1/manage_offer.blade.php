@extends('layouts.backend')
@section('content')

<div class="col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Manage Offers</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                          
                         <th>Image</th>
                         <th>Main Category</th> 
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    $offer = DB::table('pazzles')->get();

                    foreach ($offer as $offerInfo){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        
                        <td>
                            <img src="{{ asset('pazzle_image/'.$offerInfo->pazzle_image) }}" style="height: 80px;">
                        </td>
                         <td>
                         @php
                             
                             $category = DB::table('categories')->where('category_id',$offerInfo->pazzle)->first();
                         @endphp
                            {{ $category->category_name }}
                         </td>
                        <td class="center">
                            <?php 
                            if($offerInfo->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-danger">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($offerInfo->publication_status==1){
                            ?>
                            <a class="btn btn-primary" href="{{URL::to('/unpublished-offer/'.$offerInfo->id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  Unpublish
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-primary" href="{{URL::to('/published-offer/'.$offerInfo->id)}}">
                                <i class="halflings-icon thumbs_up"></i>  Publish
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/offer/'.$offerInfo->id.'/edit')}}">
                                <i class="halflings-icon white edit"></i> Edit
                            </a>
                            {{--  <a class="btn btn-info" href="{{URL::to('/setmegaoffer/'.$offerInfo->id)}}">
                                Set as Mega offer  
                            </a> --}}
                            <a class="btn btn-danger" href="{{URL::to('/delete-offer/'.$offerInfo->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++; }?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection