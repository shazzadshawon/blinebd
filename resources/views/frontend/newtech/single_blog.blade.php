@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-6 col-xs-12">
                    <h1>{{$blog->blog_title}}</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-6 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">Service</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--Sidebar Page Container-->
    <section class="sidebar-page-container">
        <div class="auto-container">
            <div class="row">

                <!--Content Side-->
                <div class="content-side  col-lg-9 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="blog-section">
                        <div class="news-block-detail">
                            <div class="inner-box">
                                <figure class="image">
                                    <img style="height: 300px; width: 100%" src="{{asset('public/uploads/blog/'.$blog->blog_image)}}" alt="" />
                                </figure>
                                <!--Post Info Box-->
                                {{--<div class="post-info-box clearfix">--}}
                                    {{--<div class="pull-left">--}}
                                        {{--<div class="date">By <span class="dark-text">Johny Britt</span> <span>Nov 11, 2016</span></div>--}}
                                    {{--</div>--}}

                                {{--</div>--}}

                                <!--lower content-->
                                <div class="lower-content">
                                    <h3>{{$blog->blog_title}}</h3>
                                    <div class="text">
                                        <blockquote> <?php print_r($blog->blog_description)?> </blockquote>
                                         </div>
                                    <!--New Posts-->

                                </div>

                            </div>
                        </div>



                    </div>
                </div>


            </div>
        </div>
    </section>




@endsection