@extends('layouts.frontend-newtech')

@section('content')

        @include('frontend.include.main_slider')
        <!--End Main Slider-->

        <!--Message of MD-->
        @include('frontend.include.message_of_md')
        <!--End of Message of MD-->

        <!--Testimonial Section-->
        @include('frontend.include.our_business_section')
        <!--End Testimonial Section-->

        <!--Services Section / Style Two-->
        <!--        -->
        {{--@include('frontend.include.post_slider_section')--}}
        <!--End Services Section-->

        <!--Media Section-->
        @include('frontend.include.media_section')
        <!--End Media Section-->

        <!--News Section-->
        @include('frontend.include.blog_section')
        <!--End News Section-->

        <!--client Section-->
        @include('frontend.include.client_slider')


      <!--  -->

@endsection 