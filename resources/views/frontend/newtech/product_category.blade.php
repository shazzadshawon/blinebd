@extends('layouts.frontend-newtech')

@section('content')
      <!--Page Title-->
      <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
            <div class="auto-container">
                  <div class="row clearfix">
                        <!--Title -->
                        <div class="title-column col-md-6 col-sm-12 col-xs-12">
                              <h1>{{ $subCategory->sub_category_name }}</h1>
                        </div>
                        <!--Bread Crumb -->
                        <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                              <ul class="bread-crumb clearfix">
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <li class="active">Shop</li>
                                    <li class="active">{{ $subCategory->sub_category_name }}</li>
                              </ul>
                        </div>
                  </div>
            </div>
      </section>
      <!--End Page Title-->

      <div class="sidebar-page-container shop-container">
            <div class="auto-container">
                  <div class="row clearfix">
                        <!--Content Side-->
                        {{--<div class="content-side  col-md-10 col-md-offset-1">--}}
                        <div class="content-side  col-md-9 pull-right">
                              <div class="shop-section">

                                    <!--Sort By-->
                                    {{--<div class="items-sorting">--}}
                                          {{--<div class="row clearfix">--}}
                                                {{--<div class="results-column col-md-6 col-sm-6 col-xs-12">--}}
                                                      {{--<h2> {{ $subCategory->sub_category_name }}</h2>--}}
                                                {{--</div>--}}
                                                {{--<div class="select-column pull-right col-md-4 col-sm-4 col-xs-12">--}}
                                                      {{--<div class="form-group">--}}
                                                            {{--<select name="sort-by">--}}
                                                                  {{--<option>Default Sorting</option>--}}
                                                                  {{--<option>By Order</option>--}}
                                                                  {{--<option>By Price</option>--}}
                                                            {{--</select>--}}
                                                      {{--</div>--}}
                                                {{--</div>--}}
                                          {{--</div>--}}
                                    {{--</div>--}}

                                    <!--Shop Items-->
                                    <div class="shop-items">
                                          <div class="row clearfix">
                                          @foreach($products as $product)
                                                <!--Shop Item-->
                                                <div class="shop-item col-lg-4 col-md-6 col-sm-3 col-xs-12">
                                                      <div class="inner-box">
                                                            <div class="image-box">
                                                                  <?php
                                                                  $image = DB::table('product_images')->where('product_id',$product->id)->orderby('product_image_id','desc')->first();
                                                                ?>
                                                                  <a href="{{url('product-details/'.$product->id)}}"><img style="height:270px;" src="{{asset('public/uploads/product/'.$image->product_image)}}" alt="" /></a>
                                                                  <div class="overlay-box">
                                                                        {{--<div class="item-options clearfix">--}}
                                                                              {{--<a href="" class="option-btn"><span class="flaticon-shopping-cart-1"></span><div class="tool-tip">Add to cart</div></a>--}}
                                                                              {{--<a href="" class="option-btn"><span class="flaticon-heart-1"></span><div class="tool-tip">Add to Fav</div></a>--}}
                                                                        {{--</div>--}}
                                                                  </div>
                                                            </div>
                                                            <div class="lower-box">
                                                                  <h4><a href="{{url('product-details/'.$product->id)}}">{{$product->product_name}}</a></h4>
                                                                  <div class="price">$
                                                                        @if(!empty($product->discount))
                                                                              {{$product->product_price - ($product->product_price * $product->discount/100)}} ({{$product->discount }}% off)
                                                                        @else
                                                                              {{$product->product_price}}
                                                                        @endif
                                                                  </div>
                                                                  <!--Rating-->
                                                                  {{--<div class="rating">--}}
                                                                        {{--<span class="fa fa-star"></span>--}}
                                                                        {{--<span class="fa fa-star"></span>--}}
                                                                        {{--<span class="fa fa-star"></span>--}}
                                                                        {{--<span class="fa fa-star"></span>--}}
                                                                        {{--<span class="fa fa-star-o"></span>--}}
                                                                  {{--</div>--}}
                                                            </div>
                                                      </div>
                                                </div>
                                          @endforeach

                                          </div>
                                    </div>
                              {{ $products->links() }}
                                    <!--styled pagination-->


                              </div>
                        </div>

                        <!--Sidebar Side-->
                        <div class="sidebar-side pull-left col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="sidebar shop-sidebar">

                                    <!--Search Box-->
                              {{--<div class="sidebar-widget search-box">--}}
                              {{--<div class="sidebar-title">--}}
                              {{--<h2>SEARCH PRODUCTS</h2>--}}
                              {{--</div>--}}
                              {{--<form method="post" action="http://wp.hostlin.com/nijmegan/shop.html">--}}
                              {{--<div class="form-group">--}}
                              {{--<input type="search" name="search-field" value="" placeholder="Search Products...">--}}
                              {{--<button type="submit"><span class="icon fa fa-search"></span></button>--}}
                              {{--</div>--}}
                              {{--</form>--}}
                              {{--</div>--}}

                              <!--Category Widget-->
                                    <div class="sidebar-widget category-widget">
                                          <div class="sidebar-title">
                                                <h2>PRODUCT CATEGORIES</h2>
                                          </div>
                                          <ul class="list-two style-two">
                                                @foreach($categories as $cat)
                                                      <li><a href="{{url('product_category/'.$cat->sub_category_id)}}">{{$cat->sub_category_name}}</a></li>
                                                @endforeach
                                          </ul>
                                    </div>

                              </div>
                        </div>

                  </div>
            </div>
      </div>
@endsection