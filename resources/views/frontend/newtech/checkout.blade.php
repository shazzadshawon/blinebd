@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->
    {!! Form::open(['route' => 'Order.store']) !!}
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>Checkout</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">Shop</li>
                        <li class="active"></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--Checkout Page-->
    <div class="checkout-page">
        <div class="auto-container">
            {{--<!--Default Links-->--}}
            {{--<ul class="default-links">--}}
                {{--<li>Existing Customer? <a href="#">Click here to login</a></li>--}}
                {{--<li>Have a coupon? <a href="#">Click here to enter your code</a></li>--}}
            {{--</ul>--}}

            <!--Billing Details-->
            <div class="billing-details">
                <div class="shop-form">
                    <form method="post" action="">
                        <div class="row clearfix">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <div class="default-title"><h2>Billing Details</h2></div>
                                <div class="row clearfix">

                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">First name <sup>*</sup></div>
                                        <input type="text" name="fname" value="" placeholder="" required>
                                    </div>

                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Last name <sup>*</sup></div>
                                        <input type="text" name="lname" value="" placeholder="" required>
                                    </div>

                                    <!--Form Group-->
                                    {{--<div class="form-group col-md-12 col-sm-12 col-xs-12">--}}
                                        {{--<div class="field-label">Company name </div>--}}
                                        {{--<input type="text" name="field-name" value="" placeholder="">--}}
                                    {{--</div>--}}

                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="field-label">Address <sup>*</sup></div>
                                        <input type="text" name="address" value="" placeholder="Street address" required>
                                    </div>

                                    <!--Form Group-->
                                    {{--<div class="form-group col-md-6 col-sm-6 col-xs-12">--}}
                                        {{--<div class="field-label">Town / City <sup>*</sup></div>--}}
                                        {{--<input type="text" name="field-name" value="" placeholder="">--}}
                                    {{--</div>--}}

                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Postcode / Zip <sup>*</sup></div>
                                        <input type="text" name="zipcode" value="" placeholder="" required>
                                    </div>

                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Email <sup>*</sup></div>
                                        <input type="text" name="email" value="" placeholder="" required>
                                    </div>

                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Phone <sup>*</sup></div>
                                        <input type="text" name="phone" value="" placeholder="" required>
                                    </div>

                                    <!--Form Group-->
                                    {{--<div class="form-group col-md-12 col-sm-12 col-xs-12">--}}
                                        {{--<div class="field-label">Country <sup>*</sup> </div>--}}
                                        {{--<select name="country">--}}
                                            {{--<option>United Kingdom (UK)</option>--}}
                                            {{--<option>Pakistan</option>--}}
                                            {{--<option>USA</option>--}}
                                            {{--<option>CANADA</option>--}}
                                            {{--<option>INDIA</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                                        {{--<div class="check-box"><input type="checkbox" name="shipping-option" id="account-option"> &ensp; <label for="account-option">Creat an account?</label></div>--}}
                                    {{--</div>--}}

                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="default-title"><h2>Additional Information</h2></div>
                                <div class="form-group">
                                    <div class="field-label">Order note </div>
                                    <textarea placeholder="Notes about your order, e.g. special notes for delivery" name="notes"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--End Billing Details-->

            <div class="default-title"><h2>Your Order</h2></div>


                <div class="sidebar-page-container shop-container">
                    <div class="auto-container">
                        <div class="cart-section">
                            <!--Cart Outer-->
                            <div class="cart-outer">
                                <div class="table-outer">
                                    <table class="cart-table">
                                        <thead class="cart-header">
                                        <tr>
                                            <th class="prod-column">PRODUCT</th>
                                            <th>&nbsp;</th>
                                            <th>QUANTITY</th>
                                            <th class="price">Price</th>
                                            <th>Total</th>
                                            <th>Remove</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        $total=0;
                                        ?>
                                        @foreach($items as $item)
                                            @php
                                                $product = DB::table('products')->where('id',$item->product_id)->first();
                                                $image = DB::table('product_images')->where('product_id',$item->product_id)->first();

                                            @endphp
                                            <tr>
                                                <td colspan="2" class="prod-column">
                                                    <div class="column-box">
                                                        <figure class="prod-thumb"><img src="{{asset('public/uploads/product/'.$image->product_image)}}" alt=""></figure>
                                                        <h4 class="prod-title">{{$product->product_name}}</h4>
                                                    </div>
                                                </td>
                                                <td class="qty">{{$item->product_quantity}}</td>
                                                <td class="price">{{$item->product_price}}</td>
                                                <td class="sub-total">{{$item->product_price}}</td>
                                                <td class="remove"><a href="{{url('remove-cart-product/'.$item->id)}}" class="remove-btn"><span class="flaticon-garbage"></span></a></td>
                                            </tr>
                                            @php
                                                $total+=$item->product_price;
                                            @endphp
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>





                            </div>
                        </div>
                    </div>
                </div>




                <!--Place Order-->
            <div class="place-order pull-right">
                {{--<div class="default-title"><h2>Payment Method</h2></div>--}}


                <!--Payment Options-->
                {{--<div class="payment-options">--}}
                    {{--<ul>--}}
                        {{--<li>--}}
                            {{--<div class="radio-option">--}}
                                {{--<input type="radio" name="payment-group" id="payment-1" checked>--}}
                                {{--<label for="payment-1"><strong>Cheque Payment</strong><span class="small-text">Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</span></label>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<div class="radio-option">--}}
                                {{--<input type="radio" name="payment-group" id="payment-3">--}}
                                {{--<label for="payment-3"><strong>Paypal</strong></label>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<div class="radio-option">--}}
                                {{--<input type="radio" name="payment-group" id="payment-2">--}}
                                {{--<label for="payment-2"><strong>Direct Bank Transfer</strong></label>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}

                <button type="submit" class="theme-btn btn-style-two place-order">PLACE ORDER</button>

            </div>
            <!--End Place Order-->

        </div>
    </div>
    {!! Form::close() !!}
@endsection