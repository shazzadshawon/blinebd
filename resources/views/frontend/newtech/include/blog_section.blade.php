<section class="news-section" style="background-image:url(public/frontend/images/background/2.jpg)">
    <div class="auto-container">

        <!--Sec Title Two-->
        <div class="sec-title-four">
            <div class="title">From Blog</div>
            <h2>LATEST NEWS</h2>
        </div>

        <div class="row clearfix">

            <!--News Block Two-->
            <?php foreach ($blogs as $blog): ?>
            <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="image-box">
                        <a href="{{url('single_blog/'.$blog->id)}}"><img style="height: 300px" src="{{asset('public/uploads/blog/'.$blog->blog_image)}}" alt="" /></a>
                    </div>
                    <div class="lower-box">
                        <h3><a href="{{url('single_blog/'.$blog->id)}}">{{$blog->blog_title}}</a></h3>
                        <ul class="post-meta">
                           
                            <li class="date">{{ date(" jS \of F, Y", strtotime($blog->created_at)) }}</li>
                        </ul>
                        <div class="text">
                        <?php print_r($blog->blog_description); ?>
                        </div>
                    </div>
                </div>
            </div>
 
            <?php endforeach ?>
          

        </div>

    </div>
</section>