<footer class="main-footer">
    <div class="auto-container">

        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">

                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo-box">
                                    <a href="{{url('/')}}"><img src="{{asset('public/frontend/images/newtech_logo_white.png')}}" alt="" /></a>
                                </div>
                                <div class="widget-content">
                                    <div class="text">
                                        <p>NEW-TECH INTERNATIONAL LTD</p>
                                        <p>Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Text Dummy Text Dummy Text Dummy Text</p>
                                    </div>
                                </div>
                                <div class="social-links-two">
                                    <a href="#"><span class="fa fa-facebook-f"></span></a>
                                    <a href="#"><span class="fa fa-twitter"></span></a>
                                    <a href="#"><span class="fa fa-google-plus"></span></a>
                                    <a href="#"><span class="fa fa-pinterest-p"></span></a>
                                    <a href="#"><span class="fa fa-linkedin"></span></a>
                                </div>
                                <br><br>
                                <a href="{{url('problem')}}" class="btn btn-primary btn-block">Employee Area</a>
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h2>Contact Us</h2>
                                <ul class="contact-info">
                                    <li><div class="icon"><span class="flaticon-location-pin"></span></div>House# 457, Road# 31, DOHS Mohakhali , Dhaka-1206</li>
                                    <li><div class="icon"><span class="flaticon-smartphone-1"></span></div>+880 1715113358</li>
                                    <li><div class="icon"><span class="flaticon-e-mail-envelope"></span></div>contact@newtech.bd.com</li>
<!--                                    <li><div class="icon"><span class="flaticon-clock"></span></div>Mon - Sat: 10am - 5pm</li>-->
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget twitter-widget">
                                <h2>Our Achievements</h2>
                                <?php
                                $achievements = DB::table('achievements')->take(3)->get();
                                ?>
                                <div class="widget-content">
                                    @foreach($achievements as $achievement)
                                    <div class="feed">
                                        <div class="month"><a href="{{url('achievements')}}">{{$achievement->title}}</a></div>
                                        <span class="text"><?php print_r(substr($achievement->description,0,50))?></span>
                                        {{--<div class="date-box">Nov  07, 2016</div>--}}
                                    </div>
                                   @endforeach
                                </div>
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget carousel-widget">
                                <h2>Gallery</h2>
                                <?php

                                $medias = DB::table('galleries')->orderBy('id','desc')->get();
                                $i=0;
                                ?>
                                <div class="row">
                                    @foreach($medias as $gal)
                                        <div class="col-md-4">
                                            <img style="height: 100px; width: 100px" src="{{asset('public/uploads/gallery/'.$gal->gallery_image)}}" alt="">
                                        </div>
                                        <?php
                                        if($i>4)
                                            break;
                                        $i++;
                                        ?>
                                    @endforeach
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <!--Footer Bottom-->
    <div class="footer-bottom">
        <div class="auto-container"><div class="copyright">Created with <i class="fa fa-heart" style="color:red" aria-hidden="true"></i> <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a></div></div>
    </div>

</footer>