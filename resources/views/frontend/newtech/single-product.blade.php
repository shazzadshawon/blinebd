@extends('layouts.frontend-newtech')

@section('content')

    <!--Page Title-->
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>{{ $product->product_name }}</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">Shop</li>
                        <li class="active">{{ $product->product_name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <div class="sidebar-page-container shop-container">
        <div class="auto-container">
            <div class="row clearfix">

                @php
                    $image = DB::table('product_images')->where('product_id',$product->id)->first();
                    $wishlist = DB::table('wishlists')
                            ->where('product_id', $product->id)
                            ->where('customer_id', Session::get('customer_id'))->first();
                @endphp

                <!--Content Side -->
                <div class="content-side pull-right col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="shop-single">
                        <div class="product-details">

                            <!--Basic Details-->
                            <div class="basic-details">
                                <div class="row clearfix">





                                    <div class="image-column col-md-6 col-sm-6 col-xs-12">
                                        <figure class="image-box"><a href="#" class="lightbox-image" title="Image Caption Here"><img src="{{asset('public/uploads/product/'.$image->product_image)}}" alt=""></a></figure>
                                    </div>
                                    <div class="info-column col-md-6 col-sm-6 col-xs-12">
                                        <div class="details-header">
                                            <h4>Product Name</h4>
                                            <div class="item-price">&#2547; {{$product->product_price}}</div>
                                        </div>

                                        <div class=""><label>availability:</label><span class="">
                @if ($product->product_quantity > 0)In Stock
                                                @else Out of Stock
                                                @endif
                  </span></div>


                                        @if(!empty($subcat))
                                            <div class="brand">{{ $subcat->sub_category_name }}</div>
                                        @endif


                                        <div class="other-options clearfix">


<div class="row">
    {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!}
    <div class="col-md-3">

        <div class="item-quantity">
            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input class="quantity-spinner form-control" value="1" name="product_quantity" style="display: block;" type="text"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                {{--<span class="input-group-btn-vertical">--}}
                {{--<button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="glyphicon glyphicon-chevron-up"></i>--}}
                {{--</button>--}}
                {{--<button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="glyphicon glyphicon-chevron-down"></i>--}}
                {{--</button>--}}
                {{--</span>--}}
            </div>
        </div>


    </div>
    <div class="col-md-6">

        <input type="hidden" name="product_id" value="{{$product->id}}">
        <input type="hidden" name="product_name" value="{{$product->product_name}}">
        <input type="hidden" name="product_name_bn" value="{{$product->product_name_bn}}">
        <input type="hidden" name="product_code" value="{{$product->product_code}}">
        <input type="hidden" name="product_price" value="@if($product->discount > 0){{ $product->product_price-($product->product_price*$product->discount)/100}}@else{{$product->product_price}}@endif">
        {{--<input type="hidden" name="product_quantity" value="1">--}}
        <input type="hidden" name="publication_status" value="{{$product->publication_status}}">

        <button type="submit" class="theme-btn btn-style-two add-to-cart"><span class="cart-icon flaticon-shopping-cart"></span> Add To Cart</button>

    </div>
    {!! Form::close() !!}
    <div class="col-md-3">

        @if (Session::has('customer_id'))

            @if($wishlist==NULL)
                {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                <input type="hidden" name="product_id" value="{{$product->id}}">
                <div class="buttons-holder">
                    <button type="submit" class="add-fav"><span class="flaticon-heart"></span></button>
                </div>
                {!! Form::close() !!}
            @else
                <button type="button" class="add-fav tool-tip"><span class="flaticon-heart"></span></button>
            @endif
        @else
            <a href="{{url('join')}}"  class="add-fav tool-tip"><span class="flaticon-heart"></span></a>
            {{--<button type="button" class="add-fav tool-tip"><span class="flaticon-heart"></span></button>--}}
        @endif

    </div>
</div>







                                        </div>
                                        {{--<div class="text">--}}
                                            {{--@php--}}
                                                {{--print_r($product->description)--}}
                                            {{--@endphp--}}
                                        {{--</div>--}}
                                        <!--Item Meta-->


                                    </div>
                                </div>
                            </div>
                            <!--Basic Details-->

                            <!--Product Info Tabs-->
                            <div class="product-info-tabs">

                                <!--Product Tabs-->
                                <div class="tabs-box product-tabs" id="product-tabs">

                                    <!--Tab Btns-->
                                    <ul class="tab-buttons clearfix">
                                        <li class="tab-btn active-btn" data-tab="#prod-description">Description</li>
                                    </ul>

                                    <!--Tabs Container-->
                                    <div class="tabs-content">

                                        <!--Tab / Active Tab-->
                                        <div class="tab active-tab" id="prod-description">
                                            <div class="content">
                                                @php
                                                    print_r($product->description)
                                                @endphp
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <!--Related Posts-->
                            <div class="related-posts">
                                <h2>RELATED PRODUCTS</h2>

                                <div class="row clearfix">
                                    <!--Shop Item-->
                                    @foreach($related as $relate)
                                        <div class="shop-item col-lg-4 col-md-6 col-sm-4 col-xs-12">
                                            <div class="inner-box">
                                                <div class="image-box">
                                                    <?php
                                                    $image = DB::table('product_images')->where('product_id',$relate->id)->first();
                                                    ?>


                                                    <a href="{{url('product-details/'.$relate->id)}}"><img style="height: 200px;" src="{{asset('public/uploads/product/'.$image->product_image)}}" alt="" /></a>
                                                    {{--<div class="overlay-box">--}}
                                                        {{--<div class="item-options clearfix">--}}
                                                            {{--<a href="shopping-cart.php" class="option-btn"><span class="flaticon-shopping-cart-1"></span><div class="tool-tip">Add to cart</div></a>--}}
                                                            {{--<a href="shop-single.php" class="option-btn"><span class="flaticon-heart-1"></span><div class="tool-tip">Add to Fav</div></a>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="lower-box">
                                                    <h4><a href="{{url('product-details/'.$relate->id)}}">{{$relate->product_name}}</a></h4>
                                                    <div class="price">&#2547;
                                                        @if(!empty($relate->discount))
                                                        {{$relate->product_price - ($relate->product_price * $relate->discount/100)}}

{{--                                                            {{$relate->product_price }}<br>--}}
                                                            ({{$relate->discount }}% off)
                                                        @else
                                                            {{$relate->product_price}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side pull-left col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="sidebar shop-sidebar">

                        <!--Search Box-->
                        {{--<div class="sidebar-widget search-box">--}}
                            {{--<div class="sidebar-title">--}}
                                {{--<h2>SEARCH PRODUCTS</h2>--}}
                            {{--</div>--}}
                            {{--<form method="post" action="http://wp.hostlin.com/nijmegan/shop.html">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="search" name="search-field" value="" placeholder="Search Products...">--}}
                                    {{--<button type="submit"><span class="icon fa fa-search"></span></button>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}

                        <!--Category Widget-->
                        <div class="sidebar-widget category-widget">
                            <div class="sidebar-title">
                                <h2>PRODUCT CATEGORIES</h2>
                            </div>
                            <ul class="list-two style-two">
                                @foreach($categories as $cat)
                                    <li><a href="{{url('product_category/'.$cat->sub_category_id)}}">{{$cat->sub_category_name}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


    @endsection