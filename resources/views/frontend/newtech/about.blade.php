@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>About Us</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">About</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!--About Office Section-->
    <section id="office" class="about-section">
        <div align="center" class="sec-title">
            <h2><span>Office</span></h2>
        </div>
        <div class="auto-container">


                @foreach($offices as $office)
                <div class="row clearfix">
                    <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="icon-box"><span class=""></span></div>
                    <div class="inner-box">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <div class="title">Office</div>
                            <h2> <span>{{$office->title}}</span></h2>
                        </div>
                        <div class="text">
                            <p class="dark-text">
                                <?php print_r($office->description); ?>
                            </p>
                        </div>
                        <!--                                <a href="#" class="theme-btn btn-style-two">Read More</a>-->
                    </div>
                </div>
                <!--Image Column-->
                <div class="image-column col-md-6 col-sm-12 col-xs-12">
                    <!--video-box-->
                    <div class="video-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image">
                            <img src="{{asset('public/uploads/about/'.$office->image)}}" alt="" />
                        </figure>
                        <!--                                <a href="https://www.youtube.com/watch?v=0e1uTwSRGgI" class="lightbox-image overlay-box"><span class="flaticon-arrows-5"></span></a>-->
                    </div>
                </div>
                </div>
                <br><br><br>
                    @endforeach
                <!--Content Column-->




        </div>
    </section>
    <!--End About Office Section-->

    <!--About Page Video Section-->
    <section class="page-title video-box about_page_separator" style="background-image:url(public/frontend/images/background/1.jpg); position: relative">
        <div class="auto-container about_page_con">
            <div class="row clearfix">
                <a href="{{url('public/uploads/video/'.$video->video_name)}}" class="lightbox-image about_page_overlay-box"><span class="flaticon-arrows-5"></span></a>
                <!--Title -->
                <div class="title-column col-md-6 col-sm-6 col-xs-12">

                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-6 col-xs-12">
                    <ul class="bread-crumb clearfix">

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End About Page Video Section-->

    <!--About Office Section-->
    <section id="showroom" class="about-section">
        <div align="center" class="sec-title">
            <h2><span>Showroom</span></h2>
        </div>
        <div class="auto-container">

            @foreach($showrooms as $office)
                <div class="row clearfix">
                <!--Image Column-->
                    <div class="image-column col-md-6 col-sm-12 col-xs-12">
                        <!--video-box-->
                        <div class="video-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <figure class="image">
                                <img src="{{asset('public/uploads/about/'.$office->image)}}" alt="" />
                            </figure>
                            <!--                                <a href="https://www.youtube.com/watch?v=0e1uTwSRGgI" class="lightbox-image overlay-box"><span class="flaticon-arrows-5"></span></a>-->
                        </div>
                    </div>
                <!--Content Column-->
                    <div class="content-column col-md-6 col-sm-12 col-xs-12">
                        <div class="icon-box"><span class=""></span></div>
                        <div class="inner-box">
                            <!--Sec Title-->
                            <div class="sec-title">
                                <div class="title">Office</div>
                                <h2> <span>{{$office->title}}</span></h2>
                            </div>
                            <div class="text">
                                <p class="dark-text">
                                    <?php print_r($office->description); ?>
                                </p>
                            </div>
                            <!--                                <a href="#" class="theme-btn btn-style-two">Read More</a>-->
                        </div>
                    </div>
                </div>
                <br><br><br>
                @endforeach

        </div>
    </section>
    <!--End About Office Section-->


@endsection