@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-6 col-xs-12">
                    <h1>SERVICES</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-6 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="index-2.html">Home</a></li>
                        <li class="active">Services</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--Sidebar Page Container-->
    <section class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="sidebar">

                        <!--Services Widget-->
                        <div class="sidebar-widget sidebar-category">
                            <!--Sidebar Title-->
                            <div class="sidebar-title">
                                <h2>SERVICE CATEGORIES</h2>
                            </div>
                            <!--List-->
                            <ul class="list">
                                @foreach($cats as $cat)
                                    <li><a href="{{url('service_cat/'.$cat->id)}}"><span class="icon flaticon-settings-1"></span>{{$cat->cat_name}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>

                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    @foreach($services as $service)

                    <div class="services-detail">
                        <h2><a href="{{url('service/'.$service->id)}}">{{$service->service_title}}</a></h2>
                        <div class="text">
                            <figure>
                                <img src="{{asset('public/uploads/service/'.$service->service_image)}}" alt="" />
                            </figure>
                        </div>

                        <!--Accordion Box-->
                        <ul class="accordion-box style-two">
                            <li class="accordion block bg-violate">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus fa fa-angle-down"></span> </div>In Details</div>
                                <div class="acc-content current">
                                    <div class="content">
                                        @php
                                        print_r($service->service_description);
                                                @endphp
                                    </div>
</div>
</li>
</ul>
<!--End Accordion Box-->

</div>

@endforeach


</div>

</div>
</div>
</section>



@endsection