@extends('layouts.frontend_about')

@section('content')


  <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    @include('frontend/include/sidebar_list')
                    <!-- /Block categories module -->



                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Search</span>
                    </div>
                    <!-- /Breadcrumb -->
                    <div class="rte">

                       
                       @if(count($products)==0)
                        <p><h2>No Item Found !</h2></p>
                       @else
                        <p><h2>Search Result for <span style="font-style: italic; color: #1e5792">{{ $query }}</span>:</h2></p>
                        <h4><span style="color: #1e5792">{{ count($products) }} product(s) Found</span></h4>

                        @foreach($products as $p)
                        @php
                        $img = DB::table('product_images')->where('product_id',$p->id)->first();
                        @endphp
                    <a href="{{ url('view_product/'.$p->id) }}">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img img-responsive" style="width: 100%" src="{{ asset('product_image/'.$img->product_image) }}">
                                </div>
                                <div class="col-md-8">
                                    <h2>{{ $p->product_name }}</h2>
                                   
                                </div>
                            </div>
                        </div>
                    </a>
                        @endforeach
                       
                       @endif


                    </div>
                    <br />
                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

@endsection