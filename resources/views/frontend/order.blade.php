@extends('layouts.frontend')

@section('content')

 <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    @include('frontend/include/sidebar_list')
                    <!-- /Block categories module -->


                   

                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Order Product</span>
                    </div>
                    <!-- /Breadcrumb -->

                    <h1 class="page-heading bottom-indent">
                        Customer service - Order Product</h1>


                    <form action="{{ url('post_order') }}" method="post" class="contact-form-box" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <fieldset>
                            <h3 class="page-subheading">Product Order Form</h3>
                            <div class="clearfix">
                                <div class="col-xs-12 col-md-4">

                                 
                                    <p class="form-group">
                                        <label for="name">Product Code</label>
                                        <input class="form-control grey validate" type="text" id="code" name="code" placeholder="eg: code-1, code-2, code-3 ..."  required="" />
                                    </p>
                                    <p class="form-group">
                                        <label for="name">Customer Name</label>
                                        <input class="form-control grey validate" type="text" id="name" name="name" value=""  required="" />
                                    </p>
                                    <p class="form-group">
                                        <label for="email">Email </label>
                                        <input class="form-control grey validate" type="email" id="email" name="email" data-validate="isEmail" value=""  required="" />
                                    </p>
                                  
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <div class="form-group">
                                        <label for="message">Address</label>
                                        <textarea class="form-control" rows="2" name="address" required="" ></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea class="form-control" rows="4" name="details" required="" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="submit">
                                <button type="submit" name="submitMessage" id="submitMessage" class="button btn btn-default button-medium"><span>Send<i class="icon-chevron-right right"></i></span></button>
                            </div>
                        </fieldset>
                    </form>

                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->


@endsection