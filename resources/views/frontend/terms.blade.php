@extends('layouts.frontend_about')

@section('content')

<div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    @include('frontend/include/sidebar_list')
                    <!-- /Block categories module -->


                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Terms and conditions of use</span>
                    </div>
                    <!-- /Breadcrumb -->
                    <div class="rte">
                        <h1 class="page-heading">Terms and conditions of use</h1>
                        <h3 class="page-subheading">Rule 1</h3>
                        <p class="bottom-indent">B-line enterprise  is a Hi-Tech enterprise having its own import, supply and sales of fiber optical and communications equipments.</p>
                        <h3 class="page-subheading">Rule 2</h3>
                        <p class="bottom-indent">The main products of b-line include UTP Cables , Fiber optic patch cord, Connector, Adapter, Optical coupler, Attenuator, Optic media converter, Optic splice closure, Optic terminal box, ODF etc, which are widely used in telecommunications, broadband, broadcast, CATV and network industry. </p>
                        <h3 class="page-subheading">Rule 3</h3>
                        <p class="bottom-indent">The main products of b-line include UTP Cables , Fiber optic patch cord, Connector, Adapter, Optical coupler, Attenuator, Optic media converter, Optic splice closure, Optic terminal box, ODF etc, which are widely used in telecommunications, broadband, broadcast, CATV and network industry. </p>
                    </div>
                    <br />
                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

@endsection