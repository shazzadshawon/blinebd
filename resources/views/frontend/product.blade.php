@extends('layouts.frontend')

@section('content')

@php

$image1 = DB::table('product_images')->where('product_id',$product->id)->first();
$images = DB::table('product_images')->where('product_id',$product->id)->get();

$category = DB::table('categories')->where('category_id',$product->category_id)->first();
$subcategory = DB::table('sub_categories')->where('sub_category_id',$product->sub_category_id)->first();

@endphp
 

 <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="center_column" class="center_column col-xs-12" style="width:100%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe">&gt;</span>
                        <a href="{{ url('product_category/'.$category->category_id) }}" title="{{ $category->category_name }}">{{ $category->category_name }}</a>
                        <span class="navigation-pipe">></span>
                        <a href="{{ url('product_sub_category/'.$subcategory->sub_category_id) }}" title="{{ $product->product_name }}">{{ $subcategory->sub_category_name }}</a>
                        <span class="navigation-pipe">></span>{{ $product->product_name }}
                    </div>
                    <!-- /Breadcrumb -->


                    <div class="primary_block row" itemscope itemtype="">
                        <div class="container">
                            <div class="top-hr"></div>
                        </div>
                        <!-- left infos-->
                        <div class="pb-left-column col-xs-12 col-sm-4 col-md-4">
                            <!-- product img-->
                            <div id="image-block" class="clearfix">
                               {{--  <span class="new-box">
						            <span class="new-label">New</span>
					            </span> --}}
                               {{--  <span class="sale-box no-print">
						            <span class="sale-label">Sale!</span>
					            </span> --}}
                                <span id="view_full_size">
                                    <a class="jqzoom" title="Product Name " rel="gal1" href="{{ asset('product_image/'.$image1->product_image) }}" itemprop="url">
                                        <img style="width: 100%; height:400px" itemprop="image" src="{{ asset('product_image/'.$image1->product_image) }}" title="Product Name " alt="Product Name "/>
							        </a>
                                </span>
                            </div> <!-- end image-block -->
                            <!-- thumbnails -->
                            <div id="views_block" class="clearfix ">
                                <span class="view_scroll_spacer">
                                    <a id="view_scroll_left" class="" title="Other views" href="javascript:{}">Previous</a>
                                </span>

                                <div id="thumbs_list">
                                    <ul id="thumbs_list_frame">
                                    	@php
$i=1;
                                    	@endphp
                                    	@foreach($images as $img)	
                                        <li id="thumbnail_{{ $img->product_image_id }}" class="@if($i==count($images)) last @endif">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: '{{ asset('product_image/'.$img->product_image) }}',largeimage: '{{ asset('product_image/'.$img->product_image) }}'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_{{ $img->product_image_id }}" src="{{ asset('product_image/'.$img->product_image) }}" alt="Product Name " title=" " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        @php
$i++;
                                    	@endphp
                                        @endforeach
                                       
                                    </ul>
                                </div> <!-- end thumbs_list -->

                                <a id="view_scroll_right" title="Other views" href="javascript:{}">Next</a>
                            </div> <!-- end views-block -->
                            <!-- end thumbnails -->
                            <p class="resetimg clear no-print">
					<span id="wrapResetImages" style="display: none;">
						<a href="singleProduct.php" id="resetImages">
							<i class="icon-repeat"></i>
							Display all pictures
						</a>
					</span>
                            </p>
                        </div> <!-- end pb-left-column -->
                        <!-- end left infos-->
                        <!-- center infos -->
                        <div class="pb-center-column col-xs-12 col-sm-5">
                            <h1 itemprop="name"> {{ $product->product_name }} </h1>
                            <div id="short_description_block">
                                <div id="short_description_content" class="rte align_justify" itemprop="description"><p>
                                     
                                       </p></div>

                               
                                <!---->
                            </div> <!-- end short_description_block -->
                            <!-- number of item in stock -->
                            <p id="pQuantityAvailable">
                                <span id="">{{ $product->product_quantity }}</span>
                                
                                <span id="quantityAvailableTxtMultiple">Items</span>
                            </p>
                            <!-- availability -->
                            <p id="availability_statut">

                                <span id="availability_value">In stock</span>
                            </p>
                            <br>
                 
                            <!-- availability -->

                            @php
                            $sizes = DB::table('product_sizes')->where('product_id',$product->id)->get();
                            
                            @endphp
@if(count($sizes) > 0)
                            <p id="pQuantityAvailable">
                                <span id="quantityAvailableTxtMultiple">Available Sizes :</span>
                                           
                             </p>
                            @foreach($sizes as $sz)
                            <p id="availability_statut">

                                <span id="availability_value">{{ $sz->size }}</span>
                            </p>
                            @endforeach
                           
@endif

                            <!--  /Module ProductComments -->
                            <p class="socialsharing_product list-inline no-print">
                                <button type="button" class="btn btn-default btn-twitter"
                                        onclick="">
                                    <i class="icon-twitter"></i> Tweet
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/twitter.gif" alt="Tweet" /> -->
                                </button>
                                <button type="button" class="btn btn-default btn-facebook"
                                        onclick="">
                                    <i class="icon-facebook"></i> Share
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/facebook.gif" alt="Facebook Like" /> -->
                                </button>
                                <button type="button" class="btn btn-default btn-google-plus"
                                        onclick="">
                                    <i class="icon-google-plus"></i> Google+
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/google.gif" alt="Google Plus" /> -->
                                </button>
                                <button type="button" class="btn btn-default btn-pinterest"
                                        onclick="">
                                    <i class="icon-pinterest"></i> Pinterest
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/pinterest.gif" alt="Pinterest" /> -->
                                </button>
                            </p>
                             <ul id="usefull_link_block" class="clearfix no-print">
                                <li class="sendtofriend">
                                    <a id="send_friend_button" href="#send_friend_form">
                                        Send to a friend
                                    </a>
                                    <div style="display: none;">
                                        <div id="send_friend_form">
                                            <h2 class="page-subheading">
                                                Send to a friend
                                            </h2>
                                            <div class="row">
                                                <div class="product clearfix col-xs-12 col-sm-6">
                                                    <img src="{{ asset('product_image/'.$image1->product_image) }}" height="200" width="200"
                                                         alt="Product Name "/>
                                                    <div class="product_desc">
                                                        <p class="product_name">
                                                            <strong>{{ $product->product_name }}</strong>
                                                        </p>
                                                        <p>
                                                            @php
                                                            print_r( $product->description );
                                                            @endphp
                                                        </p>
                                                    </div>
                                                </div><!-- .product -->
                                                <div class="send_friend_form_content col-xs-12 col-sm-6"
                                                     id="send_friend_form_content">
                                                    <div id="send_friend_form_error"></div>
                                                    <div id="send_friend_form_success"></div>
                                                    <div class="form_container">
                                                        <p class="intro_form">
                                                            Recipient :
                                                        </p>
                                                        <p class="text">
                                                            <label for="friend_name">
                                                                Name of your friend <sup class="required">*</sup> :
                                                            </label>
                                                            <input id="friend_name" name="friend_name" type="text"
                                                                   value=""/>
                                                        </p>
                                                        <p class="text">
                                                            <label for="friend_email">
                                                                E-mail address of your friend <sup
                                                                        class="required">*</sup> :
                                                            </label>
                                                            <input id="friend_email" name="friend_email" type="text"
                                                                   value=""/>
                                                        </p>
                                                        <p class="txt_required">
                                                            <sup class="required">*</sup> Required fields
                                                        </p>
                                                    </div>
                                                    <p class="submit">
                                                        <button id="sendEmail" class="btn button button-small"
                                                                name="sendEmail" type="submit">
                                                            <span>Send</span>
                                                        </button>&nbsp;
                                                        or&nbsp;
                                                        <a class="closefb" href="#">
                                                            Cancel
                                                        </a>
                                                    </p>
                                                </div> <!-- .send_friend_form_content -->
                                            </div>
                                        </div>
                                    </div>
                                </li>


                                <li class="print">
                                    <a href="javascript:print();">
                                        Print
                                    </a>
                                </li>
                            </ul>

                        
                          
                        </div>
                        <!-- end center infos-->
                        <!-- pb-right-column-->
                        <div class="pb-right-column col-xs-12 col-sm-4 col-md-3">
                            <!-- add to cart form-->
                          
                            <form id="buy_block" action="#" method="post" style="display: content;">
                                <!-- hidden datas -->
                                <p class="hidden">
                                    <input type="hidden" name="token" value="cc110c7f9f9b6cbcf2ff0d4816063f47"/>
                                    <input type="hidden" name="id_product" value="14" id="product_page_product_id"/>
                                    <input type="hidden" name="add" value="1"/>
                                    <input type="hidden" name="id_product_attribute" id="idCombination" value=""/>
                                </p>
                                <div class="box-info-product">
                                    <div class="content_prices clearfix">
                                        <!-- prices -->
                                        <div class="price">
                                            <p class="our_price_display" itemprop="offers" itemscope
                                               itemtype="">
                                                
                                                <span id="" itemprop="price">৳ {{ $product->product_price-($product->product_price*$product->discount)/100}}</span>
                                               
                                                
                                            </p>
                                            
                                        </div> <!-- end prices -->

                                         <div class="price">
                                            <p class="our_price_display" itemprop="offers" itemscope
                                               itemtype="">
                                                
                                                
                                                <span id="" itemprop=""><small style="text-decoration: line-through; color: #252525">৳ {{ $product->product_price }}</small></span>
                                                <!---->
                                                
                                            </p>
                                            
                                        </div> <!-- end prices -->
                                       

                                        <div class="clear"></div>
                                    </div> <!-- end content_prices -->
                                    <div class="product_attributes clearfix"  style="display: none">
                                        <!-- attributes -->
                                        <div id="attributes">
                                            <div class="clearfix"></div>
                                            <fieldset class="attribute_fieldset">
                                                <label class="attribute_label" for="group_1">Size :&nbsp;</label>
                                                @php
                                            $sizes = DB::table('product_sizes')->where('product_id',$product->id)->get();
                                                @endphp
                                                <div class="attribute_list">
                                                    <select name="group_1" id="group_1" class="form-control attribute_select no-print">

                                                        <option value="1" selected="selected" title="S">S</option>


                                                    </select>
                                                </div> <!-- end attribute_list -->
                                            </fieldset>
                                            <fieldset class="attribute_fieldset">
                                                <input type="hidden" class="color_pick_hidden" name="group_3"
                                                       value="11"/>
                                            </fieldset>
                                        </div> <!-- end attributes -->
                                    </div> <!-- end product_attributes -->

                                </div> <!-- end box-info-product -->



                              

                                <!-- /MODULE Block best sellers -->


                            </form>
                            <div class="clearfix"><br></div>
                              @include('frontend/include/sidebar_list')
                              @include('frontend/include/top_saller_sidebar')
                        </div> <!-- end pb-right-column-->
                    </div> <!-- end primary_block -->

                    <!-- Megnor start : TAB-->
                    <section class="tm-tabcontent">
                        <ul id="productpage_tab" class="nav nav-tabs clearfix">
                            <li class="active"><a data-toggle="tab" href="#moreinfo" class="moreinfo">Full info</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- More Info -->
                            <ul id="moreinfo" class="tm_productinner tab-pane active">
                            	<br>
                                <p>
                                	  @php
                                      	print_r($product->description);
                                      @endphp
                                </p>
                            </ul>
                            <!-- End More Info -->
                        </div>

                    </section>
                    <!-- Megnor End :TAB -->


                    <section class="page-product-box blockproductscategory">
                    	@php
                    	$releted = DB::table('products')->where('sub_category_id',$product->sub_category_id)->where('publication_status',1)->get();
                    	@endphp
                    	@if(count($releted)>0)
                        <h3 class="productscategory_h3 page-product-heading">{{ count($releted) }} other products in the same category:</h3>
                        {{-- <div id="productscategory_list" class="clearfix">

                            <!-- Megnor start -->
                            <!-- Define Number of product for SLIDER -->
                            <div class="customNavigation">
                                <a class="btn prev  productcategory_prev"><i class="icon-chevron-sign-left"></i></a>
                                <a class="btn next productcategory_next"><i class="icon-chevron-sign-right"></i></a>
                            </div>
                            <!-- Megnor End -->

                            <ul id="productcategory-carousel" class="tm-carousel clearfix">
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')
                            	@include('frontend/include/releted_product_items')

                            </ul>
                        </div> --}}

 <div class="row" id="columns_inner">
                    <div id="center_column" class="center_column col-xs-12" style="width:100%;">

                        <!--Featured Products and New Products Tab-->
                        
                        
                        <!--//Featured Products and New Products Tab-->

                        
                        @include('frontend/include/rel')


                    </div><!-- #center_column -->
                </div><!-- .row -->
                        
                        @endif
                    </section>


                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->
@endsection