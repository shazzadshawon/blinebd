<div class="animate-dropdown"><!-- ========================================= BREADCRUMB ========================================= -->

@php
    $subcat = DB::table('sub_categories')->where('sub_category_id',$productInfo->sub_category_id)->first();
    $cat = DB::table('categories')->where('category_id',$productInfo->category_id)->first();
@endphp
    <div id="breadcrumb-alt">
        <div class="container">
            <div class="breadcrumb-nav-holder minimal">
                <ul>
                 <li class="dropdown breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                @if(!empty($cat))
                    <li class="dropdown breadcrumb-item"><a href="#">{{ $cat->category_name }}</a></li>
                @endif
                @if(!empty($subcat))
                    <li class="dropdown breadcrumb-item"><a href="{{ url('Category-products/'.$subcat->sub_category_id) }}">{{ $subcat->sub_category_name }}</a></li>
                @endif
                    <li class="dropdown breadcrumb-item"><a href="">{{$productInfo->product_name}}</a></li><!-- /.breadcrumb-item -->
                </ul><!-- /.breadcrumb-nav-holder -->
            </div>
        </div><!-- /.container -->
    </div>
    <!-- ========================================= BREADCRUMB : END ========================================= -->

</div>