<footer id="footer">
    <div class="sub-form-row">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                <form role="form" method="post" action="{{ url('subscribe') }}">
                {{ csrf_field() }}
                    <input placeholder="Subscribe to our newsletter" type="email" name="email">
                    <button type="submit" class="le-button">Subscribe</button>
                </form>
            </div>
        </div><!-- /.container -->
    </div><!-- /.sub-form-row -->

    <div class="link-list-row">
        <div class="container no-padding">
            <div class="col-xs-12 col-md-4 ">
                <!-- ============================================================= CONTACT INFO ============================================================= -->
                <div class="contact-info">
                    <div class="footer-logo">
                        <img alt="logo" src="{{asset('public/ezbazzar/images/logo_e.png')}}" width="233" height="104"/>
                    </div><!-- /.footer-logo -->

                    <p class="regular-bold"> Feel free to contact us via phone,email or just send us mail.</p>

                    <p>
                        house #33, Road #14, DIT project, MerulBadda
                        Dhaka, Bangladesh 1212 (0194 0808966)
                    </p>

                    <div class="social-icons">
                        <h3>Get in touch</h3>
                        <ul>
                            <li><a href="https://www.facebook.com/ezbazarbdcom-1871833656413050/" target="_blank" class="fa fa-facebook"></a></li>
                            <li><a href="https://www.twitter.com" class="fa fa-twitter" target="_blank"></a></li>
                            <li><a href="#" class="fa fa-pinterest" target="_blank"></a></li>
                            <li><a href="#" class="fa fa-linkedin" target="_blank"></a></li>
                            <li><a href="#" class="fa fa-stumbleupon" target="_blank"></a></li>
                            <li><a href="#" class="fa fa-dribbble" target="_blank"></a></li>
                            <li><a href="#" class="fa fa-vk" target="_blank"></a></li>
                        </ul>
                    </div><!-- /.social-icons -->

                </div>
                <!-- ============================================================= CONTACT INFO : END ============================================================= -->            </div>

            <div class="col-xs-12 col-md-8 no-margin">
                <!-- ============================================================= LINKS FOOTER ============================================================= -->
                <div class="link-widget">
                    <div class="widget">

                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->

                <div class="link-widget">
                    <div class="widget">
                        <ul>

                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->

                <div class="link-widget">
                    <div class="widget">
                        <ul>
                            <li><a href="{{ url('about-us') }}">About Us</a></li>
                            <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                            <li><a href="{{ url('terms') }}">Terms &amp; Conditions</a></li>
                            <li><a href="{{ url('refund') }}">Return &amp; Refund Policy</a></li>
                            <li><a href=" {{ url('privacy') }} ">Privacy policy</a></li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->
                <!-- ============================================================= LINKS FOOTER : END ============================================================= -->            </div>
        </div><!-- /.container -->
    </div><!-- /.link-list-row -->

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-margin">
                <div class="copyright">
                    Created with <i class="fa fa-heart" style="color:red" aria-hidden="true"></i> By</span> <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a>
                </div><!-- /.copyright -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.copyright-bar -->

</footer><!-- /#footer -->