{{--<li>--}}
    {{--<div class="basket-item">--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-4 col-sm-4 no-margin text-center">--}}
                {{--<div class="thumb">--}}
                    {{--<img alt="" src="assets/images/products/product-small-01.jpg" />--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-8 col-sm-8 no-margin">--}}
                {{--<div class="title">Blueberry</div>--}}
                {{--<div class="price">&#2547;270.00</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<a class="close-btn" href="#"></a>--}}
    {{--</div>--}}
{{--</li>--}}


@foreach ($addTocart as $cart)
    @php
        $product = DB::table('products')->where('id',$cart->product_id)->first();
        $image = DB::table('product_images')->where('product_id',$product->id)->first();

    @endphp
    <li class="product-info">
        <div class="row">
            <div class="col-md-5">
                {{-- <a href="#" class="remove_link"></a> --}}
                <a href="{{ url('product-details/'.$product->id) }}">
                    <img style="height: 110px; width: 100%" class="img-responsive" src="{{ asset('product_image/'.$image->product_image) }}" alt="p10">
                </a>
            </div>
            <div class="col-md-7">
                <p class="p-name">{{ $product->product_name }}</p>

                @php
                    $j = $cart->product_price * $cart->product_quantity +$j;
                @endphp
                <p>Size: {{ $cart->size }}</p>
                <p>Qty: {{ $cart->product_quantity }}</p>
                <p class="p-rice">&#2547; {{ $cart->product_price * $cart->product_quantity}}</p>
            </div>
        </div>
    </li>
@endforeach
