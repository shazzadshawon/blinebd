
@foreach ($products as $top)

<div class="product-item product-item-holder">
   
    <div class="row">
        <div class="no-margin col-xs-12 col-sm-4 image-holder">
         @php
            $product_image = DB::table('product_images')->where('product_id',$top->id)->first();
        @endphp
            <div class="image">
                 <img style="height: 200px" src="{{asset('product_image/'.$product_image->product_image)}}" data-echo="{{asset('product_image/'.$product_image->product_image)}}" />
            </div>
        </div><!-- /.image-holder -->
        <div class="no-margin col-xs-12 col-sm-5 body-holder">
            <div class="body">
                  @if(!empty( $top->discount ))
                                            <div class="label-discount green">{{ $top->discount  }}% Discount</div>
                                        @else
                                            <div class="label-discount green">0% Discount</div>
                                        @endif
                <div class="title">
                    <a href="single-product.php">{{$top->product_name}}</a>
                </div>
                
                <div class="excerpt">
                    <p>@php
                        print_r($top->description);
                    @endphp</p>
                </div>
              
            </div>
        </div><!-- /.body-holder -->
        <div class="no-margin col-xs-12 col-sm-3 price-area">
            <div class="right-clmn">
             @php
                $discount_amount = ( $top->product_price * $top->discount / 100 );
            @endphp
                <div class="price-current">&#2547;{{ $discount_amount }}</div>
                <div class="price-prev">&#2547;{{ $top->product_price }}</div>
                <div class="availability"><label>availability:</label><span class="available">
                @if ($top->product_quantity > 0)
                    In Stock
                @else
                    Out of Stock
                @endif
                  </span></div>
                <a href="{{ url('/product-details/'.$top->id) }}" class="le-button">View Details</a>
               {{--  <a class="btn-add-to-wishlist" href="#">add to wishlist</a> --}}
            </div>
        </div><!-- /.price-area -->
    </div><!-- /.row -->
</div><!-- /.product-item -->

@endforeach