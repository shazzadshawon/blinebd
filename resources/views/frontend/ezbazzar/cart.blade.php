@extends('layouts.frontend')
@section('content')
    <section id="cart-page">
        <div class="container">

            <div class="col-xs-12 col-sm-3 col-md-2 sidemenu-holder">
                @include('frontend.ezbazzar.sidebar_navigation')
                {{--@include()--}}
            </div><!-- /.sidemenu-holder -->

            <div class="col-xs-12 col-sm-9 col-md-10 homebanner-holder">
                @include('frontend.ezbazzar.cart_content')
            </div><!-- /.homebanner-holder -->

        </div><!-- /.container -->
    </section>
@endsection