 @php
        $SubCategories_offers = DB::table('pazzles')
        ->where('publication_status',1)
        ->where('pazzle',$SubCategories->sub_category_id)->get();
        $i=1;
@endphp

@if (!empty($SubCategories_offers))
    <div class="">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    @php
        $x=0;
    @endphp
    <ol class="carousel-indicators">
    @foreach ($SubCategories_offers as $offer)
      <li data-target="#myCarousel" data-slide-to="{{ $x }}" class="@if ($x==0) active
      @endif"></li>
      @php
          $x++;
      @endphp
    @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
   

    @foreach ($SubCategories_offers as $offer)
        <div class="item @if ($i==1) active @endif">
            <img src="{{ asset('pazzle_image/'.$offer->pazzle_image) }}" alt="Los Angeles" style="width:100%; height: 300px;">
        </div>
        @php
            $i++;
        @endphp
    @endforeach
     

     
    </div>

   {{--  <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a> --}}
  </div>
</div>

@endif


<section id="gaming">
    <div class="grid-list-products">
        <h2 class="section-title">{{ $SubCategories->sub_category_name }}</h2>

        <div class="control-bar" style="height: 50px">
            <div class="grid-list-buttons">
                <ul>
                    <li class="grid-list-button-item active"><a data-toggle="tab" href="#grid-view"><i class="fa fa-th-large"></i> Grid View</a></li>
                    <li class="grid-list-button-item "><a data-toggle="tab" href="#list-view"><i class="fa fa-th-list"></i> List View</a></li>
                </ul>
            </div>

        </div><!-- /.control-bar -->
           
        <div class="tab-content">
            <div id="grid-view" class="products-grid fade tab-pane in active">

                <div class="product-grid-holder">
                    <div class="row no-margin">
                          @include('frontend/ezbazzar/category_products_grid')
                    </div><!-- /.row -->
                    <div class="row pull-right">{{ $products->links() }}</div>
                </div><!-- /.product-grid-holder -->

{{--                 <div class="pagination-holder">
                    <div class="row">

                        <div class="col-xs-12 col-sm-6 text-left">
                            <ul class="pagination ">
                                <li class="current"><a  href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">next</a></li>
                            </ul>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <div class="result-counter">
                                Showing <span>1-9</span> of <span>11</span> results
                            </div>
                        </div>

                    </div><!-- /.row -->
                </div> --}}<!-- /.pagination-holder -->
            </div><!-- /.products-grid #grid-view -->

            <div id="list-view" class="products-grid fade tab-pane ">
                <div class="products-list">
                    
                    @include('frontend/ezbazzar/category_product_list')
<div class="row pull-right">{{ $products->links() }}</div>
                </div><!-- /.products-list -->

{{--                 <div class="pagination-holder">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 text-left">
                            <ul class="pagination">
                                <li class="current"><a  href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">next</a></li>
                            </ul><!-- /.pagination -->
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="result-counter">
                                Showing <span>1-9</span> of <span>11</span> results
                            </div><!-- /.result-counter -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.pagination-holder --> --}}

            </div><!-- /.products-grid #list-view -->

        </div><!-- /.tab-content -->
    </div><!-- /.grid-list-products -->

</section><!-- /#gaming -->