<div class="tab-pane active" id="featured">
    <div class="product-grid-holder">
        <div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
            <div class="product-item">
                <div class="ribbon red"><span>sale</span></div>
                <div class="image">
                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/thumb/1.png" />
                </div>
                <div class="body">
                    <div class="label-discount green">-50% sale</div>
                    <div class="title">
                        <a href="single-product.php">Featured Product Name One</a>
                    </div>
                    <div class="brand">Brand One</div>
                </div>
                <div class="prices">
                    <div class="price-prev">&#2547;1399.00</div>
                    <div class="price-current pull-right">&#2547;1199.00</div>
                </div>

                <div class="hover-area">
                    <div class="add-cart-button">
                        <a href="single-product.php" class="le-button">add to cart</a>
                    </div>
                    <div class="wish-compare">
                        <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
            <div class="product-item">
                <div class="ribbon blue"><span>new!</span></div>
                <div class="image">
                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/thumb/2.png" />
                </div>
                <div class="body">
                    <div class="label-discount clear"></div>
                    <div class="title">
                        <a href="single-product.php">Featured Product Name Two</a>
                    </div>
                    <div class="brand">Barnd Two</div>
                </div>
                <div class="prices">
                    <div class="price-prev">&#2547;1399.00</div>
                    <div class="price-current pull-right">&#2547;2199.00</div>
                </div>
                <div class="hover-area">
                    <div class="add-cart-button">
                        <a href="single-product.php" class="le-button">add to cart</a>
                    </div>
                    <div class="wish-compare">
                        <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
            <div class="product-item">

                <div class="image">
                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/thumb/3.png" />
                </div>
                <div class="body">
                    <div class="label-discount clear"></div>
                    <div class="title">
                        <a href="single-product.php">Featured Product Name Three</a>
                    </div>
                    <div class="brand">Brand Three</div>
                </div>
                <div class="prices">
                    <div class="price-prev">&#2547;1399.00</div>
                    <div class="price-current pull-right">&#2547;199.00</div>
                </div>
                <div class="hover-area">
                    <div class="add-cart-button">
                        <a href="single-product.php" class="le-button">add to cart</a>
                    </div>
                    <div class="wish-compare">
                        <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
            <div class="product-item">
                <div class="ribbon red"><span>sale</span></div>
                <div class="ribbon green"><span>bestseller</span></div>
                <div class="image">
                    <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/thumb/4.png" />
                </div>
                <div class="body">
                    <div class="label-discount clear"></div>
                    <div class="title">
                        <a href="single-product.php">Featured Product Name Four</a>
                    </div>
                    <div class="brand">Brand Four</div>
                </div>
                <div class="prices">
                    <div class="price-prev">&#2547;1399.00</div>
                    <div class="price-current pull-right">&#2547;3199.00</div>
                </div>
                <div class="hover-area">
                    <div class="add-cart-button">
                        <a href="single-product.php" class="le-button">add to cart</a>
                    </div>
                    <div class="wish-compare">
                        <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loadmore-holder text-center">
        <a class="btn-loadmore" href="category.php">
            <i class="fa fa-plus"></i>
            load more products</a>
    </div>

</div>