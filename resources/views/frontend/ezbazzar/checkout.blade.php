@extends('layouts.frontend')
@section('content')
 {!! Form::open(['route' => 'Order.store']) !!}
        <section id="checkout-page">
            <div class="container">
                <div class="col-xs-12 no-margin">

                    <div class="billing-address">
                        <h2 class="border h1">billing address</h2>

                            <div class="row field-row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>First name*</label>
                                    <input class="le-input" name="fname">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label>last name*</label>
                                    <input class="le-input" name="lname">
                                </div>
                            </div><!-- /.field-row -->

                            <div class="row field-row">
                                <div class="col-xs-12">
                                    <label>Address</label>
                                    <input class="le-input" name="address">
                                </div>
                            </div><!-- /.field-row -->

                            <div class="row field-row">
                                <div class="col-xs-12 col-sm-4">
                                    <label>postcode / Zip*</label>
                                    <input class="le-input"  name="zipcode">
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>email address*</label>
                                    <input class="le-input" name="email">
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                    <label>phone number*</label>
                                    <input class="le-input" name="phone">
                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <br>
                                    <label>shipping</label>
                                    <div class="value">
                                        <div class="radio-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input class="le-radio le-input" type="radio" name="location" value="0"> <div class="radio-label bold">free shipping</div><br>
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="le-radio le-input" type="radio" name="location" value="15" checked>  <div class="radio-label">local delivery - <span class="bold">&#2547;15</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.field-row -->


                    </div><!-- /.billing-address -->






                    @php

                        $i=1;
                        $j=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get();
                    @endphp




                    <section id="your-order">
                        <h2 class="border h1">your order</h2>



                        <table class="table table-wishlist table-responsive cart_summary">
                            <thead>
                            <tr>
                                <th class="cart_product">

                                    Product


                                </th>
                                <th>

                                    Description


                                </th>
                                <th>

                                    Size


                                </th>
                                <th>

                                    Unit price


                                </th>
                                <th>

                                    Qty


                                </th>
                                <th>

                                    Total

                                </th>
                                <th  class="action"><i class="fa fa-trash-o"></i></th>
                            </tr>
                            </thead>
                            <tbody>

                            @php

                                $i=1;
                                $j=0;
                                $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get();
                            @endphp
                            @foreach ($addTocart as $cart)

                                @php
                                    $addTocartImage = DB::table('product_images')->where('product_id', $cart->product_id)->first();
                                @endphp
                                <tr>
                                    <td class="cart_product">
                                        <a href="{{ URL::to('/product-details/'.$cart->product_id) }}"><img style="height: 100px" src="{{asset('product_image/'.$addTocartImage->product_image)}}" alt="Product"></a>
                                    </td>
                                    <td class="cart_description">
                                        <p class="product-name"><a href="{{ URL::to('/product-details/'.$cart->product_id) }}">

                                                {{ $cart->product_name }}


                                            </a></p>
                                        <small class="cart_ref">Item Code : #{{ $cart->product_code }}</small><br>
                                        {{-- <small><a href="#">Color : Beige</a></small><br>
                                        <small><a href="#">Size : S</a></small> --}}
                                    </td>
                                    <td class="price"><span> {{ $cart->size }} </span></td>
                                    <td class="price"><span>
                                          @php

                                              $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
                                              $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                                              $BnPrice = str_replace($replace_array,$search_array,$cart->product_price);
                                          @endphp


                                            {{ $cart->product_price }} TK


                            </span></td>
                                    <td class="qty">
                                        <h3>
                                            @php

                                                $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
                                                $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                                                $QTY = str_replace($replace_array,$search_array,$cart->product_quantity);
                                            @endphp


                                            {{ $cart->product_quantity }}


                                        </h3>
                                    </td>
                                    <td class="price">
                                <span>
                                       @php
                                           $sub_total=$cart->product_price*$cart->product_quantity;

                                           $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
                                           $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                                           $SubPrice = str_replace($replace_array,$search_array,$sub_total);
                                       @endphp


                                    {{ $sub_total }} TK



                                </span>
                                    </td>
                                    <td class="action">
                                        <a href="{{URL::to('/remove-cart-product/'.$cart->id)}}">Delete item</a>
                                    </td>
                                </tr>

                                @php
                                    $j =$j+ $sub_total;
                                    $i++;
                                @endphp
                            @endforeach

                            </tbody>
                            <tfoot>

                            {{-- <tr>
                                <td colspan="3"><strong>Total</strong></td>
                                <td colspan="2"><strong>122.38 €</strong></td>
                            </tr> --}}
                            </tfoot>
                        </table>






                    </section><!-- /#your-order -->

                    <div id="total-area" class="row no-margin">
                        <div class="col-xs-12 col-lg-4 col-lg-offset-8 no-margin-right">
                            <div id="subtotal-holder">
                                <ul class="tabled-data inverse-bold no-border">
                                    <li>
                                        <label>Order total</label>
                                        <div class="value "><h1><strong>&#2547;{{ $j }}</strong></h1></div>
                                    </li>

                                </ul><!-- /.tabled-data -->



                            </div><!-- /#subtotal-holder -->
                        </div><!-- /.col -->
                    </div><!-- /#total-area -->

                    <div class="place-order-button">
                        <button type="submit" class="le-button big">place order</button>
                    </div><!-- /.place-order-button -->

                </div>
            </div>
        </section>
 {!! Form::close() !!}
@endsection