@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif --}}

<!-- page wapper-->
<div class="columns-container">
   
        <main id="about-us">
            <div class="container inner-top-xs inner-bottom-sm">

                <div class="row">
                    <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">

                        <section id="who-we-are" class="section m-t-0 wow fadeInUp">
                            <h2>Our Story</h2>
                            <p>Ezbazarbd.com is an online super shop in Dhaka, Bangladesh. We believe time is valuable to everyresident, and that they should not have to waste hours in traffic, brave bad weather and wait in line just to buy basic necessities,lifestyle, life saving items, medicines, fashion items even the tickets of national and international transports, airlines, hotel booking, visa services etc. This is the main reason behind to start ezbazarbd.com (which makes your life easy) and also aiming to deliver everything you need right at your door-step within a very short period of time.</p>
                        </section><!-- /#who-we-are -->

                        <section id="our-goal-and-idea" class="section wow fadeInUp">
                            <h2>Our Goal and Idea</h2>
                            <p>Ezbazarbd is starting the journeyand hope to get better over time. We believe using this platform we are a strong participants of improvement of our beloved country Bangladesh. We will continue our effort to increase technological involvement in e-commerce sector. We welcome any ideas to improve our service. Please feel free to suggest us.</p>
                        </section><!-- /#our-goal-and-idea -->

                        <section id="our-goal-and-idea" class="section wow fadeInUp">
                            <h1>Advisors</h1>
                            <h2>MdZinnat Ali Khan</h2>
                            <p>President of Bangladesh ShadinataParishad, and advisor of many organizations,an enthusiastic person who is involved with so many organizations those are running their business very well and contribute to the economy of Bangladesh.</p>
                        </section><!-- /#our-goal-and-idea -->

                        <section id="our-goal-and-idea" class="section wow fadeInUp">
                            <h2>Md. Abdul JalilSohel</h2>
                            <p>Sohel is a Banker doing his job in a reputed commercial bank. He has very strong and sound knowledge in Information Technology. Along with his extra curricular activities, he is a well thinker and advisor of many entities. He played an important role to set up the business of ezbazarbd.com.</p>
                        </section><!-- /#our-goal-and-idea -->

                        <section id="our-goal-and-idea" class="section wow fadeInUp">
                            <h2>Sheikh Mohammad Zadu Mia</h2>
                            <p>Completed Post Graduation on Agri-Engineering from BAU is going to be good entrepreneur who wants to change the business and corporate system of the country. He is an advisor of ezbazarbd.com.</p>
                        </section><!-- /#our-goal-and-idea -->

                        <section id="our-goal-and-idea" class="section wow fadeInUp">
                            <h2>Jyotirmoy Sarker Sameer </h2>
                            <p>A young, energetic and efficient person who completed MSC in Forestry from Khulna University and MBA from Dhaka University now working in a commercial bank and also gives important suggestion to management team all the time to start and run the online superstore www.ezbazarbd.com.</p>
                        </section><!-- /#our-goal-and-idea -->

                        <section id="our-goal-and-idea" class="section wow fadeInUp">
                            <h2>Mazadul Islam Sumon</h2>
                            <p>Mr. Sumon is another dedicated entrepreneur who graduated from Dhaka University and also completed his MBA from North South University is giving support for taking key strategy to run the ezbazarbd smoothly. </p>
                        </section><!-- /#our-goal-and-idea -->

                    </div><!-- /.col -->
                    <div class="col-xs-12 col-md-4 col-lg-4 col-sm-6 wow fadeInDown">
                        @include('frontend.ezbazzar.team')

                    </div><!-- /.col -->
                </div><!-- /.row -->

            </div><!-- /.container -->

            <section id="what-can-we-do-for-you" class="row light-bg inner-sm color-bg">

                <div class="container">
                    <div class="row">
                        <div class="col-md-3 section m-t-0 wow fadeInRight">
                            <h2>What can we do for you ?</h2>
                            <p>Ezbazarbd is starting the journeyand hope to get better over time. We believe using this platform we are a strong participants of improvement of our beloved country Bangladesh.</p>
                        </div><!-- /.section -->

                        <div class="col-md-9">
                            <ul class="services list-unstyled row m-t-35">
                                <li class="col-md-4 wow fadeInRight">
                                    <div class="service">
                                        <div class="service-icon primary-bg"><i class="fa fa-truck"></i></div>
                                        <h3>Fast Delivery</h3>
<!--                                        <p>Sed in mi rutrum, mattis eros ut, sagittis orci. Suspendisse vehicula auctor leo, nec egestas tellus fringilla ac integer.</p>-->
                                    </div>
                                </li>
                                <li class="col-md-4 wow fadeInRight">
                                    <div class="service">
                                        <div class="service-icon primary-bg"><i class="fa fa-life-saver"></i></div>
                                        <h3>Support 24/7</h3>
<!--                                        <p>Sed in mi rutrum, mattis eros ut, sagittis orci. Suspendisse vehicula auctor leo, nec egestas tellus fringilla ac integer.</p>-->
                                    </div>
                                </li>
                                <li class="col-md-4 wow fadeInRight">
                                    <div class="service">
                                        <div class="service-icon primary-bg"><i class="fa fa-star"></i></div>
                                        <h3>Best Quality</h3>
<!--                                        <p>Sed in mi rutrum, mattis eros ut, sagittis orci. Suspendisse vehicula auctor leo, nec egestas tellus fringilla ac integer.</p>-->
                                    </div>
                                </li>
                            </ul><!-- /.services -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->

            </section><!-- /#what-can-we-do-for-you -->

            <section id="our-clients" class="row inner-sm">
                <div class="container">
                    <h2 class="sr-only">Our Clients</h2>
                    <ul class="list-unstyled row list-clients">
                        <li class="col-md-2"><a href="#"><img alt="" src="{{ asset('public/ezbazzar/images/brands/brand-01.jpg') }}" /></a></li>
                        <li class="col-md-2"><a href="#"><img alt="" src="{{ asset('public/ezbazzar/images/brands/brand-02.jpg') }}" /></a></li>
                        <li class="col-md-2"><a href="#"><img alt="" src="{{ asset('public/ezbazzar/images/brands/brand-03.jpg') }}" /></a></li>
                        <li class="col-md-2"><a href="#"><img alt="" src="{{ asset('public/ezbazzar/images/brands/brand-04.jpg') }}" /></a></li>
                        <li class="col-md-2"><a href="#"><img alt="" src="{{ asset('public/ezbazzar/images/brands/brand-01.jpg') }}" /></a></li>
                        <li class="col-md-2"><a href="#"><img alt="" src="{{ asset('public/ezbazzar/images/brands/brand-02.jpg') }}" /></a></li>
                    </ul>
                </div>
            </section><!-- /#our-clients .row -->
        </main><!-- /#about-us -->
</div>
<!-- ./page wapper-->
<!-- Footer -->
@endsection