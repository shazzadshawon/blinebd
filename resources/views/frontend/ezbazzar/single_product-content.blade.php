@php
    $product_images = DB::table('product_images')->where('product_id',$productInfo->id)->get();
@endphp


<div id="single-product" class="row">

    <div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
        <div class="product-item-holder size-big single-product-gallery small-gallery">

            <div id="owl-single-product">
                <div class="single-product-gallery-item" id="slide1">
                    <a data-rel="prettyphoto" href="#">
                        <img style="height: 450px" id="product_zoom" class="img-responsive" alt="" src="{{asset('product_image/'.$product_images[0]->product_image)}}" data-zoom-image="{{asset('product_image/'.$product_images[0]->product_image)}}" />

                    </a>
                </div><!-- /.single-product-gallery-item -->
            </div><!-- /.single-product-slider -->


            <div id="gallery_01" class="single-product-gallery-thumbs gallery-thumbs">
                <ul id="owl-single-product-thumbnails" class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false">
                    @foreach($product_images as $image)
                        @if(!empty($image))
                        <li>
                            <a class="horizontal-thumb" href="#" data-image="{{asset('product_image/'.$image->product_image)}}" data-zoom-image="{{asset('product_image/'.$image->product_image)}}">
                                <img id="product-zoom" class="img-responsive" src="{{asset('product_image/'.$image->product_image)}}" />
                            </a>
                        </li>
                        @endif
                        @endforeach


                </ul>
                <!-- /#owl-single-product-thumbnails -->

                <div class="nav-holder left hidden-xs">
                    <a class="prev-btn slider-prev" data-target="#owl-single-product-thumbnails" href="#prev"></a>
                </div><!-- /.nav-holder -->

                <div class="nav-holder right hidden-xs">
                    <a class="next-btn slider-next" data-target="#owl-single-product-thumbnails" href="#next"></a>
                </div><!-- /.nav-holder -->

            </div><!-- /.gallery-thumbs -->

        </div><!-- /.single-product-gallery -->
    </div><!-- /.gallery-holder -->

<!--    DESCRIPTION-->
    <div class="no-margin col-xs-12 col-sm-7 body-holder">
        <div class="body">

           
            <div class="availability"><label>availability:</label><span class="available">
                @if ($productInfo->product_quantity > 0)In Stock
                    @else Out of Stock
                    @endif
                  </span></div>
            <div class="title"><a href="#">{{$productInfo->product_name}}</a></div>

            @if(!empty($subcat))
                <div class="brand">{{ $subcat->sub_category_name }}</div>
            @endif






                              @php
                     
                            $wishlist = DB::table('wishlists')
                                    ->where('product_id', $productInfo->id)
                                    ->where('customer_id', Session::get('customer_id'))->first();
                               @endphp
                                          
                                   {{-- wish --}}
                     
                                        @if (Session::has('customer_id'))
                                      
                                        @if($wishlist==NULL)
                                        {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                        <div class="buttons-holder">
                                            <button type="submit" class="btn-add-to-wishlist">add to wishlist</button>
                                            </div>
                                         {!! Form::close() !!}
                                          @else
                                           <div class="buttons-holder">
                                                <a class="btn-add-to-wishlist" href="{{ url('view-wishlist') }}">add to wishlist</a>
                                            </div>
                                          @endif
                                        @else 
                                       
                                            <div class="buttons-holder">
                                                <a class="btn-add-to-wishlist" href="{{URL::to('/User-Register')}}">add to wishlist</a>
                                            </div>

                                   
                                        @endif




































            @php
                $sizes = DB::table('product_sizes')->where('product_id',$productInfo->id)->get();

            @endphp
             {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!}
            Size : <select name="size">
            @foreach ($sizes as $size)
                <option value="{{ $size->size }}">{{ $size->size }}</option>
                @endforeach
                </select>

          

            <div class="excerpt">
                    <p>@php
                        print_r($productInfo->description);
                    @endphp</p>
            </div>


            <div class="prices">
                @php
                    $discount_amount = ( $productInfo->product_price * $productInfo->discount / 100 );
                @endphp
                <div class="price-current">&#2547;{{ $productInfo->product_price - $discount_amount }}</div>
                <div class="price-prev">&#2547;{{ $productInfo->product_price }}</div>


            </div>


            <div class="qnt-holder">

                {{--<a id="addto-cart" href="cart.php" class="le-button huge">add to cart</a>--}}


                    <div class="button-group">
                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}

                        <div class="le-quantity">

                            {{--<a class="minus" href="#reduce"></a>--}}
                            <input name="product_quantity"  type="text" value="1" />
                            {{--<a class="plus" href="#add"></a>--}}

                        </div>

                        <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                        <input type="hidden" name="product_name" value="{{$productInfo->product_name}}">
                        <input type="hidden" name="product_name_bn" value="{{$productInfo->product_name_bn}}">
                        <input type="hidden" name="product_code" value="{{$productInfo->product_code}}">
                        <input type="hidden" name="product_price" value="@if($productInfo->discount > 0){{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100}}@else{{$productInfo->product_price}}@endif">
                        {{--<input type="hidden" name="product_quantity" value="1">--}}
                        <input type="hidden" name="publication_status" value="{{$productInfo->publication_status}}">

                        <button  class="btn btn-success btn-lg" type="submit">

                            Add To Cart

                        </button>

                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}
                    </div>



            </div><!-- /.qnt-holder -->
            {!! Form::close() !!}
        </div><!-- /.body -->

    </div><!-- /.body-holder -->
</div><!-- /.row #single-product -->

<script src='{{ asset('public/ezbazzar/js/jquery.elevatezoom.js') }}'></script>
<script>
    $('#product_zoom').elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 750,
        gallery:'gallery_01'
    });
</script>