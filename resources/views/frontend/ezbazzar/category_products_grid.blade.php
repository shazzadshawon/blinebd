
@foreach ($products as $top)
<div class="col-xs-12 col-sm-4 no-margin product-item-holder hover">
    <div class="product-item">

                                    <div class="image">
                                        @php
                                            $product_image = DB::table('product_images')->where('product_id',$top->id)->first();
                                        @endphp
                                        <img style="height: 200px" alt="" src="{{asset('product_image/'.$product_image->product_image)}}" data-echo="{{asset('product_image/'.$product_image->product_image)}}" />
                                    </div>
                                    <div class="body">

                                         @if(!empty( $top->discount ))
                                            <div class="label-discount green">{{ $top->discount  }}% Discount</div>
                                        @else
                                            <div class="label-discount green">0% Discount</div>
                                        @endif
                                        <div class="title">
                                            <a href="single-product.php">{{$top->product_name}}</a>
                                        </div>
                                       
                                       
                                    </div>
                                    <div class="prices">
                                        <div class="price-prev">&#2547;{{ $top->product_price }}</div>
                                        @php
                                            $discount_amount = ( $top->product_price * $top->discount / 100 );
                                        @endphp
                                        <div class="price-current pull-right">&#2547;{{ $top->product_price - $discount_amount }}</div>
                                    </div>

                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <a href="{{ url('/product-details/'.$top->id) }}" class="le-button">View Details</a>
                                        </div>
                                       {{--  <div class="wish-compare">
                                            <a class="btn-add-to-wishlist" href="#">Add to wishlist</a>
                                        </div> --}}
                                    </div>
                                </div>
</div><!-- /.product-item-holder -->
@endforeach


