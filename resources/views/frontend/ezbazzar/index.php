<!DOCTYPE html>
<html lang="en">
	<?php require('head.php'); ?>
    <title>Home|Ezbazzar</title>
<body>
	
	<div class="wrapper">
		<!-- ============================================================= TOP NAVIGATION ============================================================= -->
        <?php require_once('header_top.php');?>
        <!-- ============================================================= TOP NAVIGATION : END ============================================================= -->		<!-- ============================================================= HEADER ============================================================= -->
        <?php require_once('header_main.php');?>
        <!-- ============================================================= HEADER : END ============================================================= -->


        <div id="top-banner-and-menu">
            <div class="container">

                <div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">
                    <?php require('sidebar_navigation.php');?>
                </div><!-- /.sidemenu-holder -->

                <div class="col-xs-12 col-sm-8 col-md-9 homebanner-holder">
                    <?php require('home_slider.php');?>
                </div><!-- /.homebanner-holder -->

            </div><!-- /.container -->
        </div><!-- /#top-banner-and-menu -->

        <!-- ========================================= CATEGORY SECTION ========================================= -->
        <?php require('home_category.php'); ?>
        <!-- ========================================= CATEGORY SECTION END========================================= -->

        <!-- ========================================= BEST SELLERS ========================================= -->
        <?php require('home_best_seller.php'); ?>
        <!-- ========================================= BEST SELLERS : END ========================================= -->
		<!-- ============================================================= FOOTER ============================================================= -->
        <?php require('footer.php'); ?>
        <!-- ============================================================= FOOTER : END ============================================================= -->	</div><!-- /.wrapper -->

	<!-- JavaScripts placed at the end of the document so the pages load faster -->
    <?php require('javascripts.php'); ?>

</body>
</html>