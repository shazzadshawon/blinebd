<nav class="top-bar animate-dropdown">
    <div class="container">
        <div class="col-xs-12 col-sm-6 no-margin">
            <ul>
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('about-us')}}">About</a></li>
                <li><a href="{{url('contact-us')}}">Contact</a></li>
            </ul>
        </div><!-- /.col -->

        <div class="col-xs-12 col-sm-6 no-margin">
            <ul class="right">
                <li class="dropdown">
                    <a class="dropdown-toggle"  data-toggle="dropdown" href="#change-currency">My Account</a>
                    <ul class="dropdown-menu mega _dropdown" role="menu">
                        @if (Session::has('customer_id'))
                            <li><a href="{{ url('logout') }}">logout ({{ Session::get('customer_name') }})</a></li>
                            <li><a href="{{ url('view-wishlist') }}">Wishlists</a></li>
                        @else
                            <li><a href="{{ asset('User-Register') }}">Login / Register</a></li>
                        @endif


                    </ul>
                </li>
            </ul>
        </div><!-- /.col -->
    </div><!-- /.container -->
</nav><!-- /.top-bar -->