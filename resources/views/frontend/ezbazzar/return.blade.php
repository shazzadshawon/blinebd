@extends('layouts.frontend')

@section('content')

        <main id="about-us">
            <div class="container inner-top-xs inner-bottom-sm">

                <div class="row">
                    <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">

                        <section id="who-we-are" class="section m-t-0">
                            <h2>Return &amp; Refund Policy</h2>
                            <p>A user may return any unopened or defective item within 7 days of receiving the item. Ezbazar, at its own discretion, will refund the value of that item</p>
                            <p>Following products may not be eligible for return or replacement:</p>
                            <ul class="list-group">
                                <li class="list-group-item">Damages due to misuse of product</li>
                                <li class="list-group-item">Incidental damage due to malfunctioning of product</li>
                                <li class="list-group-item">Any consumable item which has been used/installed</li>
                                <li class="list-group-item">Products with tampered or missing serial/UPC numbers</li>
                                <li class="list-group-item">Any damage/defect which are not covered under the manufacturer's warranty</li>
                                <li class="list-group-item">Any product that is returned without all original packaging and accessories, including the box, manufacturer's packaging if any, and all other items originally included with the product/s delivered</li>
                            </ul>
                        </section><!-- /#who-we-are -->


                    </div><!-- /.col -->
                    <div class="col-xs-12 col-md-4 col-lg-4 col-sm-6">

                    @include('frontend.ezbazzar.team')

                    </div><!-- /.col -->
                </div><!-- /.row -->

            </div><!-- /.container -->
        </main><!-- /#about-us -->
    <!-- ========================================= MAIN : END ========================================= 
    @endsection