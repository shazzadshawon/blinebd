@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>Achievements</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">Achievements</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!--About Office Section-->
    <section id="" class="about-section">
        <div align="center" class="sec-title">
            <h2><span>Achievement</span></h2>
        </div>
        <div class="auto-container">

@php
$i=0;
@endphp
                @foreach($achievements as $office)
                    @if($i%2==0)
                <div class="row clearfix">
                    <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="icon-box"><span class=""></span></div>
                    <div class="inner-box">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <div class="title">Achievement</div>
                            <h2> <span>{{$office->title}}</span></h2>
                        </div>
                        <div class="text">
                            <p class="dark-text">
                                <?php print_r($office->description); ?>
                            </p>
                        </div>
                        <!--                                <a href="#" class="theme-btn btn-style-two">Read More</a>-->
                    </div>
                </div>
                <!--Image Column-->
                <div class="image-column col-md-6 col-sm-12 col-xs-12">
                    <!--video-box-->
                    <div class="video-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image">
                            <img src="{{asset('public/uploads/achievement/'.$office->image)}}" alt="" />
                        </figure>
                        <!--                                <a href="https://www.youtube.com/watch?v=0e1uTwSRGgI" class="lightbox-image overlay-box"><span class="flaticon-arrows-5"></span></a>-->
                    </div>
                </div>
                </div>
                @else
                    <div class="row clearfix">

                        <!--Image Column-->
                        <div class="image-column col-md-6 col-sm-12 col-xs-12">
                            <!--video-box-->
                            <div class="video-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="image">
                                    <img src="{{asset('public/uploads/achievement/'.$office->image)}}" alt="" />
                                </figure>
                                <!--                                <a href="https://www.youtube.com/watch?v=0e1uTwSRGgI" class="lightbox-image overlay-box"><span class="flaticon-arrows-5"></span></a>-->
                            </div>
                        </div>

                        <div class="content-column col-md-6 col-sm-12 col-xs-12">
                            <div class="icon-box"><span class=""></span></div>
                            <div class="inner-box">
                                <!--Sec Title-->
                                <div class="sec-title">
                                    <div class="title">Achievement</div>
                                    <h2> <span>{{$office->title}}</span></h2>
                                </div>
                                <div class="text">
                                    <p class="dark-text">
                                        <?php print_r($office->description); ?>
                                    </p>
                                </div>
                                <!--                                <a href="#" class="theme-btn btn-style-two">Read More</a>-->
                            </div>
                        </div>

                    </div>

                @endif
                        @php
                            $i++;
                        @endphp
                <br><br><br>
                    @endforeach
                <!--Content Column-->




        </div>
    </section>
    <!--End About Office Section-->



@endsection