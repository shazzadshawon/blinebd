@extends('layouts.frontend')

@section('content')

  <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                   @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->

                    <!--   SideBar List-->
                    @include('frontend/include/sidebar_list')
                    <!-- /Block categories module -->

                </div>


                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe">&gt;</span>
                        <a href="" title="Store">Store</a><span
                            class="navigation-pipe">&gt;</span>{{ $category->category_name }}
                    </div>
                    <!-- /Breadcrumb -->

                    <h1 class="page-heading product-listing"><span class="cat-name">{{ $category->category_name }}&nbsp;</span><span class="heading-counter">There are {{ count($subcategories) }} Sub Categories.</span></h1>

                    <!-- Subcategories -->
                    
                    @include('frontend/include/subcategoryList')
                    <!-- //Subcategories -->

                    <div class="content_sortPagiBar clearfix">
                        <div class="sortPagiBar clearfix">
                            <ul class="display hidden-xs">
                                <li class="display-title">View:</li>
                                <li id="grid"><a rel="nofollow" href="#" title="Grid"><i class="icon-th-large"></i>Grid</a></li>
                                <li id="list"><a rel="nofollow" href="#" title="List"><i class="icon-th-list"></i>List</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Products list -->
                    <ul class="product_list grid row">
                        
                        @include('frontend/include/product_list_items')
                    </ul>

                    <br>
<p>{{ $products->links() }}</p>



                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->
@endsection