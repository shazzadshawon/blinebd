@extends('layouts.frontend')

@section('content')

 <div class="columns-container">
            <div id="columns" class="container">
                <div class="row">
                    <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                        
                        @include('frontend/include/weekly_special')

                        <!-- Home Slider -->
                        
                        @include('frontend/include/home_slider')
                        <!-- /Home Slider -->
                    </div>
                </div>

                <div class="row" id="columns_inner">
                    <div id="center_column" class="center_column col-xs-12" style="width:100%;">

                        <!--Featured Products and New Products Tab-->
                        
                        @include('frontend/include/featuredProduct')
                        <!--//Featured Products and New Products Tab-->

                        
                        @include('frontend/include/top_saller')

                    </div><!-- #center_column -->
                </div><!-- .row -->

            </div><!-- #columns -->
        </div><!-- .columns-container -->


@endsection