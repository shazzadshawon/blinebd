<header id="header">
    <div class="banner">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>

    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div id="header_logo">
                    <a href="{{ url('/') }}" title="Bline BD">
                        <img class="logo img-responsive" src="{{asset('public/img/logo.png')}}" alt="Bline BD"
                             width="300" height="37"/>
                    </a>
                </div>

                <!-- Block search module TOP -->
                <div id="search_block_top" class="col-sm-4 clearfix">
                    <form id="searchbox" method="get"
                          action="{{ url('product_search') }}">
                       
                        <input class="search_query form-control" type="text" id="search_query_top"
                               name="query" placeholder="Search" value="" required/>
                        <button type="submit" name="" class="btn btn-default button-search">
                            <span>Search</span>
                        </button>
                    </form>
                </div>
                <!-- /Block search module TOP -->

                <ul>
                    <li class="tmtopbanner-container">
                       <h4 title="Contact No." style="color:#ffffff; font-size:20px">
                       <i class="fa fa-phone" aria-hidden="true"></i>
  +88-02-9552038
                     </h4>
                    </li>
                </ul>
                <!-- Menu -->

                @include('frontend/include/header_menu')
                <!--/ Menu -->
            </div>
        </div>
    </div>

</header>