<!--1 product   ==> classes ==> item, first-in-line, first-item-of-tablet-line, first-item-of-mobile-line-->
<!--2 Products  ==> Classes ==> item, last-item-of-mobile-line-->
<!--3 Products  ==> Classes ==> item, first-item-of-mobile-line-->
<!--4 Products  ==> Classes ==> item, last-item-of-tablet-line, last-item-of-mobile-line-->
<!--5 Products  ==> Classes ==> item,  last-in-line, first-item-of-tablet-line, first-item-of-mobile-line-->


@foreach($releted as $f)


@if(!empty($f))
@php
$image = DB::table('product_images')->where('product_id',$f->id)->orderby('product_image_id','desc')->first();
@endphp

<!--First Item of Mobile view    -->
<li class=" item  first-item-of-mobile-line">
    <div class="product-container" itemscope>
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link"
                   href="{{ url('view_product/'.$f->id) }}"
                   title="Product Name" itemprop="url">
                    <img class="replace-2x img-responsive"
                        src="{{ asset('product_image/'.$image->product_image) }}"
                         alt="{{ $f->product_name }}"
                         title="{{ $f->product_name }}"
                         style="height: 185px;"
                         itemprop="image"/>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;
                        @if($f->discount > 0) 
                            {{ $f->product_price-($f->product_price*$f->discount)/100}}
                        @else
                            {{$f->product_price}}
                        @endif
                    </span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
                 @if($f->discount > 0)  
                    <span class="new-box">
                                          
                             <span class="new-label">{{ $f->discount }} % discount</span>

                    </span>
                @endif
                                           
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="{{ url('view_product/'.$f->id) }}"
                   title="Product Name" itemprop="url">
                    {{ $f->product_name }}
                </a>
            </h5>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                <span itemprop="price" class="price product-price">&#2547;
 @if($f->discount > 0) 
                            {{ $f->product_price-($f->product_price*$f->discount)/100}}
                        @else
                            {{$f->product_price}}
                        @endif
                </span>
                <meta itemprop="priceCurrency" content="1"/> 
                @if($f->discount > 0)  
                <small>
                    <span style="text-decoration: line-through;" >&#2547;                        
                            {{$f->product_price}} 
                    </span>
                   
                </small>
                @endif
            </div>
           

{{-- 
            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="2">
                    <span>Add to cart</span>
                </a>

                <a itemprop="url" class="button lnk_view btn btn-default"
                   href="singleProduct.php"
                   title="Product Name">
                    <span>More</span>
                </a>

                <a class="quick-view"
                   href="singleProduct.php"
                   rel="index.php?id_product=2&amp;controller=product&amp;id_lang=1"
                   title="Quick View">
                    <span>Quick view</span>
                </a>

            </div> --}}
            <div class="product-flags">
            </div>
        </div>
    </div><!-- .product-container> -->
</li>
<!--//First Item of Mobile view    -->
@endif


@endforeach