<section id="media_section" class="project-section  bg-violate">
    <div class="auto-container">
        <!--Sec Title Four-->
        <div class="sec-title-two">
            <div class="title">Our Works</div>
            <h2>Media</h2>
        </div>

        <div class="row clearfix">

            <!--Project Block-->
            <?php foreach ($galleries as $gal): ?>
            <div class="project-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="image-box">
                        <img style="height: 250px" src="{{asset('public/uploads/gallery/'.$gal->gallery_image)}}" alt="" />
                        <div class="overlay-box">
                            <a href="{{asset('public/uploads/gallery/'.$gal->gallery_image)}}" class="lightbox-image" data-fancybox-group="project-block"><span class="icon flaticon-plus"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
           


        </div>

    </div>
</section>