@if(count($topsales) > 0)

<div id="best-sellers_block_right" class="block products_block">
    <h4 class="title_block">
        <a href="#"> Top sellers </a>
    </h4>

    <div class="block_content products-block">
        <div class=" jcarousel-skin-tango">
            <div style="position: relative; display: block;" class="jcarousel-container jcarousel-container-vertical">
                <div style="overflow: hidden; position: relative;" class="jcarousel-clip jcarousel-clip-vertical">
                    <ul id="prod-slider" class="product_images">

                        @foreach($topsales as $top)
                        @php
                        $image = DB::table('product_images')->where('product_id',$top->id)->orderby('product_image_id','desc')->first();
                        @endphp

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="{{ url('view_product/'.$top->id) }}" title="" class="content_img clearfix">
                                <img src="{{ asset('product_image/'.$image->product_image) }}" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="{{ url('view_product/'.$top->id) }}" title="">{{ $top->product_name }}<br/>
                                    <span class="price">&#2547;


                                         @if($top->discount > 0) 
                            {{ $top->product_price-($top->product_price*$top->discount)/100}}
                        @else
                            {{$top->product_price}}
                        @endif

                                    </span> </a>
                            </div>
                        </li>

                        @endforeach
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endif