<section class="main-slider" data-start-height="850" data-slide-overlay="yes" id="home">

    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                @include('frontend.include.slides')
            </ul>

            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>