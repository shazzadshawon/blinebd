@php
$categories = DB::table('categories')->where('publication_status',1)->get();
@endphp

<div id="categories_block_left" class="block">
    <h2 class="title_block">
        Categories
    </h2>
    <div class="block_content">
        <ul class="tree dhtml">
@foreach($categories as $cat)
            <li >
                <a
                        href="{{ url('product_category/'.$cat->category_id) }}" title="">
                    {{ $cat->category_name }}
                </a>
                <ul>
                    @php
                    $sub_categories = DB::table('sub_categories')->where('category_id',$cat->category_id)->get();
                    @endphp
                    @foreach($sub_categories as $sub)

                    <li >
                        <a
                                href="{{ url('product_sub_category/'.$sub->sub_category_id) }}" title="">
                            {{ $sub->sub_category_name }}
                        </a>
                       
                    </li>


                    @endforeach

                  

                </ul>
            </li>

@endforeach
          
    

        </ul>
    </div>
</div>