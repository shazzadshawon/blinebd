<!--1 product   ==> classes ==> item, first-in-line, first-item-of-tablet-line, first-item-of-mobile-line-->
<!--2 Products  ==> Classes ==> item, last-item-of-mobile-line-->
<!--3 Products  ==> Classes ==> item, first-item-of-mobile-line-->
<!--4 Products  ==> Classes ==> item, last-item-of-tablet-line, last-item-of-mobile-line-->
<!--5 Products  ==> Classes ==> item,  last-in-line, first-item-of-tablet-line, first-item-of-mobile-line-->


@for($i=0;$i<count($featured);$i+=5)

@if(!empty($featured[$i]))
@php
$image = DB::table('product_images')->where('product_id',$featured[$i]->id)->orderby('product_image_id','desc')->first();
@endphp

<!-- First Item of desktop view, mobile view, tablet view-->
<li class=" item  first-in-line first-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container" itemscope>
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link" href="singleProduct.php" title="Product Name" itemprop="url">
                    <img class="replace-2x img-responsive"
                         src="{{ asset('product_image/'.$image->product_image) }}"
                         alt="Product Name"
                         title="Product Name"
                         style="height: 185px;"
                         itemprop="image"/>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;125.00</span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
               {{--  <span class="new-box">
                        <span class="new-label">New</span>
                    </span> --}}
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    Product Name
                </a>
            </h5>
            <div itemprop="offers" itemscope class="content_price" itemtype="http://schema.org/Offer">
                <span itemprop="price" class="price product-price">&#2547;125.00</span>
                <meta itemprop="priceCurrency" content="1"/>
            </div>

            <div class="comments_note" itemprop="aggregateRating" itemscope>
                <div class="star_content clearfix">
                    <div class="star star_on"></div>
                    <div class="star star_on"></div>
                    <div class="star star_on"></div>
                    <div class="star star_on"></div>
                    <div class="star star_on"></div>
                    <meta itemprop="worstRating" content="0"/>
                    <meta itemprop="ratingValue" content="2"/>
                    <meta itemprop="bestRating" content="5"/>
                </div>
                <span class="nb-comments">1 Review(s)</span>
            </div>

            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="1">
                    <span>Add to cart</span>
                </a>

                <a itemprop="url" class="button lnk_view btn btn-default"
                   href="singleProduct.php"
                   title="Product Name">
                    <span>More</span>
                </a>

                <a class="quick-view"
                   href="singleProduct.php"
                   rel="index.php?id_product=1&amp;controller=product&amp;id_lang=1"
                   title="Quick View">
                    <span>Quick view</span>
                </a>

            </div>
            <div class="product-flags">
            </div>
        </div>
    </div><!-- .product-container> -->
</li>
@endif
<!-- First Item of desktop view, mobile view, tablet view-->
@if(!empty($featured[$i+1]))
@php
$image1 = DB::table('product_images')->where('product_id',$featured[$i+1]->id)->orderby('product_image_id','desc')->first();
@endphp

<!--Last Item of Mobile view    -->
<li class=" item  last-item-of-mobile-line">
    <div class="product-container" itemscope>
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    <img class="replace-2x img-responsive"
                        src="{{ asset('product_image/'.$image->product_image) }}"
                        alt="Product Name"
                        title="Product Name"
                        itemprop="image"
                        style="height: 185px;"/>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;152.46</span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
                <span class="new-box">
                                                <span class="new-label">New</span>
                                            </span>
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    Product Name
                </a>
            </h5>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                <span itemprop="price" class="price product-price">&#2547;152.46</span>
                <meta itemprop="priceCurrency" content="1"/>
            </div>


            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="2">
                    <span>Add to cart</span>
                </a>

                <a itemprop="url" class="button lnk_view btn btn-default"
                   href="singleProduct.php"
                   title="Product Name">
                    <span>More</span>
                </a>

                <a class="quick-view"
                   href="singleProduct.php"
                   rel="index.php?id_product=2&amp;controller=product&amp;id_lang=1"
                   title="Quick View">
                    <span>Quick view</span>
                </a>

            </div>
            <div class="product-flags">
            </div>
        </div>
    </div><!-- .product-container> -->
</li>
@endif
<!--//Last Item of Mobile view    -->
@if(!empty($featured[$i+2]))
@php
$image2 = DB::table('product_images')->where('product_id',$featured[$i+2]->id)->orderby('product_image_id','desc')->first();
@endphp
<!--First Item of Mobile view    -->
<li class=" item  first-item-of-mobile-line">
    <div class="product-container" itemscope>
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    <img class="replace-2x img-responsive"
                        src="{{ asset('product_image/'.$image->product_image) }}"
                         alt="Product Name"
                         title="Product Name"
                         style="height: 185px;"
                         itemprop="image"/>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;152.46</span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
                <span class="new-box">
                                            <span class="new-label">New</span>
                                        </span>
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    Product Name
                </a>
            </h5>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                <span itemprop="price" class="price product-price">&#2547;152.46</span>
                <meta itemprop="priceCurrency" content="1"/>
            </div>


            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="2">
                    <span>Add to cart</span>
                </a>

                <a itemprop="url" class="button lnk_view btn btn-default"
                   href="singleProduct.php"
                   title="Product Name">
                    <span>More</span>
                </a>

                <a class="quick-view"
                   href="singleProduct.php"
                   rel="index.php?id_product=2&amp;controller=product&amp;id_lang=1"
                   title="Quick View">
                    <span>Quick view</span>
                </a>

            </div>
            <div class="product-flags">
            </div>
        </div>
    </div><!-- .product-container> -->
</li>
<!--//First Item of Mobile view    -->
@endif
@if(!empty($featured[$i+3]))
@php
$image3 = DB::table('product_images')->where('product_id',$featured[$i+3]->id)->orderby('product_image_id','desc')->first();
@endphp
<!--  Last Item of tablet view and Last Item of Mobile view   -->
<li class=" item  last-item-of-tablet-line last-item-of-mobile-line">
    <div class="product-container" itemscope>
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    <img class="replace-2x img-responsive"
                         src="{{ asset('product_image/'.$image->product_image) }}"
                         alt="Product Name"
                         title="Product Name"
                         style="height: 185px;"
                         itemprop="image"/>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;152.46</span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
                <span class="new-box">
                                        <span class="new-label">New</span>
                                    </span>
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    Product Name
                </a>
            </h5>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                <span itemprop="price" class="price product-price">&#2547;152.46</span>
                <meta itemprop="priceCurrency" content="1"/>
            </div>


            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="2">
                    <span>Add to cart</span>
                </a>

                <a itemprop="url" class="button lnk_view btn btn-default"
                   href="singleProduct.php"
                   title="Product Name">
                    <span>More</span>
                </a>

                <a class="quick-view"
                   href="singleProduct.php"
                   rel="index.php?id_product=2&amp;controller=product&amp;id_lang=1"
                   title="Quick View">
                    <span>Quick view</span>
                </a>

            </div>
            <div class="product-flags">
            </div>
        </div>
    </div><!-- .product-container> -->
</li>
<!--//  Last Item of tablet view and Last Item of Mobile view    -->
@endif
@if(!empty($featured[$i+4]))
@php
$image4 = DB::table('product_images')->where('product_id',$featured[$i+4]->id)->orderby('product_image_id','desc')->first();
@endphp
<!--  Last Item of Desktop view and First Item of Tablet view and First item of mobile view   -->
<li class=" item  last-in-line first-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container" itemscope>
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    <img class="replace-2x img-responsive"
                        src="{{ asset('product_image/'.$image->product_image) }}"
                         alt="Product Name"
                         title="Product Name"
                         style="height: 185px;"
                         itemprop="image"/>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;152.46</span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
                <span class="new-box">
                                    <span class="new-label">New</span>
                                </span>
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="singleProduct.php"
                   title="Product Name" itemprop="url">
                    Product Name
                </a>
            </h5>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                <span itemprop="price" class="price product-price">&#2547;152.46</span>
                <meta itemprop="priceCurrency" content="1"/>
            </div>


            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="2">
                    <span>Add to cart</span>
                </a>

                <a itemprop="url" class="button lnk_view btn btn-default"
                   href="singleProduct.php"
                   title="Product Name">
                    <span>More</span>
                </a>

                <a class="quick-view"
                   href="singleProduct.php"
                   rel="index.php?id_product=2&amp;controller=product&amp;id_lang=1"
                   title="Quick View">
                    <span>Quick view</span>
                </a>

            </div>
            <div class="product-flags">
            </div>
        </div>
    </div><!-- .product-container> -->
</li>
<!--//  Last Item of Desktop view and First Item of Tablet view and First item of mobile view    -->
@endif



@endfor