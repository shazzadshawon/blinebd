<div id="tm_toplink" class="block tm_toplinkblock">
   
  @if(Session::has('message'))
  <div class="container">
  	 <div class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>{{ Session('message') }}</strong>
  </div>
  </div>
  @endif
   
</div>