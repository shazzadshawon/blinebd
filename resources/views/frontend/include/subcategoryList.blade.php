<div id="subcategories">
    <p class="subcategory-heading">Sub Categories</p>
    <ul class="clearfix">

        @foreach($subcategories as $sub)

        <li>
            <div class="subcategory-image">
                <a href="{{ url('product_sub_category/'.$sub->sub_category_id) }}"
                   title="{{ $sub->sub_category_name }}" class="img">
                    <img class="replace-2x" src="{{ asset('public/no_image.jpg') }}" alt="{{ $sub->sub_category_name }}"
                         width="70" height="70"/>
                </a>
            </div>
            <h5><a class="subcategory-name"
                   href="{{ url('product_sub_category/'.$sub->sub_category_id) }}">{{ $sub->sub_category_name }}</a></h5>
            
        </li>

        @endforeach

      

    </ul>
</div>