<meta charset="utf-8" />
<meta name="description" content="Shop powered by Shobarjonnoweb" />
<meta name="generator" content="Shobarjonnoweb" />
<meta name="robots" content="index,follow" />
<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('public/index.ico') }}" />



<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/global.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/autoload/uniform.default.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/js/jquery/plugins/fancybox/jquery.fancybox.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/stores.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/cms.css') }}" type="text/css" media="all" />

<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/product.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/print.css') }}" type="text/css" media="print" />
<link rel="stylesheet" href="{{ asset('public/js/jquery/plugins/jqzoom/jquery.jqzoom.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockcart/blockcart.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockcurrencies/blockcurrencies.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blocklanguages/blocklanguages.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/product_list.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/category.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/js/jquery/plugins/autocomplete/jquery.autocomplete.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blocksearch/blocksearch.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blocktags/blocktags.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockuserinfo/blockuserinfo.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockviewed/blockviewed.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockwishlist/blockwishlist.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/productcomments/productcomments.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/sendtoafriend/sendtoafriend.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/socialsharing/css/socialsharing.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockpermanentlinks/blockpermanentlinks.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blockcategories/blockcategories.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmfeatureproducts/css/tmfeatureproducts.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmnewproducts/tmnewproducts.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmtopseller/tmtopseller.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmhomeslider/css/tmhomeslider.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blocknewsletter/blocknewsletter.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/crossselling/css/crossselling.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/productscategory/css/productscategory.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blocktopmenu/css/blocktopmenu.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/themes/PRS060150/css/modules/blocktopmenu/css/superfish-modified.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmhomepagecms/css/tmstyle.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmhometextbannercms/css/tmstyle.css') }}" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('public/modules/tmfooterblockcms/css/tmstyle.css') }}" type="text/css" media="all" />

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<!-- ================ Additional Links By Tempaltemela : START  ============= -->
<link rel="stylesheet" type="text/css" href="{{ asset('public/themes/PRS060150/css/megnor/custom.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/themes/PRS060150/css/megnor/skin.css') }}" />
<!-- ================ Additional Links By Tempaltemela : END  ============= -->

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans" type="text/css" media="all" />
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<!--[if IE 8]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->