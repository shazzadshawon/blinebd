<!--Line One-->

@php

$categories = DB::table('categories')->where('publication_status',1)->get();

@endphp
@foreach($categories as $cat)
<li>
    <a href="{{ url('product_category/'.$cat->category_id) }}" title="{{ $cat->category_name }}">{{ $cat->category_name }}</a>
    @php

    $sub_categories = DB::table('sub_categories')->where('category_id',$cat->category_id)->where('publication_status',1)->get();

    @endphp
    @if(!empty($sub_categories))
    <ul>
        @foreach($sub_categories as $sub)
        <li><a href="{{ url('product_sub_category/'.$sub->sub_category_id) }}" title="{{ $sub->sub_category_name }}">{{ $sub->sub_category_name }}</a></li>
        @endforeach
    </ul>
    @endif
</li>

@endforeach
{{-- 
<li class="category-thumbnail"></li> --}}