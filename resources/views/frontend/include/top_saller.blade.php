<div class="clearfix">

    <div id="best-sellers_block_center" class="block products_block">
        <h3 class="page-product-heading">Top sellers</h3>
        <div class="block_content">

            <div class="customNavigation">
                <a class="btn prev bestseller_prev"><i class="icon-chevron-sign-left"></i></a>
                <a class="btn next bestseller_next"><i class="icon-chevron-sign-right"></i></a>
            </div>
            <ul id="bestseller-carousel" class="tm-carousel product_list">


                @include('frontend/include/sale_product_items')


            </ul>
        </div>
    </div>
    <!-- TM - NewProduct -->
</div>