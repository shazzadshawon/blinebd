<div class="container" id="footer-top">

    <!-- MODULE TM - CMS BLOCK  -->
    <div id="tm_footer_top_cms_block">
        <div class="footer_top_cms">
            <div class="footer_cms">
                <div class="footer_cmsblock">
                    <div class="footer_cms1 f_cms">
                        <div class="footer_content"><a href="#">Best Prices & Best Deals</a></div>
                    </div>
                    <div class="footer_cms2 f_cms">
                        <div class="footer_content"><a href="#">Best Service After buy</a></div>
                    </div>
                    <div class="footer_cms3 f_cms">
                        <div class="footer_content"><a href="#">With Live Chat online</a></div>
                    </div>
                    <div class="footer_cms4 f_cms">
                        <div class="footer_content"><a href="#">Same Day Shipment</a></div>
                    </div>
                    <div class="footer_cms5 f_cms">
                        <div class="footer_content"><a href="#">A Worldwide Shipping</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /MODULE TM - CMS BLOCK  -->
</div>
<div class="footer-container">
    <footer id="footer" class="container">
        <div class="row">
            <!-- MODULE Block footer -->
            <div class="bottom-footer col-xs-12">

                Created with <i class="fa fa-heart" style="color:goldenrod" aria-hidden="true"></i> By</span> <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a>

            </div>
            <!-- /MODULE Block footer -->

            <!-- MODULE TM - CMS BLOCK  -->
            <div id="tm_footer_block_cms" class="footer-block col-xs-12 col-sm-3">
                <div class="footer_cms_block">
                    <div class="footer_link_quick">
                        <h4 class="title_block toggle"><a href="#">Quick Go &gt;</a></h4>
                        <div class="toggle-footer">
                            <ul class="bullet">
                               

                                <li>
                                    <div class="img img2"></div>
                                    <a href="{{ url('place_order') }}">Order</a></li>
                                <li>
                                    <div class="img img3"></div>
                                    <a href="{{ url('about') }}">About</a></li>
                                <li>
                                    <div class="img img4"></div>
                                    <a href="{{ url('contact') }}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /MODULE TM - CMS BLOCK  -->
            <!-- Block links module -->
            <section class="footer-block col-xs-12 col-sm-4" id="tm_links_block4_footer">
                <h4 class="title_block">
                    <a href="#" title="Support">Support</a>
                </h4>
                <div class="block_content toggle-footer">
                    <ul class="bullet">


                        @php
                        $cat=DB::table('categories')->where('publication_status',1)->first();
                        @endphp

                        <li><a href="{{ url('terms') }}" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
                        <li><a href="{{ url('return') }}" title="Product Return Policy">Product Return Policy</a></li>
                       @if(!empty($cat))
                        <li><a href="{{ url('product_category/'.$cat->category_id) }}" title="Product Sales">Product Sales</a></li>
                       @endif
                        {{-- <li><a href="returnPolicy.php" title="Delivery Information">Delivery Information</a></li> --}}
                    </ul>
                </div>
            </section>
            <!-- /Block links module -->
            <!-- Block links module -->
            <section class="footer-block col-xs-12 col-sm-4" id="tm_links_block5_footer">
                <h4 class="title_block">
                    <a href="#" title="Policy">Contact Us</a>
                </h4>
                <div class="block_content toggle-footer">
                    <ul class="bullet">

                        <li class="address">B-line enterprise, </br> Radio Biponi, </br> 25, Swimmingpool Market, </br> Bangabandhu National Stadium Rd, </br> Dhaka-1000 Bangladesh.</li>
                        <li class="contact"><strong>CONTACT PHONE :</br> </strong> +88-02-9552038</li>
                        <li class="email"><strong>E-MAIL ADDRESSES </strong> : <a href="#">info@bline-bd.com</a></li>
                        <!--<li class="skype"><strong>SKYPE :</strong> lorem.isum, sit amet</li>-->
                    </ul>
                </div>
            </section>
            <!-- /Block links module -->

            <!-- Block links module -->
            <section class="footer-block col-xs-12 col-sm-4" id="tm_links_block2_footer">
                <h4 class="title_block">
                    <a href="#" title="How can we help you?">Newsletter</a>
                </h4>
                <div id="footer_newsletter" class="block_content toggle-footer">
                    <div class="newsletter-block">
                        <div id="newsletter_block_left" class="block">
                            <h4>Newsletter</h4>
                            <div class="block_content">
                                 <form action="{{ url('subscribe') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input class="inputNew form-control grey newsletter-input"
                                               id="newsletter-input" type="text" name="email" size="18"
                                               placeholder="Enter your e-mail"/>
                                        <button type="submit" name="submitNewsletter"
                                                class="btn btn-default button button-small">
                                            <span>Ok</span>
                                        </button>
                                        <input type="hidden" name="action" value="0"/>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section id="social_block">
                <ul>
                    <li class="facebook">
                        <a target="_blank" href="http://www.facebook.com/prestashop">
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li class="twitter">
                        <a target="_blank" href="http://www.twitter.com/prestashop">
                            <span>Twitter</span>
                        </a>
                    </li>
                    <li class="rss">
                        <a target="_blank" href="http://www.prestashop.com/blog/en/">
                            <span>RSS</span>
                        </a>
                    </li>
                    <li class="google-plus">
                        <a href="https://www.google.com/+prestashop">
                            <span>Google Plus</span>
                        </a>
                    </li>
                    <li class="pinterest">
                        <a href="https://www.pinterest.com/+prestashop">
                            <span>Pinterest</span>
                        </a>
                    </li>
                </ul>
                <h4>Follow us</h4>
            </section>

            <!-- /Block links module -->
        </div>

    </footer>
    <div class="footer-bottom"></div>
</div>