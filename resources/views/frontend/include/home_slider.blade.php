<div class="flexslider">
    <ul class="slides">
        @foreach($sliders as $slide)
        <li class="tmhomeslider-container" id="slide_1">
            <a href="#" title="mainbanner-1">
                <img src="{{ asset('slider_image/'.$slide->slider_image) }}" alt="mainbanner-1" style="height: 450px" />
            </a>
        </li>
        @endforeach
       
    </ul>
</div>

<!-- Sales Point  -->
<div id="tm_slider_right_cms">
    <div class="col-xs-12">
        <div class="cms_product block">
            <img src="{{ asset('public/uploads/business/'.$business->image) }}" style="width: 100%; height: 100%;">
            <ul>
                <li>
                    <div class="cms_block" style="background: #f2d579; padding-left: 5%; overflow: hidden;">
                        <div class="name" style="background: #bda350;"><a href="#">{{ $business->title }}</a></div>
                       
                        <div class="desc">
                             <address>
                                <?php print_r($business->description); ?>
                            </address>
                        </div>
                        
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /Sales Point  -->