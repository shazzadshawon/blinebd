<div class="clearfix">

    <div id="best-sellers_block_center" class="block products_block">
        
        <div class="block_content">

            <div class="customNavigation">
                <a class="btn prev bestseller_prev"><i class="icon-chevron-sign-left"></i></a>
                <a class="btn next bestseller_next"><i class="icon-chevron-sign-right"></i></a>
            </div>
            <ul id="bestseller-carousel" class="tm-carousel product_list">


                @include('frontend/include/rel_item')


            </ul>
        </div>
    </div>
    <!-- TM - NewProduct -->
</div>