
@foreach($products as $f)

@php
$image = DB::table('product_images')->where('product_id',$f->id)->first();
@endphp

<li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 first-in-line first-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container" itemscope itemtype="http://schema.org/Product">
        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link"
                   href="{{ url('view_product/'.$f->id) }}"
                   title="Product Name " itemprop="url">
                    <img class="replace-2x img-responsive"
                         src="{{ asset('product_image/'.$image->product_image) }}" alt="{{ $f->product_name }}"
                         title="{{ $f->product_name }} " width="200" height="200"
                         style="height: 200px;"
                         itemprop="image"/>
                </a>
                <a class="quick-view"
                   href="{{ url('view_product/'.$f->id) }}"
                   rel="">
                    <span>Quick view</span>
                </a>
                <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" class="price product-price">&#2547;

                        @if($f->discount > 0) 
                            {{ $f->product_price-($f->product_price*$f->discount)/100}}
                        @else
                            {{$f->product_price}}
                        @endif

                    </span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>

              {{--   <span class="new-box">
                    <span class="new-label">New</span>
                </span>
                <span class="sale-box">
                    <span class="sale-label">Sale!</span>
                </span> --}}
                <div class="product-flags">
                </div>
            </div>
        </div>
        <div class="right-block">
            <h5 itemprop="name">
                <a class="product-name"
                   href="{{ url('view_product/'.$f->id) }}"
                   title="{{ $f->product_name }}" itemprop="url">
                    {{ $f->product_name }}
                </a>
            </h5>

            <p class="product-desc" itemprop="description">
               
            </p>
            <div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <span itemprop="price" class="price product-price"> &#2547;
                        @if($f->discount > 0) 
                            {{ $f->product_price-($f->product_price*$f->discount)/100}}
                        @else
                            {{$f->product_price}}
                        @endif
                 </span>
                <meta itemprop="priceCurrency" content="1"/>
            </div>

{{-- 
            <div class="button-container inner">

                <a class="button ajax_add_to_cart_button btn btn-default"
                   href="#"
                   rel="nofollow" title="Add to cart" data-id-product="14">
                    <span>Add to cart</span>
                </a>

                <div class="functional-buttons clearfix">

                    <div class="wishlist">
                        <a class="addToWishlist wishlistProd_14" href="#" rel="14" title="Add to Wishlist"> Add to Wishlist </a>
                    </div>
                    <div class="compare">
                        <a class="add_to_compare" href="#" data-id-product="14" title="Add to Compare">Add to Compare</a>
                    </div>
                </div>

            </div> --}}

            <div class="product-flags">
            </div>
            <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                <span class="available-now"><link itemprop="availability" href="http://schema.org/InStock"/>In stock </span>
            </span>

        </div>
        <div class="functional-buttons clearfix">

            <div class="wishlist">
                <a class="addToWishlist wishlistProd_14" href="#" rel="14" title="Add to Wishlist">Add to Wishlist</a>
            </div>
            <div class="compare">
                <a class="add_to_compare" href="#" data-id-product="14">Add to Compare</a>
            </div>
        </div>
    </div><!-- .product-container> -->
</li>


@endforeach
