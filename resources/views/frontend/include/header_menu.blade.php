<div id="block_top_menu" class="sf-contener clearfix col-lg-12">
    <div class="cat-title">Categories</div>
    <ul class="sf-menu clearfix menu-content">
        <li class="homelink"><a href="{{ url('/') }}"></a></li>
        <li><a href="#" title="Store">Store</a>
            <ul>
                
                @include('frontend/include/megamenu')
            </ul>
        </li>

        <li><a href="{{ url('place_order') }}" title="Order">Order</a></li>

        <li><a href="{{ url('about') }}" title="About">About</a></li>

        <li><a href="{{ url('contact') }}" title="Contact Us">Contact Us</a></li>
    </ul>
</div>