<?php foreach ($sliders as $slide): ?>
    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{asset('slider_image/'.$slide->slider_image)}}"  data-saveperformance="off"  data-title="Awesome Title Here">
    <img src="{{asset('slider_image/'.$slide->slider_image)}}"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

    <div class="tp-caption sfl sfb tp-resizeme"
         data-x="left" data-hoffset="15"
         data-y="center" data-voffset="-120"
         data-speed="1500"
         data-start="500"
         data-easing="easeOutExpo"
         data-splitin="none"
         data-splitout="none"
         data-elementdelay="0.01"
         data-endelementdelay="0.3"
         data-endspeed="1200"
         data-endeasing="Power4.easeIn"><h2 class="dark"><?php print_r($slide->title); ?></h2></div>

    <div class="tp-caption sfl sfb tp-resizeme"
         data-x="left" data-hoffset="15"
         data-y="center" data-voffset="-20"
         data-speed="1500"
         data-start="1000"
         data-easing="easeOutExpo"
         data-splitin="none"
         data-splitout="none"
         data-elementdelay="0.01"
         data-endelementdelay="0.3"
         data-endspeed="1200"
         data-endeasing="Power4.easeIn"></div>

   <!--  <div class="tp-caption sfr sfb tp-resizeme"
         data-x="left" data-hoffset="15"
         data-y="center" data-voffset="70"
         data-speed="1500"
         data-start="1500"
         data-easing="easeOutExpo"
         data-splitin="none"
         data-splitout="none"
         data-elementdelay="0.01"
         data-endelementdelay="0.3"
         data-endspeed="1200"
         data-endeasing="Power4.easeIn"><a href="services-single.html" class="theme-btn btn-style-two">READ MORE</a> &nbsp; &nbsp; <a href="contact.html" class="theme-btn btn-style-four">GET A QUOTE</a></div> -->

</li>
<?php endforeach ?>



