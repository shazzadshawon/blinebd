@extends('layouts.frontend_about')

@section('content')

 <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                     @include('frontend/include/sidebar_list')
                    <!-- /Block categories module -->


                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">About</span>
                    </div>
                    <!-- /Breadcrumb -->

                    <div class="rte">
                        <h1 class="page-heading bottom-indent">About us</h1>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-block">
                                    <h3 class="page-subheading">Our company</h3>
                                    <p><strong class="dark">B-line enterprise  is a Hi-Tech enterprise having its own import, supply and sales of fiber optical and communications equipments.</strong></p>
                                    <p>The main products of b-line include UTP Cables , Fiber optic patch cord, Connector, Adapter, Optical coupler, Attenuator, Optic media converter, Optic splice closure, Optic terminal box, ODF etc, which are widely used in telecommunications, broadband, broadcast, CATV and network industry.</p>
                                    <ul class="list-1">
                                        <li><em class="icon-ok"></em>Top quality products</li>
                                        <li><em class="icon-ok"></em>Best customer service</li>
                                        <li><em class="icon-ok"></em>30-days money back guarantee</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-box">
                                    <h3 class="page-subheading">Our Sells Point</h3>
                                    <img title="cms-img" src="{{ asset('public/uploads/business/'.$shop->image) }}" alt="cms-img" width="370" height="192" />

                                    <p><strong class="dark">{{ $shop->title }} </strong></p><br>
                                    <p>
                                    	@php
                                    	print_r($shop->description);
                                    	@endphp
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-box">
                                    <h3 class="page-subheading">Testimonials / Reviews</h3>

                                    @foreach($reviews as $rev)

                                    <div class="testimonials">
                                        <div class="inner">
                                        	<span class="before">&ldquo;</span>@php
		                                    	print_r($rev->description);
		                                    	@endphp <span class="after">&rdquo;</span>
                                        </div>
                                    </div>
                                    <p>
                                    	<img class="img img-thumbnail img-circle" style="width: 50px; height: 50px" src="{{ asset('public/uploads/brand/'.$rev->image) }}">
                                    	<strong class="dark">
                                    	@php
                                    	print_r($rev->title);
                                    	@endphp
                                    </strong></p>

                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->


@endsection