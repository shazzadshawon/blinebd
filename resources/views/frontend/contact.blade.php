@extends('layouts.frontend_contact')

@section('content')


    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    @include('frontend/include/weekly_special')

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    @include('frontend/include/sidebar_list')
                    <!-- /Block categories module -->


                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="{{ url('/') }}" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Contact Us</span>
                    </div>
                    <!-- /Breadcrumb -->

                    <h1 class="page-heading bottom-indent">
                        Customer service - Contact Us</h1>

                    <div id="map"></div>

<!--                    <div class="store-content-select selector3">-->
<!--                        <select id="locationSelect" class="form-control">-->
<!--                            <option>-</option>-->
<!--                        </select>-->
<!--                    </div>-->


                    <form action="{{ url('post_contact') }}" method="post" class="contact-form-box" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <fieldset>
                            <h3 class="page-subheading">Send a message</h3>
                            <div class="clearfix">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group selector1">
                                        <label for="id_contact">Subject Heading</label>
                                        <input class="form-control grey validate" type="text" id="title" name="title" data-validate="isEmail" value=""  required="" />
                                    </div>
                            

                                    <p class="form-group">
                                        <label for="email">Email ID</label>
                                        <input class="form-control grey validate" type="email" id="email" name="email" data-validate="isEmail" value=""  required="" />
                                    </p>
                                    <div class="form-group selector1">
                                        <label>Address</label>
                                        <input class="form-control grey" type="text" name="address" id="" value=""  required="" />
                                    </div>
                                  {{--   <p class="form-group">
                                        <label for="fileUpload">Attach File</label>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                                        <input type="file" name="fileUpload" id="fileUpload" class="form-control" />
                                    </p> --}}
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea rows="8" class="form-control" id="message" name="message" required="" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <p >
                            	<div class="submit">
                                <button type="submit" name="submitMessage" id="submitMessage" class="button btn btn-default button-medium"><span>Send<i class="icon-chevron-right right"></i></span></button>
                            </div>
                            </p>
                        </fieldset>
                    </form>

                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->



@endsection