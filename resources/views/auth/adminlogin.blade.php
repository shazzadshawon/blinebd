<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Login | Admin panel</title>

    <!-- Styles -->

{{--    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('public/css/backend/theme-default.css')}}"/>--}}

    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

</head>
<body style="background-color:#000000; background-image: url('public/login/login4.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover">
<div >



    <div class="container">
        <div  >
            <div class="col-md-4 pull-right">
                <div class="panel panel-primary"  style="margin-top: 50%; box-shadow: 0px 20px 20px rgba(0,0,0,0.5)" >
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">

                        <div>
                            <div class="row" >
                                <div class="col-md-10 col-md-offset-1">

                                    <!-- Horizontal form -->
                                    <form  role="form" method="POST" action="{{ url('/login') }}" class="form-horizontal pad-bg">
                                        {{--<h1 class="text-center">Horizontal form</h1>--}}
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-3">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control"  name="email" id="username" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-3">Password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control"  name="password" id="password" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>












</div>

<!-- Scripts -->
<script src="{{ asset('public/js/app.js') }}"></script>
</body>
</html>












