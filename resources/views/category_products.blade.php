@extends('layouts.frontend')

@section('content')
<!-- HEADER -->


{{-- {{ $SubCategories->sub_category_name }} --}}
{{-- '/product-details/'.$top->id --}}


  @include('frontend/ezbazzar/breadcrumb')
        <!-- ============================================================= HEADER : END ============================================================= -->   
        <section id="category-grid">
        <div class="container">

            <!-- ========================================= SIDEBAR ========================================= -->
            <div class="col-xs-12 col-sm-3 no-margin sidebar narrow sidemenu-holder">
                @include('frontend.ezbazzar.sidebar_navigation')
            </div>
            <!-- ========================================= SIDEBAR : END ========================================= -->

            <!-- ========================================= CONTENT ========================================= -->

            <div class="col-xs-12 col-sm-9 no-margin wide sidebar">

                <div id="grid-page-banner">
                    <a href="#">
                        <img src="assets/images/banners/mens.png" alt="" style="width: 899px; height: 277px;" />
                    </a>
                </div>

                @include('frontend.ezbazzar.category_content')
            </div><!-- /.col -->
            <!-- ========================================= CONTENT : END ========================================= -->
        </div><!-- /.container -->
        </section><!-- /#category-grid -->

@endsection