<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->integer('sub_sub_category_id');
            $table->string('product_name', 200);
            $table->string('product_name_bn', 200);
            $table->string('product_code', 200);
            $table->string('product_price');
            $table->string('product_quantity', 200);
            $table->string('discount');
            $table->longText('description');
            $table->longText('description_bn');
            $table->tinyInteger('offer_status');
            $table->tinyInteger('publication_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
