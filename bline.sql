-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 10:26 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bline`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `sub_cat_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `image`, `description`, `sub_cat_id`, `type_id`, `status`, `created_at`, `updated_at`) VALUES
(23, 'Offfice 1', '1506344584.jpg', '<p>jy Demo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDem&nbsp; Demo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDem</p>\r\n', NULL, 0, '1', '2017-09-28 07:16:06', '0000-00-00 00:00:00'),
(24, 'Showroom 1', '1506344613.jpg', '<p>mhk Demo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDem</p>\r\n', NULL, 1, '1', '2017-09-28 07:15:51', '0000-00-00 00:00:00'),
(25, 'Offfice 2', '1506578164.jpg', '<p>Demo Description</p>\r\n\r\n<p>Demo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo DescriptionDemo Description</p>\r\n', NULL, 0, '1', '2017-09-28 05:56:04', '0000-00-00 00:00:00'),
(26, 'Showroom 2', '1506578194.jpg', '<p>Demo</p>\r\n\r\n<p>DemoDemo Demo Demo DemoDemoDemo DemoDemoDemoDemoDemo Demo DemoDemoDemoDemoDemo DemoDemo DemoDemo</p>\r\n', NULL, 1, '1', '2017-09-28 06:06:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `sub_cat_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `title`, `image`, `description`, `sub_cat_id`, `type_id`, `status`, `created_at`, `updated_at`) VALUES
(21, 'Acvievement 2', '1506585614.jpg', '<p>Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo</p>\r\n', NULL, 0, '1', '2017-09-28 08:00:14', '0000-00-00 00:00:00'),
(22, 'Acvievement 1', '1506585591.jpg', '<p>Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo</p>\r\n', NULL, 0, '1', '2017-09-28 07:59:51', '0000-00-00 00:00:00'),
(23, 'Acvievement 3', '1506948403.jpg', '<p>o Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo D</p>\r\n', NULL, 0, '1', '2017-10-02 12:46:43', '0000-00-00 00:00:00'),
(24, 'Acvievement 4', '1506948607.jpg', '<p>&nbsp;</p>\r\n\r\n<p>demo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demodemo demo</p>\r\n', NULL, 0, '1', '2017-10-02 13:16:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `add_to_carts`
--

CREATE TABLE `add_to_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_bn` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `add_to_carts`
--

INSERT INTO `add_to_carts` (`id`, `product_id`, `product_name`, `product_name_bn`, `product_code`, `product_price`, `product_quantity`, `session_id`, `size`, `created_at`, `updated_at`) VALUES
(37, 131, 'Product Name 5', 'null', 'pr0105', '500', '1', 'KBzpycSboJewLlnIvkkeV8mFwjiPng6zgwRzkXrs', 'None', '2017-10-02 11:26:42', '2017-10-02 11:26:42');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_title` longtext COLLATE utf8mb4_unicode_ci,
  `blog_image` longtext COLLATE utf8mb4_unicode_ci,
  `blog_description` longtext COLLATE utf8mb4_unicode_ci,
  `blog_sub_cat_id` int(11) DEFAULT NULL,
  `blog_type_id` int(11) DEFAULT NULL,
  `blog_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_title`, `blog_image`, `blog_description`, `blog_sub_cat_id`, `blog_type_id`, `blog_status`, `created_at`, `updated_at`) VALUES
(17, 'dsd', '1506575824.jpg', '<p>Traditional</p>\r\n', NULL, 0, '1', '2017-09-28 05:17:04', '0000-00-00 00:00:00'),
(18, 'thtun', '1506575815.jpg', '<p>Description</p>\r\n', NULL, 0, '1', '2017-09-28 05:16:55', '0000-00-00 00:00:00'),
(19, 'title', '1506575803.jpg', '<p>description</p>\r\n', NULL, 0, '1', '2017-09-28 05:16:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `sub_cat_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `image`, `description`, `sub_cat_id`, `type_id`, `status`, `created_at`, `updated_at`) VALUES
(21, 'Bline BD', '1509268089.jpg', '<p>Make It Possible</p>\r\n', NULL, 0, '1', '2017-10-29 09:09:01', '0000-00-00 00:00:00'),
(22, 'hm', '1509269839.png', '<p>juy</p>\r\n', NULL, 0, '1', '2017-10-29 09:37:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `sub_cat_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `title`, `image`, `description`, `sub_cat_id`, `type_id`, `status`, `created_at`, `updated_at`) VALUES
(22, 'Bline BD', '1509432365.jpg', 'Radio Biponi\r\n25, Swimmingpool Market\r\nBangabandhu National Stadium road\r\nDhaka-1000 Bangladesh\r\n\r\n+88-02-9552038', NULL, 0, '1', '2017-10-31 06:46:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `mega_menu` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_name_bn`, `publication_status`, `mega_menu`, `created_at`, `updated_at`) VALUES
(56, 'Category 1', 'Category 1', 1, NULL, '2017-10-30 03:05:41', '2017-10-30 03:05:41'),
(57, 'Category 2', 'Category 2', 1, NULL, '2017-10-30 21:40:48', '2017-10-30 21:40:48'),
(58, 'Category 3', 'Category 3', 1, NULL, '2017-10-30 21:42:04', '2017-10-30 21:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_reference` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_title`, `contact_email`, `contact_phone`, `contact_reference`, `contact_description`, `created_at`, `updated_at`) VALUES
(4, 'sdf', 'sdf@ghj.asd', '987654433', 'fvrv', 'rtgryt', '2017-10-02 07:22:54', '2017-10-02 07:22:54'),
(5, 'bd', 'wdw@efef.com', '987654433', 'fvrv', 'gtrgt hy67u67u', '2017-10-02 07:23:45', '2017-10-02 07:23:45'),
(6, 'ME', 'me@rar.com', '01973668011', 'n/a', 'Blank', '2017-10-03 08:28:04', '2017-10-03 08:28:04'),
(7, 'sdf', 'admin@gmail.com', '675757', 'ynyu', 'jhnty', '2017-10-03 10:09:11', '2017-10-03 10:09:11'),
(8, 'Name', 'demo@gmail.com', '1233r4546', 'Demo Email', 'Demo Text', '2017-10-03 10:28:26', '2017-10-03 10:28:26'),
(9, 'Message 6', 'sms@demo.com', '', 'dhaka', 'kwednjwedn edfelhnihn', '2017-11-01 06:03:22', '2017-11-01 06:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_name`, `phone_number`, `address`, `email_address`, `password`, `created_at`, `updated_at`) VALUES
(17, 'User 1', '0123456789', 'Dhaka', 'user1@newtech.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-09-26 23:03:54', '2017-09-26 23:03:54'),
(18, 'User 2', '01323568474', 'dgfh', 'user2@newtech.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-09-26 23:14:45', '2017-09-26 23:14:45'),
(19, 'User 3', '013215646', 'Banani', 'user3@newtech.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-09-27 07:00:37', '2017-09-27 07:00:37'),
(20, 'User', '0132156468', 'dhaka', 'user@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-10-01 23:14:19', '2017-10-01 23:14:19'),
(21, 'rakib', '01973668031', 'dhaka', 'mail2rar@gmail.com', '3f2205ff8dd38410063482bfe29b7857', '2017-10-03 09:51:25', '2017-10-03 09:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `email` longtext COLLATE utf8mb4_unicode_ci,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `designation` longtext COLLATE utf8mb4_unicode_ci,
  `password` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `image`, `email`, `address`, `designation`, `password`, `status`, `role`, `created_at`, `updated_at`) VALUES
(15, 'Admin 1', '1504673252.jpg', 'admin@gmail.com', 'Dhaka', NULL, '$2y$10$4aD83GYfhQzUxsT2Uk50g.ra.DqIbbBMrhr9T9VOx.5W0FbxztnDi', '1', 1, '2017-09-07 08:49:30', '2017-09-06 04:27:45'),
(16, 'Employee 2', '1504673266.jpg', 'employee2 @newtechbd.com', '3', NULL, '2', '1', 2, '2017-09-07 08:49:33', '2017-09-06 04:27:45'),
(17, 'Employee 3', '1504675217.jpg', 'employee3 @newtechbd.com\n', NULL, NULL, '0', '1', 2, '2017-09-07 08:49:43', '0000-00-00 00:00:00'),
(19, 'Employee 4', '1504675557.jpg', 'employee4@newtechbd.com', 'Dhaka', 'Manager', '0', '1', 2, '2017-09-07 08:49:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8mb4_unicode_ci,
  `cover_image` longtext COLLATE utf8mb4_unicode_ci,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `type`, `video_name`, `cover_image`, `gallery_image`, `category_id`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
(53, 0, NULL, NULL, '1507028610.jpg', NULL, '1', NULL, NULL),
(54, 0, NULL, NULL, '1507028813.jpg', NULL, '1', NULL, NULL),
(56, 0, NULL, NULL, '1507028971.JPG', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotdeal`
--

CREATE TABLE `hotdeal` (
  `id` int(11) NOT NULL,
  `discount` longtext NOT NULL,
  `description` longtext,
  `hotdealimage` longtext,
  `end_date` longtext,
  `created_at` longtext,
  `updated_at` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotdeal`
--

INSERT INTO `hotdeal` (`id`, `discount`, `description`, `hotdealimage`, `end_date`, `created_at`, `updated_at`) VALUES
(1, '20', '<p>Big Eid Offer</p>\r\n', '1503925915.png', '09/02/2017', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_01_124411_create_admins_table', 1),
(4, '2017_01_01_135930_create_categories_table', 2),
(5, '2017_01_02_085340_create_sub_categories_table', 3),
(6, '2017_01_02_133054_create_slider_images_table', 4),
(7, '2017_01_03_094319_create_product_images_table', 5),
(8, '2017_01_03_100037_create_products_table', 5),
(9, '2017_01_09_072750_create_customers_table', 6),
(10, '2017_01_09_123439_create_wishlists_table', 7),
(11, '2017_01_10_133258_create_add_to_carts_table', 8),
(12, '2017_01_11_114149_create_orders_table', 9),
(13, '2017_01_11_114233_create_shipping_addresses_table', 9),
(14, '2017_05_04_081139_create_sub_sub_categories_table', 10),
(15, '2017_05_08_080323_create_pazzles_table', 11),
(16, '2017_05_15_103110_create_subscribes_table', 12),
(17, '2017_06_18_063013_create_product_sizes_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `order_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_id`, `customer_id`, `product_name`, `product_code`, `product_price`, `product_quantity`, `size`, `order_number`, `session_id`, `publication_status`, `created_at`, `order_date`, `updated_at`) VALUES
(1, 127, 20, 'Product Name', 'pr0101', '980', '1', 'None', '24059', 'Jq2QhPYox9SI8pX0Op6grZFVpXlNvdBazZBpF6oP', 1, '2017-10-02 00:02:54', '02-10-2017', '2017-10-02 00:04:58'),
(2, 131, 21, 'Product Name 5', 'pr0105', '500', '1', 'None', '705505831', 'nFO8zhRB7ngAtQkBTwerVXLmNz0xqzbstFya2bSV', 0, '2017-10-03 09:52:33', '03-10-2017', '2017-10-03 09:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pazzles`
--

CREATE TABLE `pazzles` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pazzle` int(11) NOT NULL,
  `pazzle_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `mega_offer` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pazzles`
--

INSERT INTO `pazzles` (`id`, `heading`, `pazzle`, `pazzle_image`, `publication_status`, `mega_offer`, `created_at`, `updated_at`) VALUES
(2, NULL, 34, '868.png', 1, NULL, '2017-09-04 21:14:44', '2017-09-04 21:14:44'),
(3, NULL, 35, '868.png', 1, NULL, '2017-09-04 21:15:00', '2017-09-04 21:15:00'),
(4, NULL, 37, 'testimonials.jpg', 1, NULL, '2017-09-04 21:15:24', '2017-09-05 03:51:55'),
(5, NULL, 34, '8903415526399_thumb_1.png', 1, NULL, '2017-09-04 21:15:53', '2017-09-04 21:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE `problems` (
  `id` int(11) NOT NULL,
  `employee_name` longtext COLLATE utf8_unicode_ci,
  `project_name` longtext COLLATE utf8_unicode_ci,
  `email` longtext COLLATE utf8_unicode_ci,
  `problem_type` longtext COLLATE utf8_unicode_ci,
  `problem_details` longtext COLLATE utf8_unicode_ci,
  `date` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`id`, `employee_name`, `project_name`, `email`, `problem_type`, `problem_details`, `date`, `created_at`, `updated_at`) VALUES
(8, 'Demo', 'code-1, code-2', 'demo@gmail.com', 'Banani', 'Product Details', '01-11-2017', '2017-11-01 06:16:43', '2017-11-01 06:16:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_id` int(11) DEFAULT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_bn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description_bn` longtext COLLATE utf8_unicode_ci NOT NULL,
  `offer_status` tinyint(4) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sub_category_id`, `sub_sub_category_id`, `product_name`, `product_name_bn`, `product_code`, `product_price`, `product_quantity`, `discount`, `description`, `description_bn`, `offer_status`, `publication_status`, `created_at`, `updated_at`) VALUES
(159, 56, 85, 0, 'Product 1', '', 'p1', '111111', '111', '11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', 1, 1, '2017-10-30 06:46:24', '2017-10-31 06:25:28'),
(160, 56, 86, 0, 'Product 2', '', 'p2', '1212', '121', '11', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 2, 1, '2017-10-30 06:47:05', '2017-10-31 06:24:18'),
(161, 56, 86, 0, 'Product 3', 'null', 'p3', '111111', '111', '12', '<p>dwedfe frwfw fwfwf</p>\r\n', 'null', 1, 1, '2017-10-30 06:47:47', '2017-10-30 06:47:47'),
(162, 56, 86, 0, 'Product 4', '', 'p4', '111111', '111', '', '<p>hy6h5yu6u jtyhtyht</p>\r\n', '', 1, 1, '2017-10-30 06:48:19', '2017-10-31 02:41:15'),
(163, 56, 85, 0, 'yhy', 'null', 'htrh', '76757', '5757', '57', '<p>675nhnhj jukyukiu</p>\r\n', 'null', 1, 1, '2017-10-30 07:27:23', '2017-10-30 07:27:23'),
(164, 56, 86, 0, 'tyujyt', 'null', 'yjtyj', '757', '7757', '7', '<p>7676576m77u67</p>\r\n', 'null', 1, 1, '2017-10-30 07:27:42', '2017-10-30 07:27:42'),
(165, 56, 86, 0, 'aaa', 'null', 'ytrty', '564646', '64', '0', '<p>ththt yjyujyu</p>\r\n', 'null', 1, 1, '2017-10-30 07:28:05', '2017-10-30 07:28:05'),
(166, 57, 87, 0, 'Product 5', 'null', 'p5', '2143424', '123', '5', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n', 'null', 2, 1, '2017-10-30 21:45:40', '2017-10-30 21:45:40'),
(167, 58, 88, 0, 'Product 6', 'null', 'p6', '59999', '50', '70', '<p>&nbsp;Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n', 'null', 2, 1, '2017-10-30 21:47:05', '2017-10-30 21:47:05'),
(168, 58, 89, 0, 'Product 7', 'null', 'p7', '300', '10000', '70', '<p>&nbsp;</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n', 'null', 3, 1, '2017-10-30 21:48:08', '2017-10-30 21:48:08'),
(169, 58, 90, 0, 'Product 8', 'null', 'p8', '100', '100', '10', '<p>Lorem Ipsum</p>\r\n', 'null', 3, 1, '2017-10-30 23:48:11', '2017-10-30 23:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_id`, `product_image`, `created_at`, `updated_at`) VALUES
(238, 159, '2086034156.jpg', '2017-10-30 06:46:24', '2017-10-30 06:46:24'),
(239, 159, '1912733928.jpg', '2017-10-30 06:46:24', '2017-10-30 06:46:24'),
(240, 159, '1247771317.jpg', '2017-10-30 06:46:24', '2017-10-30 06:46:24'),
(241, 160, '633833608.jpg', '2017-10-30 06:47:05', '2017-10-30 06:47:05'),
(242, 160, '97954692.jpg', '2017-10-30 06:47:05', '2017-10-30 06:47:05'),
(243, 161, '635073710.jpg', '2017-10-30 06:47:47', '2017-10-30 06:47:47'),
(244, 161, '2138497134.jpg', '2017-10-30 06:47:47', '2017-10-30 06:47:47'),
(245, 162, '143790563.jpg', '2017-10-30 06:48:19', '2017-10-30 06:48:19'),
(246, 163, '870432505.jpg', '2017-10-30 07:27:23', '2017-10-30 07:27:23'),
(247, 164, '624903850.jpg', '2017-10-30 07:27:42', '2017-10-30 07:27:42'),
(248, 165, '951977825.jpg', '2017-10-30 07:28:05', '2017-10-30 07:28:05'),
(249, 165, '261906987.jpg', '2017-10-30 07:28:05', '2017-10-30 07:28:05'),
(250, 166, '278576810.jpg', '2017-10-30 21:45:41', '2017-10-30 21:45:41'),
(251, 167, '310465458.jpg', '2017-10-30 21:47:05', '2017-10-30 21:47:05'),
(252, 167, '2102118477.png', '2017-10-30 21:47:05', '2017-10-30 21:47:05'),
(253, 168, '1800306386.png', '2017-10-30 21:48:08', '2017-10-30 21:48:08'),
(254, 168, '493545481.png', '2017-10-30 21:48:08', '2017-10-30 21:48:08'),
(255, 168, '1315237279.jpg', '2017-10-30 21:48:08', '2017-10-30 21:48:08'),
(256, 169, '1201408996.png', '2017-10-30 23:48:11', '2017-10-30 23:48:11'),
(257, 169, '417499793.png', '2017-10-30 23:48:11', '2017-10-30 23:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `product_id`, `size`, `created_at`, `updated_at`) VALUES
(1, 79, 'XL', '2017-06-18 00:34:28', '2017-06-18 00:34:28'),
(3, 72, 'L', '2017-06-18 00:48:54', '2017-06-18 00:48:54'),
(4, 72, 'M', '2017-06-18 00:48:59', '2017-06-18 00:48:59'),
(5, 72, 'S', '2017-06-18 00:49:05', '2017-06-18 00:49:05'),
(6, 70, 'L', '2017-07-27 02:39:30', '2017-07-27 02:39:30'),
(7, 70, 'XL', '2017-07-27 02:39:44', '2017-07-27 02:39:44'),
(8, 71, 'S', '2017-07-30 12:24:59', '2017-07-30 12:24:59'),
(9, 71, 'L', '2017-07-30 12:25:07', '2017-07-30 12:25:07'),
(10, 70, 'XXL', '2017-07-31 03:12:25', '2017-07-31 03:12:25'),
(11, 73, 'L', '2017-08-23 04:59:45', '2017-08-23 04:59:45'),
(12, 73, 'XL', '2017-08-23 04:59:53', '2017-08-23 04:59:53'),
(13, 71, 'M', '2017-08-27 07:38:02', '2017-08-27 07:38:02'),
(14, 78, 'S', '2017-08-31 02:58:10', '2017-08-31 02:58:10'),
(15, 78, 'M', '2017-08-31 02:58:15', '2017-08-31 02:58:15'),
(16, 78, 'L', '2017-08-31 02:58:22', '2017-08-31 02:58:22'),
(17, 78, 'Xl', '2017-08-31 02:58:28', '2017-08-31 02:58:28'),
(18, 78, 'XXl', '2017-08-31 02:58:40', '2017-08-31 02:58:40'),
(19, 135, 'Xl', '2017-10-29 01:22:59', '2017-10-29 01:22:59'),
(20, 148, 'Xl', '2017-10-29 02:38:51', '2017-10-29 02:38:51'),
(21, 155, 'XL', '2017-10-29 03:07:44', '2017-10-29 03:07:44'),
(22, 159, 'XXL', '2017-10-31 22:39:49', '2017-10-31 22:39:49');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `review_title`, `review_description`, `review_image`, `review_status`, `created_at`, `updated_at`) VALUES
(8, 'Review 4', '<p>This is a demo review 4</p>', '1500713667.jpg', '1', '22 July, 2017', NULL),
(9, 'Review 2', '<p>Demo&nbsp;review 2</p>', '1500713867.jpg', '1', '22 July, 2017', NULL),
(10, 'Review 1', '<p>trbtyhnyun 65u6n5u5u</p>', '1500714088.jpg', '1', '22 July, 2017', NULL),
(11, 'Review 3', '<p>tbt 6yh67 65yuh65yun 656un6un 77jmuyjmuy hjujymnu drfewr sdcse ki,lk,il&nbsp;</p>', '1500714250.jpg', '1', '22 July, 2017', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_title`, `service_image`, `service_description`, `service_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(16, 'Service name 1', '1506245251.jpg', '<p>service description 1</p>\r\n', 6, NULL, '1', NULL, NULL),
(17, 'Service name 2', '1506245419.jpg', '<p>nmk</p>\r\n', 5, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_cat`
--

CREATE TABLE `service_cat` (
  `id` int(11) NOT NULL,
  `cat_name` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_cat`
--

INSERT INTO `service_cat` (`id`, `cat_name`, `created_at`, `updated_at`) VALUES
(5, 'service cat 1.1', '2017-09-24 10:10:37', '0000-00-00 00:00:00'),
(6, 'service cat 2.2', '2017-09-24 10:12:40', '0000-00-00 00:00:00'),
(7, 'service cat 3', '2017-09-24 09:00:02', '0000-00-00 00:00:00'),
(8, 'service cat 4', '2017-09-24 09:00:02', '0000-00-00 00:00:00'),
(9, 'Service category 5', '2017-09-24 09:55:41', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `order_number`, `name`, `email`, `phone`, `address`, `zipcode`, `notes`, `location`, `created_at`, `updated_at`) VALUES
(28, '24059', 'Akash Akram', 'akash@gmail.com', '013654789', 'Dhaka', '1100', '', NULL, '2017-10-02 00:02:54', '2017-10-02 00:02:54'),
(29, '705505831', 'Rakib Ali', 'mail', '01973668031', 'dhaka', '1206', 'Please check it', NULL, '2017-10-03 09:52:33', '2017-10-03 09:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `slider_image_id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `subtitle` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`slider_image_id`, `slider_image`, `publication_status`, `created_at`, `updated_at`, `title`, `subtitle`) VALUES
(40, 'Gepon_OLT copy.jpg', 1, '2017-10-30 06:21:43', '2017-10-30 06:21:43', '<p>Demo</p>\r\n', NULL),
(41, 'Media Converter.jpg', 1, '2017-10-30 06:21:51', '2017-10-30 06:21:51', '', NULL),
(42, 'ONU.jpg', 1, '2017-10-30 06:22:03', '2017-10-30 06:22:03', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(2, 'mahmud.agvbd@gmail.com', '2017-05-15 05:24:41', '2017-05-15 05:24:41'),
(3, 'cyndiocfa@comcast.net', '2017-07-14 13:03:43', '2017-07-14 13:03:43'),
(4, 'rjostant@gmail.com', '2017-07-14 15:40:47', '2017-07-14 15:40:47'),
(5, 'msteed@txkusa.org', '2017-07-14 18:59:17', '2017-07-14 18:59:17'),
(6, 'pthompson@thompsone.com', '2017-07-14 23:18:04', '2017-07-14 23:18:04'),
(7, 'illmind828@gmail.com', '2017-07-14 23:34:41', '2017-07-14 23:34:41'),
(8, 'iamking.tg@gmail.com', '2017-07-15 00:57:12', '2017-07-15 00:57:12'),
(9, 'bkwilliamspmp@gmail.com', '2017-07-16 08:11:36', '2017-07-16 08:11:36'),
(12, 'vishal.batra75@gmail.com', '2017-07-16 18:27:11', '2017-07-16 18:27:11'),
(13, 'shazzadurrahaman@gmail.com', '2017-07-17 05:25:50', '2017-07-17 05:25:50'),
(14, '6144965638@txt.att.net', '2017-07-18 08:45:57', '2017-07-18 08:45:57'),
(15, 'k.kastendieck@t-online.de', '2017-07-18 11:14:58', '2017-07-18 11:14:58'),
(16, 'mercier.br@wanadoo.fr', '2017-07-18 13:39:50', '2017-07-18 13:39:50'),
(17, 'jesslom2000@yahoo.com', '2017-07-18 15:17:09', '2017-07-18 15:17:09'),
(18, 'david.korn.cpa@gmail.com', '2017-07-18 15:42:39', '2017-07-18 15:42:39'),
(19, 'chrishale@gmail.com', '2017-07-18 22:13:56', '2017-07-18 22:13:56'),
(20, 'koolgirl1997@yahoo.com', '2017-07-19 01:39:47', '2017-07-19 01:39:47'),
(21, 'a.page@labtech.com', '2017-07-19 02:17:54', '2017-07-19 02:17:54'),
(22, 'johnsonsk@siouxvalley.net', '2017-07-19 02:28:48', '2017-07-19 02:28:48'),
(23, 'robertpp2@comcast.net', '2017-07-20 11:02:10', '2017-07-20 11:02:10'),
(24, 'jodavi2001@yahoo.com', '2017-07-20 19:37:42', '2017-07-20 19:37:42'),
(25, 'pdreger@gmail.com', '2017-07-21 01:22:33', '2017-07-21 01:22:33'),
(26, 'crazy4bikes@gmail.com', '2017-07-21 10:39:54', '2017-07-21 10:39:54'),
(27, 'connanbar@gmail.com', '2017-07-21 17:00:23', '2017-07-21 17:00:23'),
(28, 'avimscher@gmail.com', '2017-07-21 21:50:22', '2017-07-21 21:50:22'),
(29, 'admin@gmail.com', '2017-09-04 23:09:50', '2017-09-04 23:09:50'),
(30, 'mahmud@gmail.com', '2017-09-05 01:38:14', '2017-09-05 01:38:14');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `category_id`, `sub_category_name`, `sub_category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(85, 56, 'Sub category 1.3', 'Sub category 1.3', 1, '2017-10-30 03:13:44', '2017-10-30 23:36:01'),
(86, 56, 'Sub category 1.1', 'Sub category 1.1', 1, '2017-10-30 03:58:00', '2017-10-30 03:58:00'),
(87, 57, 'Sub category 2.1', 'Sub category 2.1', 1, '2017-10-30 21:42:24', '2017-10-30 21:42:24'),
(88, 57, 'Sub category 2.2', 'Sub category 2.2', 1, '2017-10-30 21:42:33', '2017-10-30 21:42:33'),
(89, 58, 'Sub category 3.1', 'Sub category 3.1', 1, '2017-10-30 21:42:48', '2017-10-30 21:42:48'),
(90, 58, 'Sub category 3.2', 'Sub category 3.2', 1, '2017-10-30 21:43:18', '2017-10-30 21:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_sub_category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `sub_sub_category_name`, `sub_sub_category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(9, 34, 'T-Shirts', 'টি -শার্ট', 1, '2017-06-07 23:33:38', '2017-06-07 23:33:38'),
(10, 34, 'Polo’s', 'পোলো’স', 1, '2017-06-07 23:38:40', '2017-06-07 23:38:40'),
(11, 34, 'Shirt', 'শার্ট ', 1, '2017-06-07 23:49:07', '2017-06-07 23:49:07'),
(12, 34, 'Coats & Jackets', 'কোটস ও জ্যাকেট', 1, '2017-06-07 23:52:11', '2017-06-07 23:52:11'),
(13, 34, 'Pants', 'প্যান্টস', 1, '2017-06-07 23:59:25', '2017-06-07 23:59:25'),
(14, 34, 'Jeans', 'জিন্স', 1, '2017-06-08 00:00:39', '2017-06-08 00:00:39'),
(15, 34, 'Shorts & Barmudas', 'শর্টস & বারমুডা', 1, '2017-06-08 00:02:04', '2017-06-08 00:04:37'),
(16, 35, 'Panjabis & Sherwanis', 'পাঞ্জাবি ও শেরওয়ানি', 1, '2017-06-13 00:01:40', '2017-06-13 00:01:40'),
(17, 42, 'Cleaning appliance ', 'ক্লিনিং এপ্লায়েন্স', 1, '2017-07-14 15:40:26', '2017-07-14 15:40:26'),
(18, 42, 'Cleaning appliance ', 'ক্লিনিং এপ্লায়েন্স', 1, '2017-07-20 06:09:53', '2017-07-20 06:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_title`, `team_description`, `team_contact`, `team_image`, `team_status`, `created_at`, `updated_at`) VALUES
(4, 'Anika Jaman', 'Creative Designer', NULL, '1501665095.jpg', '1', NULL, NULL),
(13, 'Anam Redwan', 'Photographer', NULL, '1501665058.jpg', '1', NULL, NULL),
(14, 'Rakibul Islam Himel', 'Photographer', NULL, '1501665044.jpg', '1', NULL, NULL),
(15, 'Rezaul Karim Bhuiyan', 'Managing Director', NULL, '1501665117.jpg', '1', NULL, NULL),
(16, 'Nabila Jahan', 'CEO', NULL, '1502011276.jpg', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `email` longtext COLLATE utf8mb4_unicode_ci,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `designation` longtext COLLATE utf8mb4_unicode_ci,
  `password` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `email`, `address`, `designation`, `password`, `status`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(15, 'Admin 1', '1506939300.jpg', 'admin@gmail.com', '                                                                                                                                          Dhaka                                                                                                                                    ', '', '$2y$10$zjeL52Ywu8.l6C1S5WSPD.MCbZKIuO6SWrzVK9LXjIMOntPeJJW0e', '1', 1, 'Jafs8D1Kdz2YXSShFUXFR9odemORoQGGsWJ0S8zGcDDoDlHCDw8CH2kDeNUu', '2017-10-29 04:02:58', '2017-10-28 22:02:58'),
(29, 'Employee', '1506924691.png', 'employee@gmail.com', ' dhaka                                            ', 'Trainee', 'e10adc3949ba59abbe56e057f20f883e', '1', NULL, NULL, '2017-10-02 11:44:31', '0000-00-00 00:00:00'),
(30, 'New tech', '1506938795.png', 'admin@newtech.com.bd', 'House# 457, Road# 31, DOHS Mohakhali , Dhaka-1206', 'Admin', '$2y$10$XXLMza/YxUgKtw/Cp/j9a.1smUipF7GB/Wzi95fu/rdJdDW44oMYi', '1', 1, '8ICT9UeaGZew4bCPurrEffNqjidcLktdP4vmFcRLB2wzUSKn2C1TBC07Ctx3', '2017-10-03 11:28:32', '2017-10-03 10:28:32'),
(31, 'MD. RAKIB ALI', '1507028326.jpg', 'mail2rar@gmail.com', 'DHAKA', 'SR. DESIGNER', '3f2205ff8dd38410063482bfe29b7857', '1', 2, NULL, '2017-10-03 10:58:46', '0000-00-00 00:00:00'),
(32, 'Bline-BD', '1509249759.png', 'admin@bline-bd.com', 'Dhaka', 'Super Admin', '$2y$10$t499ywDTSkMZTaiR2zbF8.v9De5f70pLU0kXRi8bXVl/gWqpvG/36', '1', 1, 'BReoDXtl7lfhujcIcxlPnGWEs40i9B2EigTybrALqQS7qZ8xB0weqfEB29w3', '2017-10-30 04:16:40', '2017-10-29 22:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `users-2`
--

CREATE TABLE `users-2` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users-2`
--

INSERT INTO `users-2` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Hira', 'info@kenakatazone.com', '$2y$10$3nzi3/N/s/1KcWR6eZAvb.tuWpu6mFBjTH90YY3DVYxdGU8YZsl3W', '2x0OrqYe9HcAwCN1nxgjsfRZ5OcRZtd8JgEhYEOsgyZrw7bSSZ8mrCOD8dgK', NULL, '2017-07-13 04:35:47'),
(2, 'Admin', 'admin@gmail.com', '$2y$10$4aD83GYfhQzUxsT2Uk50g.ra.DqIbbBMrhr9T9VOx.5W0FbxztnDi', 'yMvTOVf7T2I2cnstnKdhRr2NHV4CUeCs1xmtcSCelFzZgPuxVS1gJS5cuSDv', NULL, '2017-09-05 01:30:50'),
(3, 'Lakes Point Admin', 'admin@lakespoint.net', '$2y$10$95uQw0BuGutOB92zrZARTOshB0n3HfUbYBor8JTBiFvk.eR95hCv2', 'Ftpq88Kw1hHj5LcH84OYTvKJUZRtRu5Pw6nL7kEkjgXQLdsVYY1s4xxZPUrm', NULL, '2017-08-07 20:10:35'),
(4, 'Ezbazzar Admin', 'admin@ezbazzarbd.com', '$2y$10$Vquq6lr4YAwGCrbQWH5UkeudUZUv5vNfh1G3Dgh4cn1leAY6sjaXG', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users-rename`
--

CREATE TABLE `users-rename` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users-rename`
--

INSERT INTO `users-rename` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Hira', 'info@kenakatazone.com', '$2y$10$3nzi3/N/s/1KcWR6eZAvb.tuWpu6mFBjTH90YY3DVYxdGU8YZsl3W', '2x0OrqYe9HcAwCN1nxgjsfRZ5OcRZtd8JgEhYEOsgyZrw7bSSZ8mrCOD8dgK', NULL, '2017-07-13 04:35:47'),
(2, 'Admin', 'admin@gmail.com', '$2y$10$4aD83GYfhQzUxsT2Uk50g.ra.DqIbbBMrhr9T9VOx.5W0FbxztnDi', 'yMvTOVf7T2I2cnstnKdhRr2NHV4CUeCs1xmtcSCelFzZgPuxVS1gJS5cuSDv', NULL, '2017-09-05 01:30:50'),
(3, 'Lakes Point Admin', 'admin@lakespoint.net', '$2y$10$95uQw0BuGutOB92zrZARTOshB0n3HfUbYBor8JTBiFvk.eR95hCv2', 'Ftpq88Kw1hHj5LcH84OYTvKJUZRtRu5Pw6nL7kEkjgXQLdsVYY1s4xxZPUrm', NULL, '2017-08-07 20:10:35'),
(4, 'Ezbazzar Admin', 'admin@ezbazzarbd.com', '$2y$10$Vquq6lr4YAwGCrbQWH5UkeudUZUv5vNfh1G3Dgh4cn1leAY6sjaXG', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_name`, `status`, `created_at`, `updated_at`) VALUES
(1, '78097.mp4', 1, '2017-08-03 10:08:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT '0',
  `daily_count` int(11) DEFAULT '0',
  `dhaka` int(11) DEFAULT '0',
  `chittagong` int(11) DEFAULT '0',
  `barisal` int(11) DEFAULT '0',
  `khulna` int(11) DEFAULT '0',
  `mymensingh` int(11) DEFAULT '0',
  `rajshahi` int(11) DEFAULT '0',
  `rangpur` int(11) DEFAULT '0',
  `sylhet` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `counter`, `daily_count`, `dhaka`, `chittagong`, `barisal`, `khulna`, `mymensingh`, `rajshahi`, `rangpur`, `sylhet`, `created_at`, `updated_at`) VALUES
(1, 6, 5, 3, 0, 0, 0, 0, 0, 0, 0, NULL, '17-10-03');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotdeal`
--
ALTER TABLE `hotdeal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pazzles`
--
ALTER TABLE `pazzles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_cat`
--
ALTER TABLE `service_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`slider_image_id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users-2`
--
ALTER TABLE `users-2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users-rename`
--
ALTER TABLE `users-rename`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `achievements`
--
ALTER TABLE `achievements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `hotdeal`
--
ALTER TABLE `hotdeal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pazzles`
--
ALTER TABLE `pazzles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `service_cat`
--
ALTER TABLE `service_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `slider_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users-2`
--
ALTER TABLE `users-2`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users-rename`
--
ALTER TABLE `users-rename`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
