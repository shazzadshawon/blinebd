<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

<head>
    <title>Return Policy|Bline-BD</title>
    <?php require('head.php'); ?>
</head>
<body id="cms" class="cms cms-3 cms-terms-and-conditions-of-use hide-right-column lang_en">
<div id="page">
    <div class="header-container">
        <?php require('header.php'); ?>
    </div>
    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    <?php require('weekly_special.php'); ?>

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    <?php require('sidebar_list.php'); ?>
                    <!-- /Block categories module -->


                    <div id="tmleft-banner" class="block">
                        <ul>
                            <li class="tmleftbanner-container">
                                <a href="#" title="LeftBanner 1">
                                    <img src="modules/tmleftbanner/img/left-banner-1.jpg"
                                         alt="LeftBanner 1"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="index.php" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Delivery & Return Policy</span>
                    </div>
                    <!-- /Breadcrumb -->
                    <div class="rte">
                        <h1 class="page-heading">Delivery & Return Policy</h1>
                        <p class="bottom-indent">B-line enterprise  is a Hi-Tech enterprise having its own import, supply and sales of fiber optical and communications equipments.</p>
                        <p class="bottom-indent">The main products of b-line include UTP Cables , Fiber optic patch cord, Connector, Adapter, Optical coupler, Attenuator, Optic media converter, Optic splice closure, Optic terminal box, ODF etc, which are widely used in telecommunications, broadband, broadcast, CATV and network industry.</p>
                    </div>
                    <br />
                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

    <!-- Footer -->
    <?php require('footer.php'); ?>
    <!-- #footer -->

</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>

<?php require('foot.php'); ?>

</body>

</html>