<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

<head>
    <title>Contact Us|Bline BD</title>
    <?php require('head.php'); ?>
</head>
<body id="stores" class="stores hide-right-column lang_en">
<div id="page">
    <div class="header-container">
        <?php require('header.php'); ?>
    </div>
    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    <?php require('weekly_special.php'); ?>

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    <?php require('sidebar_list.php'); ?>
                    <!-- /Block categories module -->


                    <div id="tmleft-banner" class="block">
                        <ul>
                            <li class="tmleftbanner-container">
                                <a href="#" title="LeftBanner 1">
                                    <img src="modules/tmleftbanner/img/left-banner-1.jpg"
                                         alt="LeftBanner 1"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="index.php" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Contact Us</span>
                    </div>
                    <!-- /Breadcrumb -->

                    <h1 class="page-heading bottom-indent">
                        Customer service - Contact Us</h1>

                    <div id="map"></div>

<!--                    <div class="store-content-select selector3">-->
<!--                        <select id="locationSelect" class="form-control">-->
<!--                            <option>-</option>-->
<!--                        </select>-->
<!--                    </div>-->


                    <form action="contact.php" method="post" class="contact-form-box" enctype="multipart/form-data">
                        <fieldset>
                            <h3 class="page-subheading">send a message</h3>
                            <div class="clearfix">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group selector1">
                                        <label for="id_contact">Subject Heading</label>
                                        <select id="id_contact" class="form-control" name="id_contact">
                                            <option value="0">-- Choose --</option>
                                            <option value="2" >Customer service</option>
                                            <option value="1" >Webmaster</option>
                                        </select>
                                    </div>
                                    <p id="desc_contact0" class="desc_contact">&nbsp;</p>
                                    <p id="desc_contact2" class="desc_contact contact-title" style="display:none;">
                                        <i class="icon-comment-alt"></i>For any question about a product, an order
                                    </p>
                                    <p id="desc_contact1" class="desc_contact contact-title" style="display:none;">
                                        <i class="icon-comment-alt"></i>If a technical problem occurs on this website
                                    </p>
                                    <p class="form-group">
                                        <label for="email">Email address</label>
                                        <input class="form-control grey validate" type="text" id="email" name="from" data-validate="isEmail" value="" />
                                    </p>
                                    <div class="form-group selector1">
                                        <label>Order reference</label>
                                        <input class="form-control grey" type="text" name="id_order" id="id_order" value="" />
                                    </div>
                                    <p class="form-group">
                                        <label for="fileUpload">Attach File</label>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                                        <input type="file" name="fileUpload" id="fileUpload" class="form-control" />
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea class="form-control" id="message" name="message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="submit">
                                <button type="submit" name="submitMessage" id="submitMessage" class="button btn btn-default button-medium"><span>Send<i class="icon-chevron-right right"></i></span></button>
                            </div>
                        </fieldset>
                    </form>

                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

    <!-- Footer -->
    <?php require('footer.php'); ?>
    <!-- #footer -->

</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>


<script>
    var defaultLat = 23.7287984;
    var defaultLong = 90.4145994;
    var locationSelect = '';
    var loggin_required = 'You must be logged in to manage your wishlist.';
    var logo_store = 'logo_stores.html';
    var map = '';
    var markers = ['img/pin.png'];
    var search_url = 'search.php';
    var searchUrl = 'contact.php';
    var translation_1 = 'No stores were found. Please try selecting a wider radius.';
    var translation_2 = 'store found -- see details:';
    var translation_3 = 'stores found -- view all results:';
    var translation_4 = 'Phone:';
    var translation_5 = 'Get directions';
    var translation_6 = 'Not found';
</script>
<?php require('foot.php') ?>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDElOclVeevpAwrC01r05nJSI6Bri8dSXA"></script>
<script type="text/javascript" src="themes/PRS060150/js/stores.js"></script>

</body>

</html>