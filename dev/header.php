<header id="header">
    <div class="banner">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>

    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div id="header_logo">
                    <a href="index.php" title="Bline BD">
                        <img class="logo img-responsive" src="img/logo.png" alt="Bline BD"
                             width="300" height="37"/>
                    </a>
                </div>

                <!-- Block search module TOP -->
                <div id="search_block_top" class="col-sm-4 clearfix">
                    <form id="searchbox" method="get"
                          action="index.php">
                        <input type="hidden" name="controller" value="search"/>
                        <input type="hidden" name="orderby" value="position"/>
                        <input type="hidden" name="orderway" value="desc"/>
                        <input class="search_query form-control" type="text" id="search_query_top"
                               name="search_query" placeholder="Search" value=""/>
                        <button type="submit" name="submit_search" class="btn btn-default button-search">
                            <span>Search</span>
                        </button>
                    </form>
                </div>
                <!-- /Block search module TOP -->

                <ul>
                    <li class="tmtopbanner-container">
                        <a href="#" title="TopBanner 1">
<!--                            <img src="modules/tmtopbanner/img/top-banner-1.jpg" alt="TopBanner 1"/>-->
                        </a>
                    </li>
                </ul>
                <!-- Menu -->
                <?php require('header_menu.php'); ?>
                <!--/ Menu -->
            </div>
        </div>
    </div>

</header>