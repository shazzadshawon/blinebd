<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

<head>
    <title>Products|Bline-BD</title>

    <?php require('head.php'); ?>

</head>
<body id="category" class="category category-21 category-men hide-right-column lang_en">
<div id="page">
    <div class="header-container">

        <?php require('header.php'); ?>

    </div>
    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    <?php require('weekly_special.php'); ?>

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->

                    <!--   SideBar List-->
                    <?php require('sidebar_subcate_list.php'); ?>
                    <!-- /Block categories module -->

                </div>


                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="index.php" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe">&gt;</span>
                        <a href="category_page.php" title="Store">Store</a><span
                            class="navigation-pipe">&gt;</span>Sub Category Name
                    </div>
                    <!-- /Breadcrumb -->

                    <h1 class="page-heading product-listing"><span class="cat-name">Sub Category Name&nbsp;</span><span class="heading-counter">There are 20 products in this Sub Category.</span></h1>

                    <div class="content_sortPagiBar clearfix">
                        <div class="sortPagiBar clearfix">
                            <ul class="display hidden-xs">
                                <li class="display-title">View:</li>
                                <li id="grid"><a rel="nofollow" href="#" title="Grid"><i class="icon-th-large"></i>Grid</a></li>
                                <li id="list"><a rel="nofollow" href="#" title="List"><i class="icon-th-list"></i>List</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Products list -->
                    <ul class="product_list grid row">
                        <?php require('product_list_items.php'); ?>
                    </ul>



                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->


    <!-- Footer -->
    <?php require('footer.php'); ?>
    <!-- #footer -->

</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>

<script>var page_name = 'Sub Category';</script>
<?php require('foot.php'); ?>

</body>

</html>