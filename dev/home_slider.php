<div class="flexslider">
    <ul class="slides">
        <li class="tmhomeslider-container" id="slide_1">
            <a href="#" title="mainbanner-1">
                <img src="modules/tmhomeslider/img/1.jpg" alt="mainbanner-1" style="height: 450px" />
            </a>
        </li>
        <li class="tmhomeslider-container" id="slide_2">
            <a href="#" title="mainbanner-2">
                <img src="modules/tmhomeslider/img/2.jpg" alt="mainbanner-2" style="height: 450px" />
            </a>
        </li>
        <li class="tmhomeslider-container" id="slide_3">
            <a href="#" title="mainbanner-3">
                <img src="modules/tmhomeslider/img/3.jpg" alt="mainbanner-3" style="height: 450px" />
            </a>
        </li>
        <li class="tmhomeslider-container" id="slide_4">
            <a href="#" title="mainbanner-4">
                <img src="modules/tmhomeslider/img/4.jpg" alt="mainbanner-3" style="height: 450px" />
            </a>
        </li>
        <li class="tmhomeslider-container" id="slide_5">
            <a href="#" title="mainbanner-5">
                <img src="modules/tmhomeslider/img/5.jpg" alt="mainbanner-3" style="height: 450px" />
            </a>
        </li>
    </ul>
</div>

<!-- Sales Point  -->
<div id="tm_slider_right_cms">
    <div class="col-xs-12">
        <div class="cms_product block">
            <img src="img/salesPoint.jpg" style="width: 100%; height: 100%;">
            <ul>
                <li>
                    <div class="cms_block" style="background: #f2d579; padding-left: 5%; overflow: hidden;">
                        <div class="name" style="background: #bda350;"><a href="#">Bline BD Enterprise</a></div>
                        <div class="desc">Radio Biponi</div>
                        <div class="desc">25, Swimmingpool Market</div>
                        <div class="desc">Bangabandhu National Stadium road</div>
                        <div class="desc">Dhaka-1000 Bangladesh</div>
                        <div class="desc"></div>
                        <a class="all" href="#"><i class="fa fa-phone" aria-hidden="true"></i>  +88-02-9552038</a></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /Sales Point  -->