<!-- MODULE Block cmsinfo -->
<div id="cmsinfo_block">
    <div class="col-xs-6">
        <div class="footer_cms_news">
            <h4>News Update</h4>
            <div id="news" class="cms_news">
                <ul>
                    <li>
                        <div class="cms_date"><span class="date">10</span> <span class="month">FEB</span>
                        </div>
                        <a class="news_content">Lorem Ipsum is not simply random Text.It has
                            root in a pieces of classical</a></li>
                    <li>
                        <div class="cms_date"><span class="date">12</span> <span class="month">DEC</span>
                        </div>
                        <a class="news_content"> innovations and marketing sleights of hand come
                            fast from electronics makers.</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="contact-us">
            <h4>Contact Us</h4>
            <div class="contact-detail">
                <ul>
                    <li class="address">Megnor Computer Pvt Ltd, 507-UTC, Beside Apple
                        Hospital,Udhana Darwaja, Ring Road,Surat, India.
                    </li>
                    <li class="contact"><strong>CONTACT PHONE :</strong> +91 123 456 789, 123
                        456 789
                    </li>
                    <li class="email"><strong>E-MAIL ADDRESSES </strong> : <a
                            href="#">info@templatemela.com</a>
                    </li>
                    <li class="skype"><strong>SKYPE :</strong> lorem.isum, sit amet</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- /MODULE Block cmsinfo -->
<!-- Block Newsletter module-->
<div class="newsletter-block">
    <div id="newsletter_block_left" class="block">
        <h4>Newsletter</h4>
        <div class="block_content">
            <form action="http://prestashop.templatemela.com/PRS06/PRS060150/index.php"
                  method="post">
                <div class="form-group">
                    <input class="inputNew form-control grey newsletter-input"
                           id="newsletter-input" type="text" name="email" size="18"
                           value="Enter your e-mail"/>
                    <button type="submit" name="submitNewsletter"
                            class="btn btn-default button button-small">
                        <span>Ok</span>
                    </button>
                    <input type="hidden" name="action" value="0"/>
                </div>
            </form>
        </div>
    </div>

</div>
<!-- /Block Newsletter module-->

<section id="social_block">
    <ul>
        <li class="facebook">
            <a target="_blank" href="http://www.facebook.com/prestashop">
                <span>Facebook</span>
            </a>
        </li>
        <li class="twitter">
            <a target="_blank" href="http://www.twitter.com/prestashop">
                <span>Twitter</span>
            </a>
        </li>
        <li class="rss">
            <a target="_blank" href="http://www.prestashop.com/blog/en/">
                <span>RSS</span>
            </a>
        </li>
        <li class="google-plus">
            <a href="https://www.google.com/+prestashop">
                <span>Google Plus</span>
            </a>
        </li>
        <li class="pinterest">
            <a href="https://www.pinterest.com/+prestashop">
                <span>Pinterest</span>
            </a>
        </li>
    </ul>
    <h4>Follow us</h4>
</section>
<div class="clearfix"></div>