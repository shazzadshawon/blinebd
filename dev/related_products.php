<!--1 product   ==> classes ==> item, first-in-line, first-item-of-tablet-line, first-item-of-mobile-line-->
<!--2 Products  ==> Classes ==> item, last-item-of-mobile-line-->
<!--3 Products  ==> Classes ==> item, first-item-of-mobile-line-->
<!--4 Products  ==> Classes ==> item, last-item-of-tablet-line, last-item-of-mobile-line-->
<!--5 Products  ==> Classes ==> item,  last-in-line, first-item-of-tablet-line, first-item-of-mobile-line-->


<!--slide ONE-->
<li class="ajax_block_product slider-item item  first-in-line first-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/Cat5e.jpg"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  last-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/CAB-UTP5CCA-305M.jpg"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  last-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/1.8%20HDMI.png"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  first-item-of-tablet-line last-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/1.8%20HJDMI%20BOX.png"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  last-in-line first-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Media%20Converter/Netlink%20Media%20Converter.jpg"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>


<!--slide Two-->
<li class="ajax_block_product slider-item item  first-in-line first-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/Cat5e.jpg"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  last-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/CAB-UTP5CCA-305M.jpg"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  last-item-of-tablet-line first-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/1.8%20HDMI.png"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  first-item-of-tablet-line last-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Cable/1.8%20HJDMI%20BOX.png"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>
<li class="ajax_block_product slider-item item  last-in-line first-item-of-mobile-line">
    <div class="product-container">
        <div class="product-block">
            <a href="singleProduct.php"
               class="lnk_img product-image" title="Product Name"><img
                    src="img/products/Media%20Converter/Netlink%20Media%20Converter.jpg"
                    style="height: 200px"
                    alt="Product Name"/></a>

        </div>
        <h5 class="product-name">
            <a href="singleProduct.php"
               title="Product Name">Product Name</a>
        </h5>
        <p class="price_display">
            <span class="price product-price">&#2547;152.46</span>
        </p>
    </div>
</li>