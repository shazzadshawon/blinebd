<script type="text/javascript">

    var CUSTOMIZE_TEXTFIELD = 1;
    var FancyboxI18nClose = 'Close';
    var FancyboxI18nNext = 'Next';
    var FancyboxI18nPrev = 'Previous';
    var PS_CATALOG_MODE = false;
    var added_to_wishlist = 'Added to your wishlist.';
    var ajax_allowed = true;
    var ajaxsearch = true;
    var allowBuyWhenOutOfStock = false;
    var attribute_anchor_separator = '-';
    var attributesCombinations = [{"id_attribute":"1","id_attribute_group":"1","attribute":"s","group":"size"},{"id_attribute":"11","id_attribute_group":"3","attribute":"black","group":"color"},{"id_attribute":"8","id_attribute_group":"3","attribute":"white","group":"color"},{"id_attribute":"2","id_attribute_group":"1","attribute":"m","group":"size"},{"id_attribute":"3","id_attribute_group":"1","attribute":"l","group":"size"}];
    var availableLaterValue = '';
    var availableNowValue = 'In stock';
    var baseDir = 'index-2.html';
    var baseUri = 'index.html';
    var blocksearch_type = 'top';
    var combinations = {"101":{"attributes_values":{"1":"S","3":"White"},"attributes":[1,8],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'1','8'"},"100":{"attributes_values":{"1":"S","3":"Black"},"attributes":[1,11],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":4996,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'1','11'"},"103":{"attributes_values":{"1":"M","3":"White"},"attributes":[2,8],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'2','8'"},"102":{"attributes_values":{"1":"M","3":"Black"},"attributes":[2,11],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'2','11'"},"105":{"attributes_values":{"1":"L","3":"White"},"attributes":[3,8],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'3','8'"},"104":{"attributes_values":{"1":"L","3":"Black"},"attributes":[3,11],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'3','11'"}};
    var combinationsFromController = {"101":{"attributes_values":{"1":"S","3":"White"},"attributes":[1,8],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'1','8'"},"100":{"attributes_values":{"1":"S","3":"Black"},"attributes":[1,11],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":4996,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'1','11'"},"103":{"attributes_values":{"1":"M","3":"White"},"attributes":[2,8],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'2','8'"},"102":{"attributes_values":{"1":"M","3":"Black"},"attributes":[2,11],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'2','11'"},"105":{"attributes_values":{"1":"L","3":"White"},"attributes":[3,8],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'3','8'"},"104":{"attributes_values":{"1":"L","3":"Black"},"attributes":[3,11],"price":0,"specific_price":false,"ecotax":0,"weight":0,"quantity":5000,"reference":"","unit_impact":"0.00","minimal_quantity":"1","available_date":"","id_image":-1,"list":"'3','11'"}};
    var confirm_report_message = 'Are you sure you want report this comment?';
    var contentOnly = false;
    var comparedProductsIds = [];
    var currencyBlank = 0;
    var currencyFormat = 1;
    var currencyRate = 1;
    var currencySign = '$';
    var currentDate = '2017-10-25 00:47:36';
    var customizationFields = false;
    var customizationIdMessage = 'Customization #';
    var default_eco_tax = 0;
    var delete_txt = 'Delete';
    var displayDiscountPrice = '0';
    var displayPrice = 1;
    var doesntExist = 'This combination does not exist for this product. Please select another combination.';
    var doesntExistNoMore = 'This product is no longer in stock';
    var doesntExistNoMoreBut = 'with those attributes but is available with others.';
    var ecotaxTax_rate = 0;
    var fieldRequired = 'Please fill in all the required fields before saving your customization.';
    var freeProductTranslation = 'Free!';
    var freeShippingTranslation = 'Free shipping!';
    var group_reduction = 0;
    var idDefaultImage = 135;
    var id_lang = 1;
    var id_product = 14;
    var img_dir = 'themes/PRS060150/img/index.html';
    var img_prod_dir = 'img/p/index.html';
    var img_ps_dir = 'img/index.html';
    var instantsearch = false;
    var isGuest = 0;
    var isLogged = 0;
    var jqZoomEnabled = true;
    var loggin_required = 'You must be logged in to manage your wishlist.';
    var maxQuantityToAllowDisplayOfLastQuantityMessage = 3;
    var minimalQuantity = 1;
    var moderation_active = true;
    var mywishlist_url = 'indexc55c.html?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=1';
    var noTaxForThisProduct = true;
    var oosHookJsCodeFunctions = [];
    var page_name = 'product';
    var priceDisplayMethod = 1;
    var priceDisplayPrecision = 2;
    var productAvailableForOrder = true;
    var productBasePriceTaxExcluded = 142;
    var productHasAttributes = true;
    var productPrice = 142;
    var productPriceTaxExcluded = 142;
    var productPriceWithoutReduction = 142;
    var productReference = 'demo_15';
    var productShowPrice = true;
    var productUnitPriceRatio = 0;
    var product_specific_price = [];
    var productcomment_added = 'Your comment has been added!';
    var productcomment_added_moderation = 'Your comment has been added and will be available once approved by a moderator';
    var productcomment_ok = 'OK';
    var productcomment_title = 'New comment';
    var productcomments_controller_url = 'index0260.html?fc=module&amp;module=productcomments&amp;controller=default&amp;id_lang=1';
    var productcomments_url_rewrite = false;
    var quantitiesDisplayAllowed = true;
    var quantityAvailable = 29996;
    var quickView = true;
    var reduction_percent = 0;
    var reduction_price = 0;
    var removingLinkText = 'remove this product from my cart';
    var roundMode = 2;
    var search_url = 'indexbd1a.html?controller=search';
    var secure_key = '59708ada0c520e749ce6ea7abb887511';
    var specific_currency = false;
    var specific_price = 0;
    var static_token = 'cc110c7f9f9b6cbcf2ff0d4816063f47';
    var stf_msg_error = 'Your e-mail could not be sent. Please check the e-mail address and try again.';
    var stf_msg_required = 'You did not fill required fields';
    var stf_msg_success = 'Your e-mail has been sent successfully';
    var stf_msg_title = 'Send to a friend';
    var stf_secure_key = 'b46e94da0bc6f7746875376b2e1dbcbf';
    var stock_management = 1;
    var taxRate = 0;
    var token = 'cc110c7f9f9b6cbcf2ff0d4816063f47';
    var upToTxt = 'Up to';
    var uploading_in_progress = 'Uploading in progress, please be patient.';
    var usingSecureMode = false;
    var wishlistProductsIds = false;

//    var CUSTOMIZE_TEXTFIELD = 1;
//    var FancyboxI18nClose = 'Close';
//    var FancyboxI18nNext = 'Next';
//    var FancyboxI18nPrev = 'Previous';
//    var added_to_wishlist = 'Added to your wishlist.';
//    var ajax_allowed = true;
//    var ajaxsearch = true;
//    var baseDir = 'index-2.html';
//    var baseUri = 'index.html';
//    var blocksearch_type = 'top';
//    var comparator_max_item = 3;
//    var comparedProductsIds = [];
//    var contentOnly = false;
//    var customizationIdMessage = 'Customization #';
//    var delete_txt = 'Delete';
//    var freeProductTranslation = 'Free!';
//    var freeShippingTranslation = 'Free shipping!';
//    var id_lang = 1;
//    var img_dir = 'themes/PRS060150/img/index.html';
//    var instantsearch = false;
//    var isGuest = 0;
//    var isLogged = 0;
//    var loggin_required = 'You must be logged in to manage your wishlist.';
//    var max_item = 'You cannot add more than 3 product(s) to the product comparison';
//    var min_item = 'Please select at least one product';
//    var mywishlist_url = 'indexc55c.html?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=1';
//    var priceDisplayMethod = 1;
//    var priceDisplayPrecision = 2;
//    var quickView = true;
//    var removingLinkText = 'remove this product from my cart';
//    var request = 'index2aec.html?id_category=21&amp;controller=category&amp;id_lang=1&amp;id_lang=1';
//    var roundMode = 2;
//    var search_url = 'indexbd1a.html?controller=search';
//    var static_token = 'cc110c7f9f9b6cbcf2ff0d4816063f47';
//    var token = '47dee978fd519b0ec657e89bd66a5de4';
//    var usingSecureMode = false;
//    var wishlistProductsIds = false;

</script>
<script type="text/javascript" src="js/jquery/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery/plugins/jquery.easing.js"></script>
<script type="text/javascript" src="js/tools.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/global.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/autoload/10-bootstrap.min.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/autoload/15-jquery.total-storage.min.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/autoload/15-jquery.uniform-modified.js"></script>
<script type="text/javascript" src="js/jquery/plugins/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/products-comparison.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/category.js"></script>
<script type="text/javascript" src="js/jquery/plugins/jquery.idTabs.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blockcart/ajax-cart.js"></script>
<script type="text/javascript" src="js/jquery/plugins/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js/jquery/plugins/jquery.serialScroll.js"></script>
<script type="text/javascript" src="js/jquery/plugins/bxslider/jquery.bxslider.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/product.js"></script>
<script type="text/javascript" src="js/jquery/plugins/jqzoom/jquery.jqzoom.js"></script>
<script type="text/javascript" src="js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blocksearch/blocksearch.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blockwishlist/js/ajax-wishlist.js"></script>
<script type="text/javascript" src="modules/productcomments/js/jquery.rating.pack.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/sendtoafriend/sendtoafriend.js"></script>
<script type="text/javascript" src="modules/socialsharing/js/socialsharing.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/tools/treeManagement.js"></script>
<script type="text/javascript" src="modules/tmhomeslider/js/tm_jquery.flexslider.min.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blocknewsletter/blocknewsletter.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/crossselling/js/crossselling.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/productscategory/js/productscategory.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blocktopmenu/js/hoverIntent.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blocktopmenu/js/superfish-modified.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/blocktopmenu/js/blocktopmenu.js"></script>
<script type="text/javascript" src="modules/productcomments/js/jquery.textareaCounter.plugin.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/modules/productcomments/js/productcomments.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/megnor/jcarousel.min.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/megnor/owl.carousel.js"></script>
<script type="text/javascript" src="themes/PRS060150/js/megnor/custom.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('.flexslider').flexslider({
            slideshowSpeed: 1500,
            pauseOnHover: false
        });
    });
    writeBookmarkLink('index.html', 'Demo Store', 'bookmark');
</script>



<!--<script type="text/javascript">-->
<!--    var CUSTOMIZE_TEXTFIELD = 1;-->
<!--    var FancyboxI18nClose = 'Close';-->
<!--    var FancyboxI18nNext = 'Next';-->
<!--    var FancyboxI18nPrev = 'Previous';-->
<!--    var added_to_wishlist = 'Added to your wishlist.';-->
<!--    var ajax_allowed = true;-->
<!--    var ajaxsearch = true;-->
<!--    var baseDir = 'index-2.html';-->
<!--    var baseUri = 'index.html';-->
<!--    var blocksearch_type = 'top';-->
<!--    var comparator_max_item = 3;-->
<!--    var comparedProductsIds = [];-->
<!--    var contentOnly = false;-->
<!--    var customizationIdMessage = 'Customization #';-->
<!--    var delete_txt = 'Delete';-->
<!--    var freeProductTranslation = 'Free!';-->
<!--    var freeShippingTranslation = 'Free shipping!';-->
<!--    var id_lang = 1;-->
<!--    var img_dir = 'themes/PRS060150/img/index.html';-->
<!--    var instantsearch = false;-->
<!--    var isGuest = 0;-->
<!--    var isLogged = 0;-->
<!--    var loggin_required = 'You must be logged in to manage your wishlist.';-->
<!--    var max_item = 'You cannot add more than 3 product(s) to the product comparison';-->
<!--    var min_item = 'Please select at least one product';-->
<!--    var mywishlist_url = 'indexc55c.html?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=1';-->
<!--    var page_name = 'category';-->
<!--    var priceDisplayMethod = 1;-->
<!--    var priceDisplayPrecision = 2;-->
<!--    var quickView = true;-->
<!--    var removingLinkText = 'remove this product from my cart';-->
<!--    var request = 'index2aec.html?id_category=21&amp;controller=category&amp;id_lang=1&amp;id_lang=1';-->
<!--    var roundMode = 2;-->
<!--    var search_url = 'indexbd1a.html?controller=search';-->
<!--    var static_token = 'cc110c7f9f9b6cbcf2ff0d4816063f47';-->
<!--    var token = '47dee978fd519b0ec657e89bd66a5de4';-->
<!--    var usingSecureMode = false;-->
<!--    var wishlistProductsIds = false;-->
<!--</script>-->
<!--<script type="text/javascript" src="js/jquery/jquery-1.11.0.min.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/jquery-migrate-1.2.1.min.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/plugins/jquery.easing.js"></script>-->
<!--<script type="text/javascript" src="js/tools.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/global.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/autoload/10-bootstrap.min.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/autoload/15-jquery.total-storage.min.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/autoload/15-jquery.uniform-modified.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/plugins/fancybox/jquery.fancybox.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/products-comparison.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/category.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blockcart/ajax-cart.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/plugins/jquery.scrollTo.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/plugins/jquery.serialScroll.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/plugins/bxslider/jquery.bxslider.js"></script>-->
<!--<script type="text/javascript" src="js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blocksearch/blocksearch.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blockwishlist/js/ajax-wishlist.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/tools/treeManagement.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blocknewsletter/blocknewsletter.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blocktopmenu/js/hoverIntent.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blocktopmenu/js/superfish-modified.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/modules/blocktopmenu/js/blocktopmenu.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/megnor/jcarousel.min.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/megnor/owl.carousel.js"></script>-->
<!--<script type="text/javascript" src="themes/PRS060150/js/megnor/custom.js"></script>-->
<!--<script type="text/javascript">-->
<!--    writeBookmarkLink('indexd081.html?id_category=21&amp;controller=category&amp;id_lang=1', 'Men - Demo Store', 'bookmark');-->
<!--</script>-->