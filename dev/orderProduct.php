<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

<head>
    <title>Order Product|Bline-BD</title>
    <?php require('head.php'); ?>
</head>
<body id="cms" class="cms cms-3 cms-terms-and-conditions-of-use hide-right-column lang_en">
<div id="page">
    <div class="header-container">
        <?php require('header.php'); ?>
    </div>
    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    <?php require('weekly_special.php'); ?>

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    <?php require('sidebar_list.php'); ?>
                    <!-- /Block categories module -->


                    <div id="tmleft-banner" class="block">
                        <ul>
                            <li class="tmleftbanner-container">
                                <a href="#" title="LeftBanner 1">
                                    <img src="modules/tmleftbanner/img/left-banner-1.jpg"
                                         alt="LeftBanner 1"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="index.php" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">Order Product</span>
                    </div>
                    <!-- /Breadcrumb -->

                    <h1 class="page-heading bottom-indent">
                        Customer service - Order Product</h1>


                    <form action="orderProduct.php" method="post" class="contact-form-box" enctype="multipart/form-data">
                        <fieldset>
                            <h3 class="page-subheading">Product Order Form</h3>
                            <div class="clearfix">
                                <div class="col-xs-12 col-md-4">

                                    <p id="desc_contact0" class="desc_contact">&nbsp;</p>
                                    <p id="desc_contact2" class="desc_contact contact-title" style="display:none;">
                                        <i class="icon-comment-alt"></i>For any question about a product, an order
                                    </p>
                                    <p id="desc_contact1" class="desc_contact contact-title" style="display:none;">
                                        <i class="icon-comment-alt"></i>If a technical problem occurs on this website
                                    </p>
                                    <p class="form-group">
                                        <label for="name">Product Code</label>
                                        <input class="form-control grey validate" type="text" id="code" name="code" value="" />
                                    </p>
                                    <p class="form-group">
                                        <label for="name">Customer Name</label>
                                        <input class="form-control grey validate" type="text" id="name" name="name" value="" />
                                    </p>
                                    <p class="form-group">
                                        <label for="email">Email address</label>
                                        <input class="form-control grey validate" type="text" id="email" name="from" data-validate="isEmail" value="" />
                                    </p>
                                    <p class="form-group">
                                        <label for="email">Contact No</label>
                                        <input class="form-control grey validate" type="text" id="contact" name="contact" value="" />
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <div class="form-group">
                                        <label for="message">Detailed Address</label>
                                        <textarea class="form-control" id="address" name="address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea class="form-control" id="message" name="message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="submit">
                                <button type="submit" name="submitMessage" id="submitMessage" class="button btn btn-default button-medium"><span>Send<i class="icon-chevron-right right"></i></span></button>
                            </div>
                        </fieldset>
                    </form>

                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

    <!-- Footer -->
    <?php require('footer.php'); ?>
    <!-- #footer -->

</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>

<?php require('foot.php'); ?>

</body>

</html>