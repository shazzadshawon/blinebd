<div id="best-sellers_block_right" class="block products_block">
    <h4 class="title_block">
        <a href="#"> Top sellers</a>
    </h4>

    <div class="block_content products-block">
        <div class=" jcarousel-skin-tango">
            <div style="position: relative; display: block;" class="jcarousel-container jcarousel-container-vertical">
                <div style="overflow: hidden; position: relative;" class="jcarousel-clip jcarousel-clip-vertical">
                    <ul id="prod-slider" class="product_images">

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/CAB-UTP5CCA-305M.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/Cat5e.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/CAB-UTP5CCA-305M.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/Cat5e.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/CAB-UTP5CCA-305M.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/Cat5e.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/CAB-UTP5CCA-305M.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/Cat5e.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/CAB-UTP5CCA-305M.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>

                        <li class="first_item clearfix jcarousel-item">
                            <a class="products-block-image"
                               href="singleProduct.php" title="" class="content_img clearfix">
                                <img src="img/products/Cable/Cat5e.jpg" width="58" height="58" alt=""/>
                            </a>
                            <div class="product-content">
                                <a href="singleProduct.php" title="">Product Name<br/>
                                    <span class="price">&#2547;152.46</span> </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>