<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

<head>
    <title>About Us|Bline-BD</title>
    <?php require('head.php'); ?>
</head>
<body id="cms" class="cms cms-3 cms-terms-and-conditions-of-use hide-right-column lang_en">
<div id="page">
    <div class="header-container">
        <?php require('header.php'); ?>
    </div>
    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    <?php require('weekly_special.php'); ?>

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="left_column" class="column col-xs-12" style="width:21%;"><!-- Block categories module -->
                    <?php require('sidebar_list.php'); ?>
                    <!-- /Block categories module -->


                    <div id="tmleft-banner" class="block">
                        <ul>
                            <li class="tmleftbanner-container">
                                <a href="#" title="LeftBanner 1">
                                    <img src="modules/tmleftbanner/img/left-banner-1.jpg"
                                         alt="LeftBanner 1"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="center_column" class="center_column col-xs-12" style="width:79%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="index.php" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe" >&gt;</span>
                        <span class="navigation_page">About</span>
                    </div>
                    <!-- /Breadcrumb -->

                    <div class="rte">
                        <h1 class="page-heading bottom-indent">About us</h1>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-block">
                                    <h3 class="page-subheading">Our company</h3>
                                    <p><strong class="dark">B-line enterprise  is a Hi-Tech enterprise having its own import, supply and sales of fiber optical and communications equipments.</strong></p>
                                    <p>The main products of b-line include UTP Cables , Fiber optic patch cord, Connector, Adapter, Optical coupler, Attenuator, Optic media converter, Optic splice closure, Optic terminal box, ODF etc, which are widely used in telecommunications, broadband, broadcast, CATV and network industry.</p>
                                    <ul class="list-1">
                                        <li><em class="icon-ok"></em>Top quality products</li>
                                        <li><em class="icon-ok"></em>Best customer service</li>
                                        <li><em class="icon-ok"></em>30-days money back guarantee</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-box">
                                    <h3 class="page-subheading">Our Sells Point</h3>
                                    <img title="cms-img" src="img/dummy.jpg" alt="cms-img" width="370" height="192" />

                                    <p><strong class="dark">Bline BD Enterprise </strong></p>
                                    <p>Radio Biponi</p>
                                    <p>25, Swimmingpool Market</p>
                                    <p>Bangabandhu National Stadium road</p>
                                    <p>Dhaka-1000 Bangladesh</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-box">
                                    <h3 class="page-subheading">Testimonials / Reviews</h3>
                                    <div class="testimonials">
                                        <div class="inner"><span class="before">&ldquo;</span> Review Text Review Text Review Text Review Text Review Text Review Text Review Text Review TextReview Text Review Text Review Text Review Text<span class="after">&rdquo;</span></div>
                                    </div>
                                    <p><strong class="dark">Reviewer Name</strong></p>
                                    <div class="testimonials">
                                        <div class="inner"><span class="before">&ldquo;</span>Review Text Review Text Review Text Review Text Review Text Review Text Review Text Review TextReview Text Review Text Review Text Review Text<span class="after">&rdquo;</span></div>
                                    </div>
                                    <p><strong class="dark">Reviewer Name</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

    <!-- Footer -->
    <?php require('footer.php'); ?>
    <!-- #footer -->

</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>

<?php require('foot.php'); ?>

</body>

</html>