<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

    <head>
        <title>Bline-BD</title>
        <?php require('head.php'); ?>
    </head>
    <body id="index" class="index hide-left-column hide-right-column lang_en">
    <div id="page">
        <div class="header-container">
            <?php require('header.php'); ?>
        </div>
        <div class="columns-container">
            <div id="columns" class="container">
                <div class="row">
                    <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                        <?php require('weekly_special.php'); ?>

                        <!-- Home Slider -->
                        <?php require('home_slider.php'); ?>
                        <!-- /Home Slider -->
                    </div>
                </div>

                <div class="row" id="columns_inner">
                    <div id="center_column" class="center_column col-xs-12" style="width:100%;">

                        <!--Featured Products and New Products Tab-->
                        <?php require('featuredProduct.php'); ?>
                        <!--//Featured Products and New Products Tab-->

                        <?php require('top_saller.php'); ?>

                    </div><!-- #center_column -->
                </div><!-- .row -->

            </div><!-- #columns -->
        </div><!-- .columns-container -->

        <!-- Footer -->
        <?php require('footer.php'); ?>
        <!-- #footer -->
    </div><!-- #page -->
    <a class="top_button" href="#" style="display:none;">&nbsp;</a>

    <script>var page_name = 'index.php';</script>
    <?php require('foot.php'); ?>

    </body>
</html>