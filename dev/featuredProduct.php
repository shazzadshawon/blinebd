<section class="tm-hometabcontent">
    <ul id="home-page-tabs" class="nav nav-tabs clearfix">
        <li class="active"><a data-toggle="tab" href="#featureProduct" class="tm-hometab">Featured products</a></li>
        <li class=""><a data-toggle="tab" href="#newProduct" class="tm-hometab">New products</a></li>
    </ul>
    <div class="tab-content">

        <div id="featureProduct" class="tm_productinner tab-pane active">

            <div id="featured-products_block_center" class="block products_block clearfix">
                <div class="block_content">

                    <div class="customNavigation">
                        <a class="btn prev feature_prev"><i class="icon-chevron-sign-left"></i></a>
                        <a class="btn next feature_next"><i class="icon-chevron-sign-right"></i></a>
                    </div>
                    <ul id="feature-carousel" class="tm-carousel product_list">

                        <!--  This is the 1st line of products-->
                        <?php require('featuredNew_product_items.php'); ?>

                        <!--  This is the 2nd line of products-->
                        <?php require('featuredNew_product_items.php'); ?>

                    </ul>
                </div>

            </div>
        </div>

        <div id="newProduct" class="tm_productinner tab-pane">
            <!-- TM - NewProduct -->
            <div id="newproducts_block" class="block products_block">
                <div class="block_content">

                    <div class="customNavigation">
                        <a class="btn prev newproduct_prev"><i
                                    class="icon-chevron-sign-left"></i></a>
                        <a class="btn next newproduct_next"><i class="icon-chevron-sign-right"></i></a>
                    </div>
                    <ul id="newproduct-carousel" class="tm-carousel product_list">
                        <!--  This is the 1st line of products-->
                        <?php require('featuredNew_product_items.php'); ?>

                        <!--  This is the 2nd line of products-->
                        <?php require('featuredNew_product_items.php'); ?>
                    </ul>
                </div>
            </div>
            <!-- TM - NewProduct -->

        </div>
    </div>
</section>