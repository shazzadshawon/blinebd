<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">

<head>
    <title>Product Details|Bline-BD</title>

    <?php require('head.php'); ?>
</head>
<body id="product"
      class="product product-14 product-blouse category-7 category-sony hide-left-column hide-right-column lang_en">
<div id="page">
    <div class="header-container">

        <?php require('header.php'); ?>

    </div>
    <div class="columns-container">
        <div id="columns" class="container">
            <div class="row">
                <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Block links module -->

                    <?php require('weekly_special.php'); ?>

                </div>
            </div>
            <div class="row" id="columns_inner">
                <div id="center_column" class="center_column col-xs-12" style="width:100%;">

                    <!-- Breadcrumb -->
                    <div class="breadcrumb clearfix">
                        <a class="home" href="index.php" title="Return to Home"><i class="icon-home"></i></a>
                        <span class="navigation-pipe">&gt;</span>
                        <a href="category_page.php" title="Category Name">Category Name</a>
                        <span class="navigation-pipe">></span>
                        <a href="subCategory_page.php" title="Sub Category Name">Sub Category Name</a>
                        <span class="navigation-pipe">></span>Product Name
                    </div>
                    <!-- /Breadcrumb -->


                    <div class="primary_block row" itemscope itemtype="http://schema.org/Product">
                        <div class="container">
                            <div class="top-hr"></div>
                        </div>
                        <!-- left infos-->
                        <div class="pb-left-column col-xs-12 col-sm-4 col-md-4">
                            <!-- product img-->
                            <div id="image-block" class="clearfix">
                                <span class="new-box">
						            <span class="new-label">New</span>
					            </span>
                                <span class="sale-box no-print">
						            <span class="sale-label">Sale!</span>
					            </span>
                                <span id="view_full_size">
                                    <a class="jqzoom" title="Product Name " rel="gal1" href="img/products/Cable/1.8%20HDMI.png" itemprop="url">
                                        <img itemprop="image" src="img/products/Cable/1.8%20HDMI.png" title="Product Name " alt="Product Name "/>
							        </a>
                                </span>
                            </div> <!-- end image-block -->
                            <!-- thumbnails -->
                            <div id="views_block" class="clearfix ">
                                <span class="view_scroll_spacer">
                                    <a id="view_scroll_left" class="" title="Other views" href="javascript:{}">Previous</a>
                                </span>

                                <div id="thumbs_list">
                                    <ul id="thumbs_list_frame">
                                        <li id="thumbnail_128">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Cable/1.8%20HDMI.png',largeimage: 'img/products/Cable/1.8%20HDMI.png'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_128" src="img/products/Cable/1.8%20HDMI.png" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        <li id="thumbnail_129">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Cable/CAB-UTP5CCA-305M.jpg',largeimage: 'img/products/Cable/CAB-UTP5CCA-305M.jpg'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_129" src="img/products/Cable/CAB-UTP5CCA-305M.jpg" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        <li id="thumbnail_130">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Cable/Cat5e.jpg',largeimage: 'img/products/Cable/Cat5e.jpg'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_130" src="img/products/Cable/Cat5e.jpg" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        <li id="thumbnail_135" class="last">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Media%20Converter/Netlink%20Media%20Converter.jpg',largeimage: 'img/products/Media%20Converter/Netlink%20Media%20Converter.jpg'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_135" src="img/products/Media%20Converter/Netlink%20Media%20Converter.jpg" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>


                                        <li id="thumbnail_128">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Cable/1.8%20HDMI.png',largeimage: 'img/products/Cable/1.8%20HDMI.png'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_128" src="img/products/Cable/1.8%20HDMI.png" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        <li id="thumbnail_129">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Cable/CAB-UTP5CCA-305M.jpg',largeimage: 'img/products/Cable/CAB-UTP5CCA-305M.jpg'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_129" src="img/products/Cable/CAB-UTP5CCA-305M.jpg" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        <li id="thumbnail_130">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Cable/Cat5e.jpg',largeimage: 'img/products/Cable/Cat5e.jpg'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_130" src="img/products/Cable/Cat5e.jpg" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                        <li id="thumbnail_135" class="last">
                                            <a href="javascript:void(0);"
                                               rel="{gallery: 'gal1', smallimage: 'img/products/Media%20Converter/Netlink%20Media%20Converter.jpg',largeimage: 'img/products/Media%20Converter/Netlink%20Media%20Converter.jpg'}"
                                               title="Product Name ">
                                                <img class="img-responsive" id="thumb_135" src="img/products/Media%20Converter/Netlink%20Media%20Converter.jpg" alt="Product Name " title="Product Name " height="80" width="80" itemprop="image"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div> <!-- end thumbs_list -->

                                <a id="view_scroll_right" title="Other views" href="javascript:{}">Next</a>
                            </div> <!-- end views-block -->
                            <!-- end thumbnails -->
                            <p class="resetimg clear no-print">
					<span id="wrapResetImages" style="display: none;">
						<a href="singleProduct.php" id="resetImages">
							<i class="icon-repeat"></i>
							Display all pictures
						</a>
					</span>
                            </p>
                        </div> <!-- end pb-left-column -->
                        <!-- end left infos-->
                        <!-- center infos -->
                        <div class="pb-center-column col-xs-12 col-sm-5">
                            <h1 itemprop="name">Product Name </h1>
                            <div id="short_description_block">
                                <div id="short_description_content" class="rte align_justify" itemprop="description"><p>
                                        Product Short Description Product Short DescriptionProduct Short Description Product Short Description Product Short Description </p></div>

                                <p class="buttons_bottom_block">
                                    <a href="javascript:{}" class="button">
                                        More details
                                    </a>
                                </p>
                                <!---->
                            </div> <!-- end short_description_block -->
                            <!-- number of item in stock -->
                            <p id="pQuantityAvailable">
                                <span id="quantityAvailable">29996</span>
                                <span style="display: none;" id="quantityAvailableTxt">Item</span>
                                <span id="quantityAvailableTxtMultiple">Items</span>
                            </p>
                            <!-- availability -->
                            <p id="availability_statut">

                                <span id="availability_value">In stock</span>
                            </p>


                            <!--  /Module ProductComments -->
                            <p class="socialsharing_product list-inline no-print">
                                <button type="button" class="btn btn-default btn-twitter"
                                        onclick="socialsharing_twitter_click('Mauris%20blandit%20vehicula%20%20http_/prestashop.templatemela.com/PRS06/PRS060150/index0a93.html?id_product=14&amp;controller=product&amp;id_lang=1');">
                                    <i class="icon-twitter"></i> Tweet
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/twitter.gif" alt="Tweet" /> -->
                                </button>
                                <button type="button" class="btn btn-default btn-facebook"
                                        onclick="socialsharing_facebook_click();">
                                    <i class="icon-facebook"></i> Share
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/facebook.gif" alt="Facebook Like" /> -->
                                </button>
                                <button type="button" class="btn btn-default btn-google-plus"
                                        onclick="socialsharing_google_click();">
                                    <i class="icon-google-plus"></i> Google+
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/google.gif" alt="Google Plus" /> -->
                                </button>
                                <button type="button" class="btn btn-default btn-pinterest"
                                        onclick="socialsharing_pinterest_click();">
                                    <i class="icon-pinterest"></i> Pinterest
                                    <!-- <img src="http://prestashop.templatemela.com/PRS06/PRS060150/modules/socialsharing/img/pinterest.gif" alt="Pinterest" /> -->
                                </button>
                            </p>

                            <!-- usefull links-->
                            <ul id="usefull_link_block" class="clearfix no-print">
                                <li class="sendtofriend">
                                    <a id="send_friend_button" href="#send_friend_form">
                                        Send to a friend
                                    </a>
                                    <div style="display: none;">
                                        <div id="send_friend_form">
                                            <h2 class="page-subheading">
                                                Send to a friend
                                            </h2>
                                            <div class="row">
                                                <div class="product clearfix col-xs-12 col-sm-6">
                                                    <img src="img/p/1/3/5/135-home_default.jpg" height="200" width="200"
                                                         alt="Product Name "/>
                                                    <div class="product_desc">
                                                        <p class="product_name">
                                                            <strong>Product Name </strong>
                                                        </p>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                                                            at ante. Mauris eleifend, quam a vulputate dictum, massa
                                                            quam dapibus leo, eget vulputate orci purus ut lorem. In
                                                            fringilla mi in ligula.</p>
                                                    </div>
                                                </div><!-- .product -->
                                                <div class="send_friend_form_content col-xs-12 col-sm-6"
                                                     id="send_friend_form_content">
                                                    <div id="send_friend_form_error"></div>
                                                    <div id="send_friend_form_success"></div>
                                                    <div class="form_container">
                                                        <p class="intro_form">
                                                            Recipient :
                                                        </p>
                                                        <p class="text">
                                                            <label for="friend_name">
                                                                Name of your friend <sup class="required">*</sup> :
                                                            </label>
                                                            <input id="friend_name" name="friend_name" type="text"
                                                                   value=""/>
                                                        </p>
                                                        <p class="text">
                                                            <label for="friend_email">
                                                                E-mail address of your friend <sup
                                                                        class="required">*</sup> :
                                                            </label>
                                                            <input id="friend_email" name="friend_email" type="text"
                                                                   value=""/>
                                                        </p>
                                                        <p class="txt_required">
                                                            <sup class="required">*</sup> Required fields
                                                        </p>
                                                    </div>
                                                    <p class="submit">
                                                        <button id="sendEmail" class="btn button button-small"
                                                                name="sendEmail" type="submit">
                                                            <span>Send</span>
                                                        </button>&nbsp;
                                                        or&nbsp;
                                                        <a class="closefb" href="#">
                                                            Cancel
                                                        </a>
                                                    </p>
                                                </div> <!-- .send_friend_form_content -->
                                            </div>
                                        </div>
                                    </div>
                                </li>


                                <li class="print">
                                    <a href="javascript:print();">
                                        Print
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- end center infos-->
                        <!-- pb-right-column-->
                        <div class="pb-right-column col-xs-12 col-sm-4 col-md-3">
                            <!-- add to cart form-->
                            <form id="buy_block" action="#" method="post">
                                <!-- hidden datas -->
                                <p class="hidden">
                                    <input type="hidden" name="token" value="cc110c7f9f9b6cbcf2ff0d4816063f47"/>
                                    <input type="hidden" name="id_product" value="14" id="product_page_product_id"/>
                                    <input type="hidden" name="add" value="1"/>
                                    <input type="hidden" name="id_product_attribute" id="idCombination" value=""/>
                                </p>
                                <div class="box-info-product">
                                    <div class="content_prices clearfix">
                                        <!-- prices -->
                                        <div class="price">
                                            <p class="our_price_display" itemprop="offers" itemscope
                                               itemtype="http://schema.org/Offer">
                                                <link itemprop="availability" href="http://schema.org/InStock"/>
                                                <span id="our_price_display" itemprop="price">&#2547;142.00</span>
                                                <!---->
                                                <meta itemprop="priceCurrency" content="USD"/>
                                            </p>
                                            <p id="reduction_percent" style="display:none;">
									<span id="reduction_percent_display">
																			</span>
                                            </p>
                                            <p id="old_price" class="hidden">
                                                <span id="old_price_display"></span>
                                                <!--  -->
                                            </p>
                                        </div> <!-- end prices -->
                                        <p id="reduction_amount" style="display:none">
								<span id="reduction_amount_display">
																</span>
                                        </p>

                                        <div class="clear"></div>
                                    </div> <!-- end content_prices -->
                                    <div class="product_attributes clearfix">
                                        <!-- attributes -->
                                        <div id="attributes">
                                            <div class="clearfix"></div>
                                            <fieldset class="attribute_fieldset">
                                                <label class="attribute_label" for="group_1">Size :&nbsp;</label>
                                                <div class="attribute_list">
                                                    <select name="group_1" id="group_1" class="form-control attribute_select no-print">
                                                        <option value="1" selected="selected" title="S">S</option>
                                                        <option value="2" title="M">M</option>
                                                        <option value="3" title="L">L</option>
                                                    </select>
                                                </div> <!-- end attribute_list -->
                                            </fieldset>
                                            <fieldset class="attribute_fieldset">
                                                <input type="hidden" class="color_pick_hidden" name="group_3"
                                                       value="11"/>
                                            </fieldset>
                                        </div> <!-- end attributes -->
                                    </div> <!-- end product_attributes -->

                                </div> <!-- end box-info-product -->


                                <?php require('sidebar_list.php'); ?>

                                <!-- MODULE Block best sellers -->
                                <?php require('top_saller_module.php'); ?>
                                <!-- /MODULE Block best sellers -->


                            </form>
                        </div> <!-- end pb-right-column-->
                    </div> <!-- end primary_block -->

                    <!-- Megnor start : TAB-->
                    <section class="tm-tabcontent">
                        <ul id="productpage_tab" class="nav nav-tabs clearfix">
                            <li class="active"><a data-toggle="tab" href="#moreinfo" class="moreinfo">More info</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- More Info -->
                            <ul id="moreinfo" class="tm_productinner tab-pane active">
                                <p><strong>Some Heading</strong></p>
                                <p>Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description </p>
                                <p><strong>Some Heading</strong></p>
                                <p>Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description Product Description</p>
                            </ul>
                            <!-- End More Info -->
                        </div>

                    </section>
                    <!-- Megnor End :TAB -->


                    <section class="page-product-box blockproductscategory">
                        <h3 class="productscategory_h3 page-product-heading">19 other products in the same category:</h3>
                        <div id="productscategory_list" class="clearfix">

                            <!-- Megnor start -->
                            <!-- Define Number of product for SLIDER -->
                            <div class="customNavigation">
                                <a class="btn prev  productcategory_prev"><i class="icon-chevron-sign-left"></i></a>
                                <a class="btn next productcategory_next"><i class="icon-chevron-sign-right"></i></a>
                            </div>
                            <!-- Megnor End -->

                            <ul id="productcategory-carousel" class="tm-carousel clearfix">
                                <?php require('related_products.php'); ?>
                            </ul>
                        </div>
                    </section>


                </div><!-- #center_column -->
            </div><!-- .row -->
        </div><!-- #columns -->
    </div><!-- .columns-container -->

    <!-- Footer -->
    <?php require('footer.php'); ?>
    <!--Footer-->

</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>

<script>var page_name = 'Products';</script>
<?php require('foot.php'); ?>

</body>

</html>