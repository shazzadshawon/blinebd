<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = DB::table('galleries')
         ->get();
        return view('backend.gallery.galleries',compact('galleries'));
    }

 

    public function add()
    {
        return view('backend.gallery.addgallery');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        //return Input::file('gallery_image');

        $imgname = Input::get('gallery_image');
        //$filename = time().'.jpg';
        $place = Input::get('place');

        //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);


        $file = array_get($input,'gallery_image');
           // SET UPLOAD PATH 
            $destinationPath = 'public/uploads/gallery/'; 
            // GET THE FILE EXTENSION
            $extension = $file->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $fileName = time() . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $upload_success = $file->move($destinationPath, $fileName); 

        
       

        DB::table('galleries')->insert(
        [
            'gallery_image' => $fileName,
            'gallery_image_status' => 1,
            'category_id' => $place,
            'type' => 0,
        ]
        );
         return redirect('galleries')->with('success', 'New Image Uploaded Successfully');
    }

     public function storevideo(Request $request)
    {
        $input = Input::all();
        //return Input::file('gallery_image');

       // $imgname = Input::get('video_name');
        //$filename = time().'.jpg';
        $place = Input::get('place');

        //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);


        $video = array_get($input,'video_name');
           // SET UPLOAD PATH 
            $destinationPath = 'public/uploads/gallery/'; 
            // GET THE FILE EXTENSION
            $extension = $video->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $name = time();
            $fileName =  $name . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $upload_success = $video->move($destinationPath, $fileName); 

        $cover = array_get($input,'cover_image');
           // SET UPLOAD PATH 
            $destinationPath = 'public/uploads/gallery/'; 
            // GET THE FILE EXTENSION
            $extension = $cover->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $cover_image = $name . '-cover.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $upload_success1 = $cover->move($destinationPath, $cover_image); 

        
       

        DB::table('galleries')->insert(
        [
            'video_name' => $fileName,
            'cover_image' => $fileName,
            'gallery_image_status' => 1,
            'category_id' => $place,
            'type' => 1,
            'cover_image' => $cover_image,
        ]
        );
         return redirect('galleries')->with('success', 'New video Uploaded Successfully');
    }

   

    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        $gal = DB::table('galleries')
                    ->where('id',$id)
                    ->get();
                    $gallery = $gal[0];
        return view('backend.gallery.editgallery',compact('gallery'));
    }



    public function update(Request $request, $id)
    {
        $place = Input::get('place');
          if(Input::file('gallery_image'))
            {
                //return 'hy';
                 $filename = time().'.jpg';
Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);
                   

        // if($place == 'wedding'){
        //     //return 1;
            
        // }
        // elseif($place == 'events'){
        //     Image::make(Input::file('gallery_image'))->save('public/images/gallery/events/'.$filename);
        // }
        // elseif ($place == 'music') {
        //      Image::make(Input::file('gallery_image'))->save('public/images/gallery/music/'.$filename);
        // }


                 //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);

                   DB::table('galleries')
            ->where('id', $id)
            ->update([
                    
                    'gallery_image' => $filename,
                    'folder' => $place,
                    
                ]);


             //return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
             }
             else{

                   DB::table('galleries')
            ->where('id', $id)
            ->update([
                    
                    
                    'folder' => $place,
                    
                ]);


             }
             return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //return
         $gallery = DB::table('galleries')
            ->where('id', $id)->get();
 //$folder = $image[0]->folder;

            //$img = Image::destroy('public/images/gallery/'.$folder.'/'.$image[0]->gallery_image);

            //$img->destroy();
            if($gallery[0]->type=='0')
            {
                unlink('public/uploads/gallery/'.$gallery[0]->gallery_image);
                DB::table('galleries')
                ->where('id', $id)->delete();
                return redirect('galleries')->with('success', ' Image Deleted Successfully');
            }
            else{
               unlink('public/uploads/gallery/'.$gallery[0]->video_name); 
               unlink('public/uploads/gallery/'.$gallery[0]->cover_image); 
                DB::table('galleries')
                ->where('id', $id)->delete();
                return redirect('galleries')->with('success', ' Video Deleted Successfully');
            }


        
    }
}
