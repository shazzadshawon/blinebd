<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Product;
use App\ProductImage;
use App\Category;
use App\SubCategory;
use DB;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SubCategory = DB::table('sub_categories')
                ->join('categories', 'sub_categories.category_id', '=', 'categories.category_id')               
                ->select('sub_categories.*', 'categories.category_name')
                ->get();
        return view('backend.manage_sub_category')->with('SubCategories',$SubCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $categories = Category::where('publication_status',1)->get();
          return view('backend.add_sub_category')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,array(
            'category_id'=>'required',            
           'sub_category_name'=>'required|max:255'
       ));
        $sub_category = new SubCategory;
      
       $sub_category->category_id = $request->category_id;
       $sub_category->sub_category_name = $request->sub_category_name;
       $sub_category->sub_category_name_bn = $request->sub_category_name;
       $sub_category->publication_status = $request->publication_status;
       $sub_category->save();
       if($sub_category){
           Session::flash('success','Sub Category has been Saved Successfully ....!');
        return Redirect::to('/add-sub-category');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $SubCategories = SubCategory::where('sub_category_id',$id)->first();
        $category = Category::where('publication_status',1)->get();
//        // return the view and pass in the var we previously created
//        return view('backend.edit_sub_category')->withSubCategory($SubCategories)->withCategory($category);
        
           // find the post in the database and save as a var
        $sub_category = SubCategory::where('sub_category_id',$id)->first();
        // return the view and pass in the var we previously created
        return view('backend.edit_sub_category')->withSubCategory($sub_category)->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, array(
            'category_id' => 'required',
            'sub_category_name' => 'required|max:255'
        ));
        $sub_category = SubCategory::where('sub_category_id', $id)
                ->update(['category_id' => $request->category_id,
            'sub_category_name' => $request->sub_category_name,
            'sub_category_name_bn' => $request->sub_category_name_bn,
            'publication_status' => $request->publication_status
        ]);

        Session::flash('success', 'Sub Category Has Been Updated Successfully..!');
        return Redirect::to('/manage-sub-category');
    }
      public function unpublished($id) {

//        $category = new Category;
        $sub_category = SubCategory::where('sub_category_id', $id)
                ->update(['publication_status' => 0]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();


        Session::flash('success', 'Sub Category Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-sub-category');
    }
    
      public function published( $id)
    {
   
//        $category = new Category;
        $sub_category = SubCategory::where('sub_category_id',$id)
                ->update(['publication_status' =>1]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();
      
        
            Session::flash('success', 'Sub Category Has Been published Successfully..!');
            return Redirect::to('/manage-sub-category');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::where('sub_category_id',$id)->get();
        foreach ($products as $pro) {
            $imgs = ProductImage::where('product_id',$pro->id)->get();
            foreach ($imgs as $img) {
                unlink('product_image/'.$img->product_image);
            }

            ProductImage::where('product_id',$pro->id)->delete();
        }
        Product::where('sub_category_id',$id)->delete();
       
       SubCategory::where('sub_category_id',$id)->delete();

        Session::flash('success', 'Item Has Been Deleted Successfully ....!');
            return Redirect::to('/manage-sub-category');
    }
}
