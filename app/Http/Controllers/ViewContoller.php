<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Category;
use App\SubCategory;
use App\SliderImage;
use App\SubSubCategory;
use App\Product;
use App\Customer;
use App\Wishlist;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class ViewContoller extends Controller
{



    public function index() {

        $main_categories = DB::table('categories')->orderby('category_id','asc')->get();
        $business = DB::table('business')->orderby('id','desc')->first();
        //return $mega_offer;
        $sliders = DB::table('slider_images')->orderby('slider_image_id','desc')->get();
        $featured = DB::table('products')->where('offer_status',1)->orderby('id','desc')->take(10)->get();
        $newarrivals = DB::table('products')->where('offer_status',2)->orderby('id','desc')->take(10)->get();
        $topsales = DB::table('products')->where('offer_status',3)->orderby('id','desc')->take(10)->get();
        $brands = DB::table('brands')->orderby('id','desc')->get();
        $blogs = DB::table('blogs')->orderby('id','desc')->take(3)->get();
        
        // $image = DB::table('product_images')->where('product_id',4)->get();
        // print_r($image);
        // exit();
                   //return 213323;
        return view('frontend.main',compact('sliders','main_categories','business','newarrivals','featured','topsales'));
    }

    public function product_category($id) {
        $categories =  DB::table('categories')->where('publication_status',1)->get();
        $subcategories = DB::table('sub_categories')
            ->where('category_id',$id)
            ->where('publication_status',1)
            ->orderby('sub_category_id','desc')
            ->get();
        $category = Category::where('category_id',$id)->first();
        $products = Product::where('category_id', $id)
         ->where('publication_status',1)
                  ->orderBy('id', 'desc')
                 ->paginate(15);
        return view('frontend.product_category',compact('products','category','categories','subcategories'));
    }

    public function product_sub_category($id) {
         $categories =  DB::table('categories')->where('publication_status',1)->get();
         $subCategory = SubCategory::where('sub_category_id',$id)->first();
         $products = Product::where('sub_category_id', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->paginate(15); 
        return view('frontend.product_sub_category',compact('products','subCategory','categories'));
    }




    public function SingleProductPage($id) {
     
         $product = DB::table('products')->where('id',$id)->first();
         $topsales = DB::table('products')->where('offer_status',3)->orderby('id','desc')->take(10)->get();
         $categories = DB::table('categories')->where('publication_status',1)->get();

         $related = DB::table('products')->where('sub_category_id',$product->sub_category_id)->orderBy('id','desc')->take(4)->get();
                
        return view('frontend.product',compact('product','categories','related','topsales'));
    }





    public function cart() {
        //return Session::getId();
         $cart_items = DB::table('add_to_carts')->where('session_id',Session::getId())->get();
        return view('frontend.cart',compact('cart_items'));
    }


    public function ProductPageSub($id) {
         $subCategory = SubSubCategory::where('id',$id)->first();
         $products = Product::where('sub_sub_category_id', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('sub_category_product')->with('products',$products)->with('SubCategories',$subCategory);
    }

    public function CrazyDeal($id) {
     
         $products = Product::where('offer_status', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('crazy_deal')->with('products',$products)->with('id',$id);
    }


    public function OfferProduct($id) {
     
         $products = Product::where('offer_status', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('offer_product')->with('products',$products)->with('id',$id);
    }
    








    
    public function CustomerLogin() {
        
        return view('frontend.authentication');
    }
     public function CustomerSignUp() {
        
        return view('registration');
    }
    public function about_us() {
        
        $shop = DB::table('business')->first();
        $reviews = DB::table('brands')->get();
 
        return view('frontend.about',compact('shop','reviews'));
    }

    public function achievements() {
        $achievements = DB::table('achievements')->get();

        return view('frontend.achievements',compact('achievements'));
    }

    public function service_cat($id) {
        $cats = DB::table('service_cat')->get();
        $service_cat = DB::table('service_cat')->where('id',$id)->get();
        $services =  DB::table('services')->where('service_cat_id',$id)->get();

        return view('frontend.service_cat',compact('service_cat','cats','services'));
    }

    public function service($id) {
        $cats = DB::table('service_cat')->get();
         $service =  DB::table('services')->where('id',$id)->first();

        return view('frontend.service',compact('cats','service'));
    }


    public function contact_us() {
        
        return view('frontend.contact');
    }

     public function md_message() {
        
        return view('md_message');
    }

    public function treamCondition() {
        
        return view('delivery_policy');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
           'customer_name'=>'required|max:255'
       ));
        $cuntomer = new Customer;
      
       $cuntomer->customer_name = $request->customer_name;
       $cuntomer->phone_number = $request->phone_number;
       $cuntomer->address = $request->address;
       $cuntomer->email_address = $request->email_address;
       $cuntomer->password = md5($request->password);

       if($cuntomer->save()){
              $request->Session()->put('customer_name',$cuntomer->customer_name);
              $request->Session()->put('customer_id',$cuntomer->id);
           Session::flash('message','Registration Completed ....!');
        return Redirect::to('/checkout');
       }else{
        Session::flash('message','Invalid info ....!');
        return Redirect::to('/Login-Customer');
       }
    }
public function CustomerLoginCheck(Request $request) {      

        //echo "login";
        //return view('admin.admin_master');
        $email_address = $request->email_address;

        $password = md5($request->password);
                
      
 
        $result = DB::table('customers')
                ->where('email_address', $email_address)    
                ->where('password', $password)
                ->first();
        
      
        
        if ($result) {
            //return view('admin.admin_master');
           $request->Session()->put('customer_name',$result->customer_name);
              $request->Session()->put('customer_id',$result->id);
              Session::flash('message','You are successfully  logged in!');
             return Redirect::to('/');
             //return Session::get('customer_name');
        } else {
            Session::flash('message','User Id / Password Invalid');
            return Redirect::to('/join');
        }
   
        
    }

public function employeelogincheck(Request $request) {

        //echo "login";
        //return view('admin.admin_master');
        $email_address = $request->email_address;

         $password = md5($request->password);



         $result = DB::table('users')
                ->where('email', $email_address)
                ->where('password', $password)
                ->first();



        if ($result) {
            //return view('admin.admin_master');
           $request->Session()->put('employee_name',$result->name);
              $request->Session()->put('employee_id',$result->id);
              Session::flash('message','Employee Login Successful');
             return Redirect::to('/problem');
             //return Session::get('customer_name');
        } else {
            Session::flash('message','Employee Id / Password Invalid');
            return Redirect::to('/employee_login');
        }


    }
    
    public function place_order()
    {
        return view('frontend.order');

    }

    public function post_order(Request $request)
    {
        DB::table('problems')->insert([
            'project_name' => Input::get('code'),
            'employee_name' => Input::get('name'),
            'email' => Input::get('email'),
            'problem_type' => Input::get('address'),
            'problem_details' => Input::get('details'),
            'date' => date("d-m-Y"),

        ]);

$to = "order@bline-bd.com";
//$to = "akramul.i@agvcorp.com";
$subject = $request->get('name');
$txt =  'Client Name: '.$request->get('name').'<\br>';
$txt .=  'Product Code: '.$request->get('code').'<\br>';
//$txt .= 'Contact no: '.$request->get('contact_phone').'<\br>';
$txt .= 'Description: '.$request->get('details').'<\br>';
$txt .= 'Address: '.$request->get('address').'<\br>';

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

$headers .= 'From: <'.$request->get('email').'>' . "\r\n";
//$headers .= 'Cc: user@example.com' . "\r\n";


mail($to,$subject,$txt,$headers);




        Session::flash('message','Your Order has been placed. We will contact you soon');
        return \redirect()->back();
    }


    public function logoutcustomer()
    {
        Session::put('customer_name',null);
        Session::put('customer_id',null);

        Session::flash('message','You are successfully Logged Out!');
        return Redirect::to('/join');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function employee_logout()
    {
        Session::put('employee_name',null);
        Session::put('employee_id',null);

        Session::flash('message','Employee Successfully Logged Out!');
        return Redirect::to('/');
    }


    public function post_contact(Request $request)
    {
        //return $request->all();

        DB::table('contacts')->insert(
            [
                'contact_title' => $request->get('title'),
                'contact_email' => $request->get('email'),
                'contact_reference' => $request->get('address'),
                'contact_description' => $request->get('message'),
                'contact_phone' => '',
            ]);

$to = "info@bline-bd.com";
//$to = "akramul.i@agvcorp.com";
$subject = $request->get('title');
$txt = 'Address: '.$request->get('address').'<\br>';
$txt .= 'Description: '.$request->get('message').'<\br>';


$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

$headers .= 'From: <'.$request->get('email').'>' . "\r\n";
//$headers .= 'Cc: user@example.com' . "\r\n";


mail($to,$subject,$txt,$headers);




        Session::flash('message','Message Sent Successfully');
        return redirect()->back();
    }



    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    } 
    public function product_search(Request $request)
    {
       // return Input::all();
        //return $request->all();
       // $cat_id = $request->get('cat');
        $query = $request->get('query');
        //if ($cat_id==0) {
            $products = DB::table('products')->where('product_name', 'LIKE', '%'.$query.'%')->where('publication_status',1)->get();
        // }
        // else{
        //     $products = DB::table('products')
        //     ->where('category_id',$cat_id)
        //     ->where('product_name', 'LIKE', '%'.$query.'%')->get();
        // }
        return view('frontend.product_search',compact('products','query'));
    }

}
